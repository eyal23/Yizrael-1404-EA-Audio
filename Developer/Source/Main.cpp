/*
  ==============================================================================

    This file contains the basic startup code for a JUCE application.

  ==============================================================================
*/

#include <JuceHeader.h>

#include "DesktopGui/DAWApplication.h"

START_JUCE_APPLICATION (DAWApplication) //the app's execution