/*
  ==============================================================================

    Track.cpp
    Created: 27 Nov 2020 4:14:26pm
    Author:  משתמש

  ==============================================================================
*/

#include <vector>
#include <string>

#include "Track.h"
#include "../Helper/Ids.h"
#include "../DbWrapper/DbWrapper.h"


/*
    usage: constructor
    state: the track's intital state
*/
Track::Track(juce::ValueTree& state) :
    _state(state),
    _id(state, IDs::Track::id, nullptr),
    _name(state, IDs::Track::name, nullptr),
    _color(state, IDs::Track::color, nullptr),
    _length(state, IDs::Track::length, nullptr),
    _fader(state, IDs::Track::fader, nullptr),
    _panner(state, IDs::Track::panner, nullptr),
    _mute(state, IDs::Track::mute, nullptr)
{
    this->loadDataFromDb();
}

/*
    usage: evaluates the track's id with another id
    other: the other id
    return: if the ids are matchs
*/
bool Track::operator==(const int other) const
{
    return this->_id.get() == other;
}

/*
    usage: adds a listener to class state (ValueTree)
    listener: the listener to add
    return: the ValueTree
*/
juce::ValueTree Track::addListener(juce::ValueTree::Listener* listener)
{
    this->_state.addListener(listener);

    return this->_state;
}

/*
    usage: removes a listener from the class state (ValueTree)
    listener: the listener to remove
*/
void Track::removeListener(juce::ValueTree::Listener* listener)
{
    this->_state.removeListener(listener);
}

/*
    usage: gets the track's id
    return: the track's id
*/
int Track::getId() const
{
    return this->_id.get();
}

/*
    usage: gets the track's color
    return: the track's color
*/
std::string Track::getColor() const
{
    return this->_color.get().toStdString();
}

/*
    usage: gets the track's playback length
    return: the track's playback length
*/
float Track::getLength() const
{
    return this->_length.get();
}

/*
    usage: gets the track's fader
    return: the track's fader
*/
float Track::getFader() const
{
    return this->_fader.get();
}

/*
    usage: gets the track's panner
    return: the track's panner
*/
float Track::getPanner() const
{
    return this->_panner.get();
}

/*
    usage: check's if the track is muted
    return: is the track muted
*/
bool Track::getMute() const
{
    return this->_mute.get();
}

/*
    usage: gets the track's audio object
    return: the track's audio object
*/
TrackNode* Track::getTrackNode() const
{
    return this->_trackNode.get();
}

/*
    usage: gets a clip's object, by id
    id: the clip's id
    return: the clip's object
*/
Clip* Track::getClipObject(const int id)
{
    //for each clip, check if the id matchs
    for (auto* clip : this->_clips)
    {
        if (*clip == id)
        {
            return clip;
        }
    }

    return nullptr;
}

/*
    usage: creates a new clip
    clipData: the clip's data
*/
void Track::createClip(Models::Clip& clipData)
{
    //finding the clip's max length and actual length
    juce::File audioFile(clipData.path);
    juce::WavAudioFormat wavFormat;
    juce::AudioFormatReader* formatReaderPtr = wavFormat.createMemoryMappedReader(audioFile);
    std::unique_ptr<juce::AudioFormatReader> formatReader(formatReaderPtr);
    clipData.maxLength = formatReader.get()->lengthInSamples / formatReader.get()->sampleRate;
    clipData.length = clipData.length > clipData.maxLength ? clipData.maxLength : clipData.length;
    clipData.trackId = this->getId();

    //creating the clip in the db
    clipData.id = DbWrapper::getInstance()->createClip(clipData);

    //creating the path to copy the audio file to
    const std::string relativePath = std::string("DbFiles/") +
        std::string("Clips") +
        std::string("/") +
        std::to_string(clipData.id) +
        std::string(".wav");

    //creating matching state (ValueTree)
    juce::ValueTree newClip(IDs::Clip::object);
    newClip.setProperty(IDs::Clip::id, clipData.id, nullptr);
    newClip.setProperty(IDs::Clip::name, clipData.name.c_str(), nullptr);
    newClip.setProperty(IDs::Clip::path, (juce::File::getCurrentWorkingDirectory().getFullPathName() + std::string("/") + relativePath).toStdString().c_str(), nullptr);
    newClip.setProperty(IDs::Clip::maxLength, clipData.maxLength, nullptr);
    newClip.setProperty(IDs::Clip::length, clipData.length, nullptr);
    newClip.setProperty(IDs::Clip::StartingSecond, clipData.start, nullptr);

    //setting up the clip's object
    this->_clips.add(new Clip(newClip));
    this->_clips.getLast()->addListener(this);

    //setting up the clip's audio object
    this->_trackNode->addClip(this->_clips.getLast()->getClipNode());

    //adding the state to the track's state
    this->_state.addChild(newClip, -1, nullptr);

    //calculating the track's new playback length
    if (this->getLength() < clipData.start + clipData.length)
    {
        this->_length.setValue(clipData.start + clipData.length, nullptr);
    }
}

/*
    usage: deletes a clip
    id: the clip's id
*/
void Track::deleteClip(const int id)
{
    //if the clip exists, delete it
    if (this->doesClipExist(id))
    {
        //delete the clip from the db
        DbWrapper::getInstance()->deleteClip(id);

        //remove the clip from the state
        this->_state.removeChild(this->_state.getChildWithProperty(IDs::Clip::id, id), nullptr);

        int last = 0, secondToLast = 0;

        //finding the last and second last clips
        if (this->_clips.size() > 1)
        {
            for (int i = 0; i < this->_clips.size(); i++)
            {
                if (this->_clips[i]->getStartingSecond() + this->_clips[i]->getLength() > this->_clips[last]->getStartingSecond() + this->_clips[last]->getLength())
                {
                    secondToLast = last;
                    last = i;
                }
                else if (this->_clips[i]->getStartingSecond() + this->_clips[i]->getLength() > this->_clips[secondToLast]->getStartingSecond() + this->_clips[secondToLast]->getLength())
                {
                    secondToLast = i;
                }
            }
        }

        //for each clip, if the id matchs delete it
        for (int i = 0; i < this->_clips.size(); i++)
        {
            if (*this->_clips[i] == id)
            {
                //calculating the track's new playback length
                if (this->_clips.size() == 1)
                {
                    this->_length.setValue(0, nullptr);
                }
                else if (i == last)
                {
                    this->_length.setValue(this->_clips[secondToLast]->getStartingSecond() + this->_clips[secondToLast]->getLength(), nullptr);
                }

                this->_trackNode->removeClip(this->_clips[i]->getClipNode());
                this->_clips.removeObject(this->_clips[i]);
                break;
            }
        }
    }
}

/*
    usage: retrieves the track's all clips data
    return: the track's all clips data
*/
std::vector<Models::Clip> Track::retrieveClips() const
{
    std::vector<Models::Clip> clips;

    //for each clip, map to a Models::Clip object
    for (auto* clip : this->_clips)
    {
        clips.push_back(Models::Clip(
            clip->getId(),
            clip->getName(),
            clip->getPath(),
            clip->getMaxLength(),
            clip->getLength(),
            clip->getStartingSecond(),
            this->getId()
        ));
    }

    return clips;
}

/*
    usage: sets the track's fader
    newFader: the track's new fader
*/
void Track::setFader(const float newFader)
{
    this->_fader.setValue(newFader, nullptr);
    DbWrapper::getInstance()->updateTrackFader(this->_id.get(), newFader);
    this->_trackNode->setFader(newFader);
}

/*
    usage: sets the track's panner
    newPanner: the track's new panner
*/
void Track::setPanner(const float newPanner)
{
    this->_panner.setValue(newPanner, nullptr);
    DbWrapper::getInstance()->updateTrackPanner(this->_id.get(), newPanner);
    this->_trackNode->setPanner(newPanner);
}

/*
    usage: sets the track's mute
    newMute: the track's new mute
*/
void Track::setMute(const bool newMute)
{
    this->_mute.setValue(newMute, nullptr);
    DbWrapper::getInstance()->updateTrackMute(this->_id.get(), newMute);
    this->_trackNode->setMute(newMute);
}

/*
    usage: sets the track's name
    name: the track's new name
*/
void Track::setName(const std::string name)
{
    this->_name.setValue(name, nullptr);
    DbWrapper::getInstance()->updateTrackName(this->_id.get(), name);
}

/*
    usage: sets the track's color
    color: the track's new color
*/
void Track::setColor(const std::string color)
{
    this->_color.setValue(color, nullptr);
    DbWrapper::getInstance()->updateTrackColor(this->_id.get(), color);
}

/*
    usage: gets the track's name
    return: the track's name
*/
std::string Track::getName() const
{
    return this->_name.get().toStdString();
}

/*
    usage: callback to when a broadcasting ValueTree's property changed
    treeWhosePropertyHasChanged: the tree whose property has changed
    property: the property that has changed
*/
void Track::valueTreePropertyChanged(juce::ValueTree& treeWhosePropertyHasChanged, const juce::Identifier& property)
{
    if (this->getLength() < (float)treeWhosePropertyHasChanged[IDs::Clip::StartingSecond] + (float)treeWhosePropertyHasChanged[IDs::Clip::length])
    {
        this->_length.setValue((float)treeWhosePropertyHasChanged[IDs::Clip::StartingSecond] + (float)treeWhosePropertyHasChanged[IDs::Clip::length], nullptr);
    }
}

/*
    usage: check's if a clip is in the track
    id: the clip's id
    return: if the clip is in the track
*/
bool Track::doesClipExist(const int id) const
{
    for (auto& clip : this->_clips)
    {
        if (clip->getId() == id)
        {
            return true;
        }
    }

    return false;
}

/*
    usage: loads the track's clips from the db
*/
void Track::loadDataFromDb()
{
    //gets the clips data from the db
    auto clips = DbWrapper::getInstance()->retrieveClips(this->getId());
    juce::Array<ClipNode*> clipsNodes;

    //for each clip, map to a Clip object
    for (auto& clip : clips)
    {
        //creating matching state (ValueTree)
        juce::ValueTree newClip(IDs::Clip::object);
        newClip.setProperty(IDs::Clip::id, clip.id, nullptr);
        newClip.setProperty(IDs::Clip::name, clip.name.c_str(), nullptr);
        newClip.setProperty(IDs::Clip::path, (juce::File::getCurrentWorkingDirectory().getFullPathName() + "/" + clip.path).toStdString().c_str(), nullptr);
        newClip.setProperty(IDs::Clip::maxLength, clip.maxLength, nullptr);
        newClip.setProperty(IDs::Clip::length, clip.length, nullptr);
        newClip.setProperty(IDs::Clip::StartingSecond, clip.start, nullptr);

        //calculates the track's playback length
        if (this->getLength() < clip.start + clip.length)
        {
            this->_length.setValue(clip.start + clip.length, nullptr);
        }

        //adding the state to the track's state
        this->_state.addChild(newClip, -1, nullptr);

        //setting up the clip's object
        this->_clips.add(new Clip(newClip));
        this->_clips.getLast()->addListener(this);

        //setting up the clip's audio object
        clipsNodes.add(this->_clips.getLast()->getClipNode());
    }

    //setting up the track's audio object
    this->_trackNode.reset(new TrackNode(
        Models::Track(
            this->getId(), 
            this->getName(),
            this->getColor(),
            "",
            this->getFader(),
            this->getPanner(),
            this->getMute()
        ), 
        clipsNodes
    ));
}
