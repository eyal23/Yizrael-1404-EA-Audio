/*
  ==============================================================================

    Project.cpp
    Created: 7 Nov 2020 2:35:40pm
    Author:  eyal

  ==============================================================================
*/

#include "Project.h"
#include "../Helper/Ids.h"
#include "../DbWrapper/DbWrapper.h"
#include "../Audio/TimeLinePlayer.h"
#include "../Audio/Nodes/TrackNode.h"
#include "../Audio/Nodes/ClipNode.h"


/*
    usage: constructor
    state: the project's intital state
*/
Project::Project(juce::ValueTree& state) :
    _state(state), 
    _name(state, IDs::Project::name, nullptr),
    _creationDate(state, IDs::Project::creationDate, nullptr),
    _timeLineLength(state, IDs::Project::timeLineLength, nullptr)
{
    //loads the project's tracks
    loadTracksFromDb();
}

/*
    usage: evaluates the name of the project to another string
    other: the other string
    return: if the name of the project is equal to the other string
*/
bool Project::operator==(const std::string& other) const
{
    return this->_name.get().toStdString() == other;
}

/*
    usage: adds a listener to class state (ValueTree)
    listener: the listener to add
    return: the ValueTree
*/
juce::ValueTree Project::addListener(juce::ValueTree::Listener* listener)
{
    this->_state.addListener(listener);
    this->_listeners.add(listener);

    return this->_state;
}

/*
    usage: removes a listener from the class state (ValueTree)
    listener: the listener to remove
*/
void Project::removeListener(juce::ValueTree::Listener* listener)
{
    this->_state.removeListener(listener);
    this->_listeners.removeFirstMatchingValue(listener);
}

/*
    usage: removes all the listners from the class state (ValueTree)
*/
void Project::removeAllListeners()
{
    //for each listener, remove it
    for (auto* listener : this->_listeners)
    {
        this->_state.removeListener(listener);
    }

    this->_listeners.clear();
}

/*
    usage: gets the name of the project
    return: the name of the project
*/
std::string Project::getName() const
{
    return this->_name.get().toStdString();
}

/*
    usage: gets the creation date of the project
    return: the creation date of the project
*/
std::string Project::getCreationDate() const
{
    return this->_creationDate.get().toStdString();
}

/*
    usage: gets the project's audio object
    return: the project's audio object
*/
TimeLinePlayer* Project::getTimeLinePlayer()
{
    return this->_timeLinePlayer.get();
}

/*
    usage: gets a track's object, by id
    id: the track's id
    return: the track's object
*/
Track* Project::getTrackObject(const int id)
{
    //for each track, check if the id matchs
    for (auto* track : this->_tracks)
    {
        if (*track == id)
        {
            return track;
        }
    }

    return nullptr;
}

/*
    usage: gets all track's objects of the project
    return: all track's objects of the project
*/
juce::OwnedArray<Track>& Project::getTracksObjects()
{
    return this->_tracks;
}

/*
    usage: creates a new track
    trackData: the track's data
*/
void Project::createTrack(Models::Track& trackData)
{
    //creates the track in the db
    trackData.projectName = this->getName();
    trackData.id = DbWrapper::getInstance()->createTrack(trackData);

    //creating matching state (ValueTree)
    juce::ValueTree newTrack(IDs::Track::object);
    newTrack.setProperty(IDs::Track::id, trackData.id, nullptr);
    newTrack.setProperty(IDs::Track::name, trackData.name.c_str(), nullptr);
    newTrack.setProperty(IDs::Track::color, trackData.color.c_str(), nullptr);
    newTrack.setProperty(IDs::Track::length, 0.0f, nullptr);
    newTrack.setProperty(IDs::Track::fader, 0, nullptr);
    newTrack.setProperty(IDs::Track::panner, 0, nullptr);
    newTrack.setProperty(IDs::Track::mute, false, nullptr);

    //creating the track's object
    Track* newTrackObj = new Track(newTrack);
    this->_tracks.add(newTrackObj);
    this->_tracks.getLast()->addListener(this);

    //setting up the track's audio object
    this->_timeLinePlayer->addTrack(this->_tracks.getLast()->getTrackNode());

    //adds the track to the class state (ValueTree)
    this->_state.addChild(newTrack, -1, nullptr);
}

/*
    usage: deletes a track
    id: the track's id
*/
void Project::deleteTrack(const int id)
{
    //if the track exists, delete it
    if (this->doesTrackExist(id))
    {
        //deletes the track from the db
        DbWrapper::getInstance()->deleteTrack(id); 

        //removes the track from the class state (ValueTree)
        this->_state.removeChild(this->_state.getChildWithProperty(IDs::Track::id, id), nullptr);

        //find the track's object, and delete it
        for (int i = 0; i < this->_tracks.size(); i++)
        {
            if (*this->_tracks[i] == id)
            {
                this->_timeLineLength.setValue(this->calcLength(), nullptr);

                this->_timeLinePlayer->removeTrack(this->_tracks[i]->getTrackNode());
                this->_tracks.removeObject(this->_tracks[i]);
                break;
            }
        }
    }
}

/*
    usage: retrieves all track's data of the project
    return: all track's data of the project
*/
std::vector<Models::Track> Project::retrieveTracks() const
{
    std::vector<Models::Track> tracks;

    //for each track in the project, map to a Models::Track object
    for (auto* track : this->_tracks)
    {
        tracks.push_back(Models::Track(
            track->getId(),
            track->getName(),
            track->getColor(), 
            this->getName(),
            track->getFader(),
            track->getPanner(),
            track->getMute()
        ));
    }

    return tracks;
}

/*
    usage: callback to when a broadcasting ValueTree's property changed
    treeWhosePropertyHasChanged: the tree whose property has changed
    property: the property that has changed
*/
void Project::valueTreePropertyChanged(juce::ValueTree& treeWhosePropertyHasChanged, const juce::Identifier& property)
{
    //if one of the track's playback length has changed, update the project's length
    if (property == IDs::Track::length)
    {
        this->_timeLineLength.setValue(this->calcLength(), nullptr);
    }
}

/*
    usage: checks if a track exists in the project
    id: the track's id
    return: if the track exists in the project
*/
bool Project::doesTrackExist(const int id) const
{
    //for each track, check if the id matchs
    for (auto* track : this->_tracks)
    {
        if (track->getId() == id)
        {
            return true;
        }
    }

    return false;
}

/*
    usage: load's the project's tracks from the db
*/
void Project::loadTracksFromDb()
{
    //gets all tracks data from the db
    auto tracks = DbWrapper::getInstance()->retrieveTracks(this->_name.get().toStdString());
    juce::Array<TrackNode*> trackNodes;

    //zeros the project's playback length
    this->_timeLineLength.setValue(0.0f, nullptr);

    //for each track, map to a Track object
    for (auto& track : tracks)
    {
        //creating the matching state (ValueTree)
        juce::ValueTree newTrack(IDs::Track::object);
        newTrack.setProperty(IDs::Track::id, track.id, nullptr);
        newTrack.setProperty(IDs::Track::name, track.name.c_str(), nullptr);
        newTrack.setProperty(IDs::Track::color, track.color.c_str(), nullptr);
        newTrack.setProperty(IDs::Track::length, 0.0f, nullptr);
        newTrack.setProperty(IDs::Track::fader, track.fader, nullptr);
        newTrack.setProperty(IDs::Track::panner, track.panner, nullptr);
        newTrack.setProperty(IDs::Track::mute, track.mute, nullptr);

        //adds the state to the project's state
        this->_state.addChild(newTrack, -1, nullptr);

        //setting up the track object
        this->_tracks.add(new Track(newTrack));
        this->_tracks.getLast()->addListener(this);
        trackNodes.add(this->_tracks.getLast()->getTrackNode());

        //calculating the project's playback length
        if (this->_tracks.getLast()->getLength() > this->_timeLineLength.get())
        {
            this->_timeLineLength.setValue(this->_tracks.getLast()->getLength(), nullptr);
        }
    }

    //setting up the project's audio object
    this->_timeLinePlayer.reset(new TimeLinePlayer(trackNodes));
}

/*
    usage: claculates the project's playback length
    return: the project's playback length
*/
float Project::calcLength() const
{
    float length = 0;

    for (auto* track : this->_tracks)
    {
        if (length < track->getLength())
        {
            length = track->getLength();
        }
    }

    return length;
}
