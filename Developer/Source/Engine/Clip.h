/*
  ==============================================================================

    Clip.h
    Created: 4 Dec 2020 12:49:21pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../Audio/Nodes/ClipNode.h"


/*
    a class for managing the entire state of a clip
*/
class Clip
{
public:
    Clip(juce::ValueTree& state);

    bool operator==(const int other) const;

    juce::ValueTree addListener(juce::ValueTree::Listener* listener);
    void removeListener(juce::ValueTree::Listener* listener);

    int getId() const;
    std::string getName() const;
    std::string getPath() const;
    float getMaxLength() const;
    float getLength() const;
    float getStartingSecond() const;
    ClipNode* getClipNode() const;

    void cutFromEnd(const float length);
    void changeStart(const float start);

    void setName(const std::string name);
    void setPath(const std::string path);


private:
    juce::ValueTree _state;                   //the class state (ValueTree)
    juce::CachedValue<int> _id;               //the clip's id
    juce::CachedValue<juce::String> _name;    //the clip's name
    juce::CachedValue<juce::String> _path;    //the clip's path
    juce::CachedValue<float> _maxLenght;      //the clip's max length
    juce::CachedValue<float> _lenght;         //the clip's length
    juce::CachedValue<float> _startingSecond; //the clip's starting second
    std::unique_ptr<ClipNode> _clipNode;      //the clip's audio object
};