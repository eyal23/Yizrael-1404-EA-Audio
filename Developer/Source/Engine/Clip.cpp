/*
  ==============================================================================

    Clip.cpp
    Created: 4 Dec 2020 12:49:21pm
    Author:  משתמש

  ==============================================================================
*/

#include "Clip.h"
#include "../DbWrapper/DbWrapper.h"
#include "../Helper/Ids.h"


/*
    usage: constructor
    state: the track's initial state (ValueTree)
*/
Clip::Clip(juce::ValueTree& state) :
    _state(state),
    _id(state, IDs::Clip::id, nullptr),
    _name(state, IDs::Clip::name, nullptr),
    _path(state, IDs::Clip::path, nullptr),
    _maxLenght(state, IDs::Clip::maxLength, nullptr),
    _lenght(state, IDs::Clip::length, nullptr),
    _startingSecond(state, IDs::Clip::StartingSecond, nullptr)
{
    //setting up the clip's audio object
    this->_clipNode.reset(new ClipNode(Models::Clip(
        this->getId(),
        this->getName(),
        this->getPath(),
        this->getMaxLength(),
        this->getLength(),
        this->getStartingSecond(),
        -1
    )));
}

/*
    usage: evaluates the clip's id to another id
    other: the other id
    return: if the ids matchs
*/
bool Clip::operator==(const int other) const
{
    return this->_id.get() == other;
}

/*
    usage: adds a listener to class state (ValueTree)
    listener: the listener to add
    return: the ValueTree
*/
juce::ValueTree Clip::addListener(juce::ValueTree::Listener* listener)
{
    this->_state.addListener(listener);

    return this->_state;
}

/*
    usage: removes a listener from the class state (ValueTree)
    listener: the listener to remove
*/
void Clip::removeListener(juce::ValueTree::Listener* listener)
{
    this->_state.removeListener(listener);
}

/*
    usage: gets the clip's id
    return: the clip's id
*/
int Clip::getId() const
{
    return this->_id.get();
}

/*
    usage: gets the clip's name
    return: the clip's name
*/
std::string Clip::getName() const
{
    return this->_name.get().toStdString();
}

/*
    usage: gets the clip's path
    return: the clip's path
*/
std::string Clip::getPath() const
{
    return this->_path.get().toStdString();
}

/*
    usage: gets the clip's max length
    return: the clip's max length
*/
float Clip::getMaxLength() const
{
    return this->_maxLenght.get();
}

/*
    usage: gets the clip's length
    return: the clip's length
*/
float Clip::getLength() const
{
    return this->_lenght.get();
}

/*
    usage: gets the clip's starting second
    return: the clip's starting second
*/
float Clip::getStartingSecond() const
{
    return this->_startingSecond.get();
}

/*
    usage: gets the clip's audio object
    return: the clip's audio object
*/
ClipNode* Clip::getClipNode() const
{
    return this->_clipNode.get();
}

/*
    usage: cuts the clip from the end (set length)
    legnth: the new length
*/
void Clip::cutFromEnd(const float length)
{
    this->_lenght.setValue(length, nullptr);
    DbWrapper::getInstance()->updateClipLenght(this->_id.get(), length);
    this->_clipNode->setLenght(length);
}

/*
    usage: sets the clip's starting second
    start: the new starting second
*/
void Clip::changeStart(const float start)
{
    this->_startingSecond.setValue(start, nullptr);
    DbWrapper::getInstance()->updateClipStartingSecond(this->_id.get(), start);
    this->_clipNode->setStart(start);
}

/*
    usage: sets the clip's name
    path: the new name
*/
void Clip::setName(const std::string name)
{
    this->_name.setValue(name, nullptr);
    DbWrapper::getInstance()->updateClipName(this->_id.get(), name);
}

/*
    usage: sets the clip's path
    path: the new path
*/
void Clip::setPath(const std::string path)
{
    this->_path.setValue(path, nullptr);
    DbWrapper::getInstance()->updateClipPath(this->_id.get(), path);
}
