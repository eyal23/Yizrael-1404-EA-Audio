/*
  ==============================================================================

    Track.h
    Created: 27 Nov 2020 4:14:26pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include <vector>

#include "Clip.h"
#include "../Helper/Models.h"
#include "../Audio/Nodes/TrackNode.h"


/*
    a class for managing the entire state of a track
*/
class Track : private juce::ValueTree::Listener
{
public:
    Track(juce::ValueTree& state);

    bool operator==(const int other) const;

    juce::ValueTree addListener(juce::ValueTree::Listener* listener);
    void removeListener(juce::ValueTree::Listener* listener);

    int getId() const;
    std::string getName() const;
    std::string getColor() const;
    float getLength() const;
    float getFader() const;
    float getPanner() const;
    bool getMute() const;
    TrackNode* getTrackNode() const;

    Clip* getClipObject(const int id);

    void createClip(Models::Clip& clipData);

    void deleteClip(const int id);

    std::vector<Models::Clip> retrieveClips() const;

    void setFader(const float newFader);
    void setPanner(const float newPanner);
    void setMute(const bool newMute);

    void setName(const std::string name);
    void setColor(const std::string color);


private:
    //callback to when a broadcasting ValueTree's property changed
    void valueTreePropertyChanged(juce::ValueTree& treeWhosePropertyHasChanged, const juce::Identifier& property) override;

    bool doesClipExist(const int id) const;
    void loadDataFromDb();

private:
    juce::ValueTree _state;                 //the class state (ValueTree)
    juce::CachedValue<int> _id;             //the track's id
    juce::CachedValue<juce::String> _name;  //the track's name
    juce::CachedValue<juce::String> _color; //the track's color
    juce::CachedValue<float> _length;       //the track's playback length
    juce::CachedValue<float> _fader;        //the track's fader
    juce::CachedValue<float> _panner;       //the track's panner
    juce::CachedValue<bool> _mute;          //is the track mutes
    juce::OwnedArray<Clip> _clips;          //the track's clips
    std::unique_ptr<TrackNode> _trackNode;  //the track's audio object
};