/*
  ==============================================================================

    Engine.h
    Created: 7 Nov 2020 2:35:22pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include <vector>

#include "Project.h"
#include "../Helper/Models.h"


/*
    a class for managing the entire state of the app (singletone)
*/
class Engine
{
public:
    juce::ValueTree addListener(juce::ValueTree::Listener* listener); //adds listener to the class state (ValueTree)
    void removeListener(juce::ValueTree::Listener* listener);         //removes listener from the class state (ValueTree)

    //part of the singletone pattern
    Engine(Engine& other) = delete;
    void operator=(const Engine& other) = delete;

    static Engine* getInstance(); //gets the class instance

    Project* getOpenedProjectObject();
    juce::AudioDeviceManager& getDeviceManager();
    void createProject(const Models::Project& projectData);
    void deleteProject(const std::string& name);
    std::vector<Models::Project> retrieveProjects() const;
    void openProject(const std::string& name);
    void closeProject();

private:
    Engine();
    ~Engine();

    bool doesProjectExist(const std::string& name) const;

private:
    static Engine* _instance;                           //the class instance
    juce::ValueTree _state;                             //the class state
    std::unique_ptr<Project> _openedProject;            //the currently opened project
    juce::CachedValue<juce::String> _openedProjectName; //the opened project name
    juce::AudioDeviceManager _deviceManager;            //the audio devices manager
};