/*
  ==============================================================================

    Engine.cpp
    Created: 7 Nov 2020 2:35:22pm
    Author:  eyal

  ==============================================================================
*/

#include <thread>

#include "Engine.h"
#include "../DbWrapper/DbWrapper.h"
#include "../Helper/Ids.h"

Engine* Engine::_instance{ nullptr };


/*
    usage: adds a listener to class state (ValueTree)
    listener: the listener to add
    return: the ValueTree
*/
juce::ValueTree Engine::addListener(juce::ValueTree::Listener* listener)
{
    this->_state.addListener(listener);

    return this->_state;
}

/*
    usage: removes a listener from the class state (ValueTree)
    listener: the listener to remove
*/
void Engine::removeListener(juce::ValueTree::Listener* listener)
{
    this->_state.removeListener(listener);
}

/*
    usage: constructor
*/
Engine::Engine() :
    _state(IDs::Engine::object), _openedProjectName(_state, IDs::Engine::openedProjectName, nullptr, "")
{

    this->_deviceManager.initialise(2, 2, nullptr, true);
}

/*
    usage: destructor
*/
Engine::~Engine()
{
}

/*
    usage: checks if a project exists by name
    name: the name of the project to check for
    return: if the project exists
*/
bool Engine::doesProjectExist(const std::string& name) const
{
    //retrieve all projects
    auto projects = this->retrieveProjects();

    //for each project, check if names match
    for (auto& project : projects)
    {
        if (project.name == name)
        {
            return true;
        }
    }

    return false;
}

/*
    usage: gets the class instance
    return: teh class instance
*/
Engine* Engine::getInstance()
{
    if (_instance == nullptr)
    {
        _instance = new Engine();
    }

    return _instance;
}


/*
    usage: gets the currently opened project object
    return: the currently opened project object
*/
Project* Engine::getOpenedProjectObject()
{
    return this->_openedProject.get();
}

/*
    usage: gets the device manager
    return: the device manager
*/
juce::AudioDeviceManager& Engine::getDeviceManager()
{
    return this->_deviceManager;
}


/*
    usage: creates a new project
    projectData: the new project's data
*/
void Engine::createProject(const Models::Project& projectData)
{
    //if the project doesn't exists, create it on the db
    if (!this->doesProjectExist(projectData.name))
    {
        DbWrapper::getInstance()->createProject(projectData);
    }
}

/*
    usage: deletes a project
    name: the project to delete's name.
*/
void Engine::deleteProject(const std::string& name)
{
    //if the project exists, delete it from the db
    if (this->doesProjectExist(name))
    {
        DbWrapper::getInstance()->deleteProject(name);
    }
}

/*
    usage: retrieves all projects data
    return: the projects data.
*/
std::vector<Models::Project> Engine::retrieveProjects() const
{
    return DbWrapper::getInstance()->retrieveProjects();
}

/*
    usage: opens a project
    name: the name of the project to open.
*/
void Engine::openProject(const std::string& name)
{
    //if the project exists, open it
    if (this->doesProjectExist(name))
    {
        //mapping data into Models::Project object
        Models::Project projectData = DbWrapper::getInstance()->retrieveProject(name);

        //creating matching state (ValueTree)
        juce::ValueTree project(IDs::Project::object);
        project.setProperty(IDs::Project::name, juce::String(projectData.name), nullptr);
        project.setProperty(IDs::Project::creationDate, juce::String(projectData.creationDate), nullptr);
        project.setProperty(IDs::Project::timeLineLength, 0.0f, nullptr);
        
        //setting up the new opened project object
        this->_openedProject.reset(new Project(project));
        this->_openedProjectName.setValue(name, nullptr);
    }
}

/*
    usage: closes the opened project
*/
void Engine::closeProject()
{
    this->_openedProject.reset(nullptr);
    this->_openedProjectName.setValue("", nullptr);
}
