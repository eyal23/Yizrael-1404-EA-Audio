/*
  ==============================================================================

    Project.h
    Created: 7 Nov 2020 2:35:40pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include <vector>

#include "Track.h"
#include "../Helper/Models.h"


/*
    a class for managing the entire state of a project
*/
class Project : private juce::ValueTree::Listener
{
public:
    Project(juce::ValueTree& state);

    bool operator==(const std::string& other) const;

    juce::ValueTree addListener(juce::ValueTree::Listener* listener);
    void removeListener(juce::ValueTree::Listener* listener);
    void removeAllListeners();

    std::string getName() const;
    std::string getCreationDate() const;
    TimeLinePlayer* getTimeLinePlayer();
    Track* getTrackObject(const int id);
    juce::OwnedArray<Track>& getTracksObjects();

    void createTrack(Models::Track& trackData);
    void deleteTrack(const int id);
    std::vector<Models::Track> retrieveTracks() const;

private:
    //callback to when a broadcasting ValueTree's property changed
    void valueTreePropertyChanged(juce::ValueTree& treeWhosePropertyHasChanged, const juce::Identifier& property) override;

    bool doesTrackExist(const int id) const;
    void loadTracksFromDb();
    float calcLength() const;

    Project();

private:
    juce::ValueTree _state;                             //the class state
    juce::CachedValue<juce::String> _name;              //the project's name
    juce::CachedValue<juce::String> _creationDate;      //the project's creation date
    juce::CachedValue<float> _timeLineLength;           //the project's playback length
    juce::OwnedArray<Track> _tracks;                    //the tracks if the project
    std::unique_ptr<TimeLinePlayer> _timeLinePlayer;    //the audio object of the project
    juce::Array<juce::ValueTree::Listener*> _listeners; //the listeners to the class
};