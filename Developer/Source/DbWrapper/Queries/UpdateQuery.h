/*
  ==============================================================================

    UpdateQuery.h
    Created: 7 Jan 2021 3:05:39pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include <iostream>
#include <vector>


/*
    a class for easy creation of sql update query
*/
class UpdateQuery
{
public:
    UpdateQuery(
        const std::string& table,
        const std::vector<std::string>& values,
        const std::vector<std::string> fields,
        const std::vector<std::pair<std::string, std::string>>& conditions
    );

    std::string getQueryString() const;

private:
    std::string UpdateQuery::getUpdatePart() const;
    std::string UpdateQuery::getSetPart()const;
    std::string UpdateQuery::getWherePart()const;

private:
    std::string _table;                                           //the query's table
    std::vector<std::string> _values;                             //the query's values
    std::vector<std::string> _fields;                             //the query's fields
    std::vector<std::pair<std::string, std::string>> _conditions; //the query's conditions
};