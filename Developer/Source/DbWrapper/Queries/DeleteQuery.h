/*
  ==============================================================================

    DeleteQuery.h
    Created: 11 Nov 2020 2:42:48pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <iostream>
#include <vector>


/*
    a class for easy creation of sql delete query
*/
class DeleteQuery
{
public:
    DeleteQuery(
        const std::string& table,
        const std::vector<std::pair<std::string, std::string>>& conditions
    );

    std::string getQueryString() const;

private:
    std::string getDeletePart() const;
    std::string getWherePart() const;

private:
    const std::string _table;                                           //the table of the query
    const std::vector<std::pair<std::string, std::string>> _conditions; //the conditions of the query
};