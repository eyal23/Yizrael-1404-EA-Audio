/*
  ==============================================================================

    SelectQuery.cpp
    Created: 11 Nov 2020 2:41:51pm
    Author:  eyal

  ==============================================================================
*/

#include "SelectQuery.h"


/*
	usage: constructor
	tables: the table(s) to select from
	joiners: how to join the tables together (not required)
	fields: the fields to select
	conditions: the conditions for a column to be selected
*/
SelectQuery::SelectQuery(
    const std::vector<std::string>& tables,
    const std::vector<std::pair<std::string, std::string>>& joiners,
    const std::vector<std::string>& fields,
    const std::vector<std::pair<std::string, std::string>>& conditions
) : _tables(tables), _joiners(joiners), _fields(fields), _conditions(conditions) {}

/*
	usage: gets the select query's string
	return: the select query's string
*/
std::string SelectQuery::getQueryString() const
{
	return this->getSelectPart() +
		this->getFromPart() +
		this->getJoinPart() +
		this->getWherePart();
}

/*
	usage: gets the "SELECT" part of select query's string
	return: the "SELECT" part of select query's string
*/
std::string SelectQuery::getSelectPart() const
{
	std::string selectPartStr = "SELECT ";

	//apply each field
	for (int i = 0; i < this->_fields.size(); i++)
	{
		selectPartStr += this->_fields[i];
		if (i < this->_fields.size() - 1)
		{
			selectPartStr += ",";
		}
		selectPartStr += " ";
	}

	return selectPartStr;
}

/*
	usage: gets the "FROM" part of select query's string
	return: the "FROM" part of select query's string
*/
std::string SelectQuery::getFromPart() const
{
	return std::string("FROM ") +
		this->_tables[0] +
		std::string(" ");
}

/*
	usage: gets the "JOIN" part of select query's string
	return: the "JOIN" part of select query's string
*/
std::string SelectQuery::getJoinPart() const
{
	std::string joinPartStr = "";

	//apply each joiner
	for (int i = 1; i < this->_tables.size(); i++)
	{
		joinPartStr += std::string("INNER JOIN ") +
			this->_tables[i] +
			std::string(" ON ") +
			this->_joiners[i - 1].first +
			std::string(" = ") +
			this->_joiners[i - 1].second +
			std::string(" ");
	}

	return joinPartStr;
}

/*
	usage: gets the "WHERE" part of select query's string
	return: the "WHERE" part of select query's string
*/
std::string SelectQuery::getWherePart() const
{
	std::string wherePartStr = "";

	if (!this->_conditions.empty())
	{
		wherePartStr = std::string("WHERE ");

		//apply each condition
		for (int i = 0; i < this->_conditions.size(); i++)
		{
			wherePartStr += this->_conditions[i].first +
				std::string(" = ") +
				this->_conditions[i].second;

			if (i < this->_conditions.size() - 1)
			{
				wherePartStr += " AND ";
			}
		}

		wherePartStr += ";";
	}

	return wherePartStr;
}