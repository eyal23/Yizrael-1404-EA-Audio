/*
  ==============================================================================

    InsertQuery.cpp
    Created: 11 Nov 2020 2:42:05pm
    Author:  eyal

  ==============================================================================
*/

#include "InsertQuery.h"

/*
	usage: constructor
	table: the table to insert to
	values: the values to insert
*/
InsertQuery::InsertQuery(
    const std::string& table,
    const std::vector<std::string>& values
) : _table(table), _values(values) {}

/*
	usage: gets the insert query's string
	return: the insert query's string
*/
std::string InsertQuery::getQueryString() const
{
	return this->getInsertPart() + this->getValuesPart();
}

/*
	usage: gets the "INSERT" part of insert query's string
	return: the "INSERT" part of insert query's string
*/
std::string InsertQuery::getInsertPart() const
{
	std::string insertPartStr = std::string("INSERT INTO ") +
		this->_table +
		std::string(" (");

	//fileds of the projects table
	if (this->_table == "PROJECTS")
	{
		insertPartStr += "PROJECT_NAME , PROJECT_CREATION_DATE) ";
	}
	//fileds of the tracks table
	else if (this->_table == "TRACKS")
	{
		insertPartStr += "TRACK_NAME , TRACK_COLOR , TRACK_PROJECT_NAME , TRACK_FADER , TRACK_PANNER , TRACK_MUTE) ";
	}
	//fileds of the clips table
	else if (this->_table == "CLIPS")
	{
		insertPartStr += "CLIP_NAME , CLIP_PATH , CLIP_MAX_LENGTH , CLIP_LENGTH , CLIP_STARTING_SECOND , CLIP_TRACK_ID) ";
	}

	return insertPartStr;
}

/*
	usage: gets the "VALUES" part of insert query's string
	return: the "VALUES" part of insert query's string
*/
std::string InsertQuery::getValuesPart() const
{
	std::string valuesPartStr = "VALUES (";

	//match each value for field
	for (int i = 0; i < this->_values.size(); i++)
	{
		valuesPartStr += this->_values[i];
		if (i < this->_values.size() - 1)
		{
			valuesPartStr += " , ";
		}
		else
		{
			valuesPartStr += ");";
		}
	}

	return valuesPartStr;
}
