/*
  ==============================================================================

    SelectQuery.h
    Created: 11 Nov 2020 2:41:51pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <iostream>
#include <vector>


/*
    a class for easy creation of select sql query
*/
class SelectQuery
{
public:
	SelectQuery(
        const std::vector<std::string>& tables,
        const std::vector<std::pair<std::string, std::string>>& joiners,
        const std::vector<std::string>& fields,
        const std::vector<std::pair<std::string, std::string>>& conditions
    );

    std::string getQueryString() const;

private:
    std::string getSelectPart() const;
    std::string getFromPart() const;
    std::string getJoinPart() const;
    std::string getWherePart() const;

private:
    const std::vector<std::string> _tables;                             //the tables of the query
    const std::vector<std::pair<std::string, std::string>> _joiners;    //the joiners of the query (not required)
    const std::vector<std::string> _fields;                             //the fields of the query
    const std::vector<std::pair<std::string, std::string>> _conditions; //the conditions of the query
};