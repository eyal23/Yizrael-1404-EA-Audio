/*
  ==============================================================================

    UpdateQuery.cpp
    Created: 7 Jan 2021 3:05:39pm
    Author:  משתמש

  ==============================================================================
*/

#include <iterator>

#include "UpdateQuery.h"


/*
	usage: constructor
	table: the table to update
	values: the new values
	fields: the fields to update
	conditions: the conditions for a column to update
*/
UpdateQuery::UpdateQuery(const std::string& table,
	const std::vector<std::string>& values,
	const std::vector<std::string> fields,
	const std::vector<std::pair<std::string, std::string>>& conditions
) : _table(table), _values(values), _fields(fields), _conditions(conditions) {}

/*
	usage: gets the update query's string
	return: the update query's string
*/
std::string UpdateQuery::getQueryString() const
{
	return this->getUpdatePart() + this->getSetPart() + this->getWherePart();
}

/*
	usage: gets the "UPDATE" part of the query
	return: the "UPDATE" part of the query
*/
std::string UpdateQuery::getUpdatePart()const
{
	std::string updatePart = std::string("UPDATE ") +
		this->_table +
		std::string(" ");

	return updatePart;
}

/*
	usage: gets the "SET" part of the query
	return: the "SET" part of the query
*/
std::string UpdateQuery::getSetPart()const
{
	std::string setPart = "SET ";
	int count = 0;
	
	//match each value to field
	for (std::vector<std::string>::const_iterator  it1 = this->_fields.begin(), it2 = _values.begin() ; it1 != _fields.end(), it2 != _values.end(); it1++,it2++ )
	{
		setPart += *it1 + "=" + *it2;
		if (count < this->_values.size() - 1)
		{
			setPart += " , ";
		}
		else
		{
			setPart += " ";
		}
		count++;
	}

	return setPart;
}

/*
	usage: gets the "WHERE" part of the query
	return: the "WHERE" part of the query
*/
std::string UpdateQuery::getWherePart() const
{
	std::string wherePartStr = "";

	if (!this->_conditions.empty())
	{
		wherePartStr = std::string("WHERE ");

		//apply each condition
		for (int i = 0; i < this->_conditions.size(); i++)
		{
			wherePartStr += this->_conditions[i].first +
				std::string(" = ") +
				this->_conditions[i].second;

			if (i < this->_conditions.size() - 1)
			{
				wherePartStr += " AND ";
			}
		}

		wherePartStr += ";";
	}

	return wherePartStr;
}