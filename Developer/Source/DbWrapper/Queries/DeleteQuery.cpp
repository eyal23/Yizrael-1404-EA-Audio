/*
  ==============================================================================

    DeleteQuery.cpp
    Created: 11 Nov 2020 2:42:48pm
    Author:  eyal

  ==============================================================================
*/

#include "DeleteQuery.h"


/*
	usage: constructor
	table: the table to delete from
	conditions: the conditions for a column to be deleted
*/
DeleteQuery::DeleteQuery(
	const std::string& table,
	const std::vector<std::pair<std::string, std::string>>& conditions
) : _table(table), _conditions(conditions) {}

/*
	usage: constructs the delete query's string
	return: the delete query's string
*/
std::string DeleteQuery::getQueryString() const
{
	return this->getDeletePart() + this->getWherePart();
}

/*
	usage: gets the "DELETE" part of the delete query string
	return: the "DELETE" part of the delete query string
*/
std::string DeleteQuery::getDeletePart() const
{
	return std::string("DELETE FROM ") + this->_table;
}

/*
	usage: gets the "WHERE" part of the delete query string
	return: the "WHERE" part of the delete query string
*/
std::string DeleteQuery::getWherePart() const
{
	std::string wherePartStr = " WHERE ";

	//apply each condition
	for (int i = 0; i < this->_conditions.size(); i++)
	{
		wherePartStr += this->_conditions[i].first +
			std::string(" = ") +
			this->_conditions[i].second;

		if (i < this->_conditions.size() - 1)
		{
			wherePartStr  += " AND ";
		}
		else
		{
			wherePartStr += ";";
		}
	}

	return wherePartStr;
}
