/*
  ==============================================================================

    InsertQuery.h
    Created: 11 Nov 2020 2:42:05pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <iostream>
#include <vector>


/*
    a class for easy creation of insert sql query
*/
class InsertQuery
{
public:
    InsertQuery(
        const std::string& table,
        const std::vector<std::string>& values
    );

    std::string getQueryString() const;

private:
    std::string getInsertPart() const;
    std::string getValuesPart() const;

private:
    const std::string _table;               //the table of the query
    const std::vector<std::string> _values; //the values of the fields of the query
};