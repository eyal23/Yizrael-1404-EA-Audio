/*
  ==============================================================================

    DbWrapper.h
    Created: 11 Nov 2020 12:41:22pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <vector>

#include "../Helper/Models.h"
#include "sqlite3.h"


/*
    a wrapper class for easy access to the sql db (singletone)
*/
class DbWrapper
{
public:
    //part of the singletone design-pattern
    DbWrapper(const DbWrapper& other) = delete;
    void operator=(const DbWrapper& other) = delete;

    static DbWrapper* getInstance(); //gets the class instance

    void createProject(const Models::Project& projectData);
    int createTrack(const Models::Track& trackData);
    int createClip(const Models::Clip& clipData);

    void deleteProject(const std::string& name);
    void deleteTrack(const unsigned int id);
    void deleteClip(const unsigned int id);

    void updateTrackName(const int id, const std::string newName);
    void updateTrackColor(const int id, const std::string newColor);
    void updateTrackFader(const int id, const float newFader);
    void updateTrackPanner(const int id, const float newPanner);
    void updateTrackMute(const int id, const bool isMuted);
    void updateClipStartingSecond(const int id, const float newSec);
    void updateClipLenght(const int id, const float newLen);
    void updateClipName(const int id, const std::string newName);
    void updateClipPath(const int id, const std::string newPath);

    Models::Project retrieveProject(const std::string& name);
    std::vector<Models::Project> retrieveProjects();
    std::vector<Models::Track> retrieveTracks(const std::string& projectName);   
    std::vector<Models::Clip> retrieveClips(const unsigned int trackId);

private:
    DbWrapper();
    ~DbWrapper();

	void initDatabase();
	static int callback(void* data, int argc, char** argv, char** azColName);

private:
    static DbWrapper* _instance; //the class instance
	sqlite3* _db;                //the actaul db
};

