/*
  ==============================================================================

    DbWrapper.cpp
    Created: 11 Nov 2020 12:41:22pm
    Author:  eyal

  ==============================================================================
*/

#include <io.h>
#include <map>

#include "DbWrapper.h"
#include "Queries/SelectQuery.h"
#include "Queries/InsertQuery.h"
#include "Queries/DeleteQuery.h"
#include "Queries/UpdateQuery.h"

//defining all db contants
#define DATABASE_NAME "DawDb.sqlite"
#define PROJECTS_TABLE_NAME "PROJECTS"
#define PROJECT_NAME_FIELD "PROJECT_NAME"
#define PROJECT_CREATION_DATE_FIELD "PROJECT_CREATION_DATE"
#define TRACKS_TABLE_NAME "TRACKS"
#define TRACK_ID_FIELD "TRACK_ID"
#define TRACK_NAME_FIELD "TRACK_NAME"
#define TRACK_COLOR_FIELD "TRACK_COLOR"
#define TRACK_PROJECT_NAME_FIELD "TRACK_PROJECT_NAME"
#define TRACK_FADER_FIELD "TRACK_FADER"
#define TRACK_PANNER_FIELD "TRACK_PANNER"
#define TRACK_MUTE_FIELD "TRACK_MUTE"
#define CLIPS_TABLE_NAME "CLIPS"
#define CLIP_ID_FIELD "CLIP_ID"
#define	CLIP_NAME_FIELD "CLIP_NAME"
#define CLIP_PATH_FIELD "CLIP_PATH"
#define CLIP_MAX_LENGTH_FIELD "CLIP_MAX_LENGTH"
#define CLIP_LENGTH_FIELD "CLIP_LENGTH"
#define CLIP_STARTING_SECOND_FIELD "CLIP_STARTING_SECOND"
#define CLIP_TRACK_ID_FIELD "CLIP_TRACK_ID"

DbWrapper* DbWrapper::_instance{ nullptr };


/*
	usage: gets the class instance
	return: the class instance
*/
DbWrapper* DbWrapper::getInstance()
{
	if (_instance == nullptr)
	{
		_instance = new DbWrapper();
	}

    return _instance;
}

/*
	usage: constructor
*/
DbWrapper::DbWrapper()
{
	//checks if the db already exists
	int doesExist = _access(DATABASE_NAME, 0);

	//opens the db
	if (sqlite3_open(DATABASE_NAME, &this->_db) == SQLITE_OK)
	{
		//initializes the db if it just got created
		if (doesExist == -1)
		{
			initDatabase();
		}
	}
}

/*
	usage: destructor
*/
DbWrapper::~DbWrapper()
{
	sqlite3_close(this->_db);
}

/*
	usage: creates a new project in the database.
	projectData: the new projects data
*/
void DbWrapper::createProject(const Models::Project& projectData)
{
	//creates the matching sql query
	InsertQuery query(
		PROJECTS_TABLE_NAME, 
		{
			std::string("\"") + projectData.name + std::string("\""),
			std::string("\"") + projectData.creationDate + std::string("\"")
		}
	);

	//running the query on the db
	sqlite3_exec(_instance->_db, query.getQueryString().c_str(), nullptr, nullptr, nullptr);
}

/*
	usage: creates a new track in the database.
	trackData: the new track's data
	return: the new track's id
*/
int DbWrapper::createTrack(const Models::Track& trackData)
{
	//creates the sql insert query
	InsertQuery insertQuery(
		TRACKS_TABLE_NAME,
		{
			std::string("\"") + trackData.name + std::string("\""),
			std::string("\"") + trackData.color + std::string("\""),
			std::string("\"") + trackData.projectName + std::string("\""),
			"0",
			"0",
			"0"
		}
	);

	//running the insert query on the db
	sqlite3_exec(_instance->_db, insertQuery.getQueryString().c_str(), nullptr, nullptr, nullptr);

	//finding the new track's id
	std::vector<std::map<std::string, std::string>> result;

	//creates the sql select query
	SelectQuery selectQuery(
		{ TRACKS_TABLE_NAME },
		{},
		{ TRACK_ID_FIELD },
		{
			{ TRACK_NAME_FIELD, std::string("\"") + trackData.name + std::string("\"") },
			{ TRACK_PROJECT_NAME_FIELD, std::string("\"") + trackData.projectName + std::string("\"") }
		}
	);

	//running the select query on the db
	sqlite3_exec(this->_db, selectQuery.getQueryString().c_str(), callback, &result, nullptr);

	return std::stoi(result[0][TRACK_ID_FIELD]);
}

/*
	usage: creates a new clip in the database
	clipData: the new clip's data
	return: the new clip's id
*/
int DbWrapper::createClip(const Models::Clip& clipData)
{
	//creates the sql insert query
	InsertQuery insertQuery(
		CLIPS_TABLE_NAME,
		{
			std::string("\"") + clipData.name + std::string("\""),
			std::string("\"\""),
			std::to_string(clipData.maxLength),
			std::to_string(clipData.length),
			std::to_string(clipData.start),
			std::to_string(clipData.trackId)
		}
	);

	//running the insert query on the db
	sqlite3_exec(_instance->_db, insertQuery.getQueryString().c_str(), nullptr, nullptr, nullptr);

	//finding the new clip's id
	std::vector<std::map<std::string, std::string>> result;

	//creates the sql select query
	SelectQuery selectQuery(
		{ CLIPS_TABLE_NAME },
		{},
		{ CLIP_ID_FIELD },
		{
			{ CLIP_NAME_FIELD, std::string("\"") + clipData.name + std::string("\"") },
			{ CLIP_TRACK_ID_FIELD, std::to_string(clipData.trackId) }
		}
	);

	//running the select query on the db
	sqlite3_exec(this->_db, selectQuery.getQueryString().c_str(), callback, &result, nullptr);

	const int clipId = std::stoi(result[0][CLIP_ID_FIELD]);

	//copying the given audio file to the db's directroy
	const std::string relativePath = std::string("DbFiles/") +
		std::string("Clips") +
		std::string("/") +
		std::to_string(clipId) +
		std::string(".wav");

	juce::File audioFile(clipData.path);
	juce::File copiedFile(juce::File::getCurrentWorkingDirectory().getFullPathName() + std::string("/") + relativePath);
	copiedFile.create();
	audioFile.copyFileTo(copiedFile);

	//creates the sql update query
	UpdateQuery updateQuery(
		std::string("CLIPS"),
		{ std::string("\"") + relativePath + std::string("\"") },
		{ CLIP_PATH_FIELD },
		{ { CLIP_ID_FIELD, std::to_string(clipId) } }
	);

	//running the update query on the db
	sqlite3_exec(_instance->_db, updateQuery.getQueryString().c_str(), nullptr, nullptr, nullptr);

	return clipId;
}

/*
	usage: deletes a project from the database
	name: the name of the project to delete
*/
void DbWrapper::deleteProject(const std::string& name)
{
	//retieves all tracks of the project
	auto tracks = _instance->retrieveTracks(name);

	//deletes all tracks of the project
	for (auto& track : tracks)
	{
		_instance->deleteTrack(track.id);
	}

	//creates the sql delete query
	DeleteQuery query(
		PROJECTS_TABLE_NAME,
		{
			{ PROJECT_NAME_FIELD, std::string("\"") + name + std::string("\"") }
		}
	);

	//running the delete query on the db
	sqlite3_exec(_instance->_db, query.getQueryString().c_str(), nullptr, nullptr, nullptr);
}

/*
	usage: deletes a track from the database
	id: the id of the track to delete
*/
void DbWrapper::deleteTrack(const unsigned int id)
{
	//retrieves all clips of the track
	auto clips = _instance->retrieveClips(id);

	//deletes all clips of the track
	for (auto& clip : clips)
	{
		_instance->deleteClip(clip.id);
	}

	//creates the sql delete query
	DeleteQuery query(
		TRACKS_TABLE_NAME,
		{
			{ TRACK_ID_FIELD, std::to_string(id) }
		}
	);

	//running the delete query on the db
	sqlite3_exec(_instance->_db, query.getQueryString().c_str(), nullptr, nullptr, nullptr);
}

/*
	usage: deletes a clip from the database
	id: the id of the clip to delete
*/
void DbWrapper::deleteClip(const unsigned int id)
{
	//deletes the clip's audio file
	const std::string relativePath = std::string("DbFiles/") +
		std::string("Clips") +
		std::string("/") +
		std::to_string(id) +
		std::string(".wav");

	juce::File audioFile(juce::File::getCurrentWorkingDirectory().getFullPathName() + std::string("\\") + relativePath);
	bool result = audioFile.deleteFile();

	//creates the sql delete query
	DeleteQuery query(
		CLIPS_TABLE_NAME,
		{
			{ CLIP_ID_FIELD, std::to_string(id) }
		}
	);

	//running the delete query on the db
	sqlite3_exec(_instance->_db, query.getQueryString().c_str(), nullptr, nullptr, nullptr);
}

/*
	usage: update a track's name in the database
	id: the id of the track to update
	newName: the new name
*/
void DbWrapper::updateTrackName(const int id, const std::string newName)
{
	//creates the sql update query
	UpdateQuery query(
		std::string("TRACKS"),
		{ std::string("\"") + newName + std::string("\"") },
		{ TRACK_NAME_FIELD },
		{ { TRACK_ID_FIELD, std::to_string(id) } }
	);

	//running the update query on the db
	sqlite3_exec(_instance->_db, query.getQueryString().c_str(), nullptr, nullptr, nullptr);
}

/*
	usage: update a track's color in the database
	id: the id of the track to update
	newColor: the new color
*/
void DbWrapper::updateTrackColor(const int id, const std::string newColor)
{
	//creates the sql update query
	UpdateQuery query(
		std::string("TRACKS"),
		{ std::string("\"") + newColor + std::string("\"") },
		{ TRACK_COLOR_FIELD },
		{ { TRACK_ID_FIELD, std::to_string(id) } }
	);

	//running the update query on the db
	sqlite3_exec(_instance->_db, query.getQueryString().c_str(), nullptr, nullptr, nullptr);
}

/*
	usage: update a track's fader in the database
	id: the id of the track to update
	newFader: the new fader
*/
void DbWrapper::updateTrackFader(const int id, const float newFader)
{
	//creates the sql update query
	UpdateQuery query(
		std::string(TRACKS_TABLE_NAME),
		{ std::to_string(newFader) },
		{ TRACK_FADER_FIELD },
		{ { TRACK_ID_FIELD, std::to_string(id) } }
	);

	//running the update query on the db
	sqlite3_exec(_instance->_db, query.getQueryString().c_str(), nullptr, nullptr, nullptr);
}

/*
	usage: update a track's panner in the database
	id: the id of the track to update
	newPanner: the new panner
*/
void DbWrapper::updateTrackPanner(const int id, const float newPanner)
{
	//creates the sql update query
	UpdateQuery query(
		std::string(TRACKS_TABLE_NAME),
		{ std::to_string(newPanner) },
		{ TRACK_PANNER_FIELD },
		{ { TRACK_ID_FIELD, std::to_string(id) } }
	);

	//running the update query on the db
	sqlite3_exec(_instance->_db, query.getQueryString().c_str(), nullptr, nullptr, nullptr);
}

/*
	usage: update a track's mute in the database
	id: the id of the track to update
	newMute: the new mute
*/
void DbWrapper::updateTrackMute(const int id, const bool isMuted)
{
	//creates the sql update query
	UpdateQuery query(
		std::string(TRACKS_TABLE_NAME),
		{ std::to_string(isMuted) },
		{ TRACK_MUTE_FIELD },
		{ { TRACK_ID_FIELD, std::to_string(id) } }
	);

	//running the update query on the db
	sqlite3_exec(_instance->_db, query.getQueryString().c_str(), nullptr, nullptr, nullptr);
}

/*
	usage: updates a clip's starting second in the database
	id: the id of the clip to update
	newSec: the new starting second
*/
void DbWrapper::updateClipStartingSecond(const int id, const float newSec)
{
	//creates the sql update query
	UpdateQuery query(
		std::string("CLIPS"),
		{ std::to_string(newSec) }, 
		{ CLIP_STARTING_SECOND_FIELD },
		{ { CLIP_ID_FIELD, std::to_string(id) } }
	);

	//running the update query on the db
	sqlite3_exec(_instance->_db, query.getQueryString().c_str(), nullptr, nullptr, nullptr);
}

/*
	usage: updates a clip's length in the database
	id: the id of the clip to update
	newLen: the new length
*/
void DbWrapper::updateClipLenght(const int id, const float newLen)
{
	//creates the sql update query
	UpdateQuery query(
		std::string("CLIPS"),
		{ std::to_string(newLen) }, 
		{ CLIP_LENGTH_FIELD },
		{ { CLIP_ID_FIELD, std::to_string(id) } }
	);

	//running the update query on the db
	sqlite3_exec(_instance->_db, query.getQueryString().c_str(), nullptr, nullptr, nullptr);
}

/*
	usage: updates a clip's name in the database
	id: the id of the clip to update
	newName: the new name
*/
void DbWrapper::updateClipName(const int id, const std::string newName)
{
	//creates the sql update query
	UpdateQuery query(
		std::string("CLIPS"),
		{ std::string("\"") + newName + std::string("\"") },
		{ CLIP_NAME_FIELD },
		{ { CLIP_ID_FIELD, std::to_string(id) } }
	);

	//running the update query on the db
	sqlite3_exec(_instance->_db, query.getQueryString().c_str(), nullptr, nullptr, nullptr);
}

/*
	usage: updates a clip's path in the database
	id: the id of the clip to update
	newPath: the new path
*/
void DbWrapper::updateClipPath(const int id, const std::string newPath)
{
	//creates the sql update query
	UpdateQuery query(
		std::string("CLIPS"),
		{ std::string("\"") + newPath + std::string("\"") },
		{ CLIP_PATH_FIELD },
		{ { CLIP_ID_FIELD, std::to_string(id) } }
	);

	//running the update query on the db
	sqlite3_exec(_instance->_db, query.getQueryString().c_str(), nullptr, nullptr, nullptr);
}

/*
	usage: retrieves a project's data from the database
	name: the project name
	return: the project's data
*/
Models::Project DbWrapper::retrieveProject(const std::string& name)
{
	std::vector<std::map<std::string, std::string>> result;

	//creates the sql select query
	SelectQuery query(
		{ PROJECTS_TABLE_NAME },
		{},
		{ PROJECT_NAME_FIELD, PROJECT_CREATION_DATE_FIELD },
		{
			{ PROJECT_NAME_FIELD, std::string("\"") + name + std::string("\"") }
		}
	);

	//running the select query on the db
	sqlite3_exec(this->_db, query.getQueryString().c_str(), callback, &result, nullptr);

	return Models::Project(
		result[0][PROJECT_NAME_FIELD],
		result[0][PROJECT_CREATION_DATE_FIELD]
	);
}

/*
	usage: retrieves all projects data from the database
	return: all projects data
*/
std::vector<Models::Project> DbWrapper::retrieveProjects()
{
	std::vector<std::map<std::string, std::string>> result;
	std::vector<Models::Project> projects;

	//creates the sql select query
	SelectQuery query(
		{ PROJECTS_TABLE_NAME },
		{},
		{ PROJECT_NAME_FIELD, PROJECT_CREATION_DATE_FIELD },
		{}
	);

	//running the select query on the db
	sqlite3_exec(this->_db, query.getQueryString().c_str(), callback, &result, nullptr);

	//mapping the results to Models::Project objects
	for (auto& column : result)
	{
		projects.push_back(Models::Project(
			column[PROJECT_NAME_FIELD],
			column[PROJECT_CREATION_DATE_FIELD]
		));
	}

	return projects;
}

/*
	usage: retrieves projects all tracks data from the database
	projectName: the project name
	return: the project's all tracks data
*/
std::vector<Models::Track> DbWrapper::retrieveTracks(const std::string& projectName)
{
	std::vector<std::map<std::string, std::string>> result;
	std::vector<Models::Track> tracks;

	//creates the sql select query
	SelectQuery query(
		{ TRACKS_TABLE_NAME },
		{},
		{ 
			TRACK_ID_FIELD, 
			TRACK_NAME_FIELD,
			TRACK_COLOR_FIELD,
			TRACK_PROJECT_NAME_FIELD,
			TRACK_FADER_FIELD,
			TRACK_PANNER_FIELD,
			TRACK_MUTE_FIELD
		},
		{
			{ TRACK_PROJECT_NAME_FIELD, std::string("\"") + projectName + std::string("\"") }
		}
	);

	//running the select query on the db
	sqlite3_exec(this->_db, query.getQueryString().c_str(), callback, &result, nullptr);

	//mapping the results to Models::Track objects
	for (auto& column : result)
	{
		tracks.push_back(Models::Track(
			std::stoi(column[TRACK_ID_FIELD]), 
			column[TRACK_NAME_FIELD], 
			column[TRACK_COLOR_FIELD], 
			column[TRACK_PROJECT_NAME_FIELD],
			std::stof(column[TRACK_FADER_FIELD]),
			std::stof(column[TRACK_PANNER_FIELD]),
			std::stoi(column[TRACK_MUTE_FIELD])

		));
	}

	return tracks;
}

/*
	usage: retrieves a track all clips data from the database
	trackId: the track id
	return: the track's all clips data
*/
std::vector<Models::Clip> DbWrapper::retrieveClips(const unsigned int trackId)
{
	std::vector<std::map<std::string, std::string>> result;
	std::vector<Models::Clip> clips;

	//creates the sql select query
	SelectQuery query(
		{ CLIPS_TABLE_NAME },
		{},
		{ CLIP_ID_FIELD, CLIP_NAME_FIELD, CLIP_PATH_FIELD, CLIP_MAX_LENGTH_FIELD, CLIP_LENGTH_FIELD, CLIP_STARTING_SECOND_FIELD, CLIP_TRACK_ID_FIELD},
		{
			{ CLIP_TRACK_ID_FIELD, std::to_string(trackId) }
		}
		);

	//running the select query on the db
	sqlite3_exec(this->_db, query.getQueryString().c_str(), callback, &result, nullptr);

	//mapping the results to Models::Clip objects
	for (auto& column : result)
	{
		clips.push_back(Models::Clip(
			std::stoi(column[CLIP_ID_FIELD]),
			column[CLIP_NAME_FIELD],
			column[CLIP_PATH_FIELD], 
			std::stof(column[CLIP_MAX_LENGTH_FIELD]),
			std::stof(column[CLIP_LENGTH_FIELD]), 
			std::stof(column[CLIP_STARTING_SECOND_FIELD]),
			std::stoi(column[CLIP_TRACK_ID_FIELD])
		));
	}

	return clips;
}

/*
	usage: initializes the database with all of the needed tables.
*/
void DbWrapper::initDatabase()
{
	//the create tables sql queries
	const std::vector<const char*> tableQueries = {
		"CREATE TABLE PROJECTS (PROJECT_NAME TEXT PRIMARY KEY , PROJECT_CREATION_DATE TEXT NOT NULL);",
		"CREATE TABLE TRACKS (TRACK_ID INTEGER PRIMARY KEY AUTOINCREMENT, TRACK_NAME TEXT NOT NULL, TRACK_COLOR TEXT NOT NULL, TRACK_FADER FLOAT, TRACK_PANNER FLOAT, TRACK_MUTE INTEGER, TRACK_PROJECT_NAME TEXT FOREIGON KEY REFERENCES PROJECTS(PROJECT_NAME));",
		"CREATE TABLE CLIPS (CLIP_ID INTEGER PRIMARY KEY AUTOINCREMENT, CLIP_NAME TEXT NOT NULL, CLIP_PATH TEXT NOT NULL, CLIP_MAX_LENGTH FLOAT, CLIP_LENGTH FLOAT, CLIP_STARTING_SECOND FLOAT, CLIP_TRACK_ID TEXT FOREIGON KEY REFERENCES TRACKS(TRACK_ID));"
	};

	//running all create queries on the db
	for (int i = 0; i < tableQueries.size(); i++)
	{
		sqlite3_exec(this->_db, tableQueries[i], nullptr, nullptr, nullptr);
	}
}

/*
	usage: fetchs column's data from the database.
	data: the place to save the data.
	argc: the amount of fields the column has.
	argv: the fields values.
	azColName: the names of the fields.
	return: 0
*/
int DbWrapper::callback(void* data, int argc, char** argv, char** azColName)
{
	std::vector<std::map<std::string, std::string>>* pData = (std::vector<std::map<std::string, std::string>>*)data;
	std::map<std::string, std::string> column;

	if (*argv != NULL)
	{
		//for each field, map to a matching map key
		for (int i = 0; i < argc; i++)
		{
			column[azColName[i]] = argv[i];
		}

		//adds the column to the results
		pData->push_back(column);
	}

	return 0;
}
