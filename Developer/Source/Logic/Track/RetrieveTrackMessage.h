/*
  ==============================================================================

    RetrieveTrackMessage.h
    Created: 27 Nov 2020 8:30:51pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include <future>
#include <vector>

#include "../LogicMessage.h"
#include "../../Helper/Models.h"


/*
    a class for retrieving the opened projects all tracks data logical message
*/
class RetrieveTrackMessage : public LogicMessage
{
public:
    std::future<std::vector<Models::Track>> getFutureResponse();

    void operator()() override; //message logic

private:
    std::promise<std::vector<Models::Track>> _logicResponse; //the message's future response
};