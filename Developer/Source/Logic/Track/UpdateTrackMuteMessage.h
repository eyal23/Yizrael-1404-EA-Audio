/*
  ==============================================================================

    UpdateMuteMessage.h
    Created: 7 Feb 2021 11:24:43am
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for updating a track's mute logical message
*/
class UpdateTrackMuteMessage : public LogicMessage
{
public:
    UpdateTrackMuteMessage(const bool Mute, const int trackId);

    void operator()() override; //message logic

private:
    const bool _Mute;
    const int _TrackId;
};