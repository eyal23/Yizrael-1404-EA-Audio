/*
  ==============================================================================

    DeleteTrackMessage.h
    Created: 27 Nov 2020 8:29:54pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for deleting a clip logical message
*/
class DeleteTrackMessage : public LogicMessage
{
public:
    DeleteTrackMessage(const int id);

    void operator()() override; //message logic

private:
    const int _id;
};
