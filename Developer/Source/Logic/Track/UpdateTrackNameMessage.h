/*
  ==============================================================================

    UpdateTrackNameMessage.h
    Created: 16 Feb 2021 3:01:13pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for updating a track's name logical message
*/
class UpdateTrackNameMessage : public LogicMessage
{
public:
    UpdateTrackNameMessage(const std::string name, const int trackId);

    void operator()() override; //message logic

private:
    const std::string _name;
    const int _TrackId;
};