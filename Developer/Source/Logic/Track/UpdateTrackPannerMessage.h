/*
  ==============================================================================

    UpdateTrackPannerMessage.h
    Created: 27 Jan 2021 6:53:27pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for updating a track's panner logical message
*/
class UpdateTrackPannerMessage : public LogicMessage
{
public:
    UpdateTrackPannerMessage(const float Panner, const int trackId);

    void operator()() override; //message logic

private:
    const float _Panner;
    const int _TrackId;
};