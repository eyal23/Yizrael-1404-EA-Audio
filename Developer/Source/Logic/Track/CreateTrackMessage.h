/*
  ==============================================================================

    CreateTrackMessage.h
    Created: 27 Nov 2020 8:29:38pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"
#include "../../Helper/Models.h"


/*
    a class for creating a track logical message
*/
class CreateTrackMessage : public LogicMessage
{
public:
    CreateTrackMessage(Models::Track& trackData);

    void operator()() override; //message logic

private:
    Models::Track _trackData; //the track's data
};