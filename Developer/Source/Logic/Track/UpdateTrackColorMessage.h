/*
  ==============================================================================

    UpdateTrackColorMessage.h
    Created: 16 Feb 2021 3:03:54pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for updating a track's color logical message
*/
class UpdateTrackColorMessage : public LogicMessage
{
public:
    UpdateTrackColorMessage(const std::string color, const int trackId);

    void operator()() override; //message logic

private:
    const std::string _color;
    const int _TrackId;
};