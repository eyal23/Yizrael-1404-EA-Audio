/*
  ==============================================================================

    DeleteTrackMessage.cpp
    Created: 27 Nov 2020 8:29:54pm
    Author:  משתמש

  ==============================================================================
*/

#include "DeleteTrackMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    id: the track's id
*/
DeleteTrackMessage::DeleteTrackMessage(const int id) :
    _id(id)
{
}

/*
    usage: message logic
*/
void DeleteTrackMessage::operator()()
{
    Engine::getInstance()->getOpenedProjectObject()->deleteTrack(this->_id);
}