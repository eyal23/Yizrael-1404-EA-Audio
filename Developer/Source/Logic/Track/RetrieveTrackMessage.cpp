/*
  ==============================================================================

    RetrieveTrackMessage.cpp
    Created: 27 Nov 2020 8:30:51pm
    Author:  משתמש

  ==============================================================================
*/

#include "RetrieveTrackMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: gets the message's future response
    return: the message's future resposne
*/
std::future<std::vector<Models::Track>> RetrieveTrackMessage::getFutureResponse()
{
    return this->_logicResponse.get_future();
}

/*
    usage: message logic
*/
void RetrieveTrackMessage::operator()()
{
    this->_logicResponse.set_value({ Engine::getInstance()->getOpenedProjectObject()->retrieveTracks() });
}