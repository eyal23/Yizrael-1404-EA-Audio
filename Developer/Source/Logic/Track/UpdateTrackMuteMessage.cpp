/*
  ==============================================================================

    UpdateMuteMessage.cpp
    Created: 7 Feb 2021 11:24:43am
    Author:  eyal

  ==============================================================================
*/

#include "UpdateTrackMuteMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    mute: the track's new mute
    trackId: the track's id
*/
UpdateTrackMuteMessage::UpdateTrackMuteMessage(const bool Mute, const int trackId) :
    _Mute(Mute), _TrackId(trackId)
{
}

/*
    usage: message logic
*/
void UpdateTrackMuteMessage::operator()()
{
    Engine::getInstance()->getOpenedProjectObject()->getTrackObject(this->_TrackId)->setMute(this->_Mute);
}
