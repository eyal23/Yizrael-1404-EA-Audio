/*
  ==============================================================================

    UpdateTrackFader.cpp
    Created: 20 Jan 2021 5:02:03pm
    Author:  משתמש

  ==============================================================================
*/

#include "UpdateTrackFaderMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    fader: the track's new fader
    trackId: the track's id
*/
UpdateTrackFaderMessage::UpdateTrackFaderMessage(const float fader, const int trackId) :
    _fader(fader), _TrackId(trackId)
{
}

/*
    usage: message logic
*/
void UpdateTrackFaderMessage::operator()()
{
    Engine::getInstance()->getOpenedProjectObject()->getTrackObject(this->_TrackId)->setFader(this->_fader);
}
