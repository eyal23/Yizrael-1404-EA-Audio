/*
  ==============================================================================

    UpdateTrackNameMessage.cpp
    Created: 16 Feb 2021 3:01:13pm
    Author:  משתמש

  ==============================================================================
*/

#include "UpdateTrackNameMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    name: the track's new name
    trackId: the track's id
*/
UpdateTrackNameMessage::UpdateTrackNameMessage(const std::string name, const int trackId) :
    _name(name), _TrackId(trackId)
{
}

/*
    usage: message logic
*/
void UpdateTrackNameMessage::operator()()
{
    Engine::getInstance()->getOpenedProjectObject()->getTrackObject(this->_TrackId)->setName(this->_name);
}
