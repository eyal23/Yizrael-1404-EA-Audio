/*
  ==============================================================================

    UpdateTrackPannerMessage.cpp
    Created: 27 Jan 2021 6:53:27pm
    Author:  eyal

  ==============================================================================
*/

#include "UpdateTrackPannerMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    panner: the track's new panner
    trackId: the track's id
*/
UpdateTrackPannerMessage::UpdateTrackPannerMessage(const float Panner, const int trackId) :
    _Panner(Panner), _TrackId(trackId)
{
}

/*
    usage: message logic
*/
void UpdateTrackPannerMessage::operator()()
{
    Engine::getInstance()->getOpenedProjectObject()->getTrackObject(this->_TrackId)->setPanner(this->_Panner);
}