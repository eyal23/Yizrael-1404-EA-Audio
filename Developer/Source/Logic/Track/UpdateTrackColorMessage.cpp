/*
  ==============================================================================

    UpdateTrackColorMessage.cpp
    Created: 16 Feb 2021 3:03:54pm
    Author:  משתמש

  ==============================================================================
*/

#include "UpdateTrackColorMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    color: the track's new color
    trackId: the track's id
*/
UpdateTrackColorMessage::UpdateTrackColorMessage(const std::string color, const int trackId) :
    _color(color), _TrackId(trackId)
{
}

/*
    usage: message logic
*/
void UpdateTrackColorMessage::operator()()
{
    Engine::getInstance()->getOpenedProjectObject()->getTrackObject(this->_TrackId)->setColor(this->_color);
}
