/*
  ==============================================================================

    CreateTrackMessage.cpp
    Created: 27 Nov 2020 8:29:38pm
    Author:  משתמש

  ==============================================================================
*/

#include "CreateTrackMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    trackData: the track's data
*/
CreateTrackMessage::CreateTrackMessage(Models::Track& trackData) :
    _trackData(trackData)
{
}

/*
    usage: message logic
*/
void CreateTrackMessage::operator()()
{
    Engine::getInstance()->getOpenedProjectObject()->createTrack(this->_trackData);
}
