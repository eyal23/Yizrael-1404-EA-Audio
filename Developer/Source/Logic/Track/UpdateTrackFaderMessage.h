/*
  ==============================================================================

    UpdateTrackFader.h
    Created: 20 Jan 2021 5:02:03pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
     a class for updating a track's fader logical message
*/
class UpdateTrackFaderMessage : public LogicMessage
{
public:
    UpdateTrackFaderMessage(const float fader, const int trackId);

    void operator()() override; //message logic

private:
    const float _fader;
    const int _TrackId;
};