/*
  ==============================================================================

    ChangeLengthMessage.h
    Created: 11 Jan 2021 2:00:21pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for changing clip's
*/
class ChangeLengthMessage : public LogicMessage
{
public:
    ChangeLengthMessage(const float length, const int trackId, const int clipId);

    void operator()() override;

private:
    const float _length;
    const int _trackId;
    const int _clipId;
};