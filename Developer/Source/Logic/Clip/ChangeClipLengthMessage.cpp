/*
  ==============================================================================

    ChangeLengthMessage.cpp
    Created: 11 Jan 2021 2:00:21pm
    Author:  משתמש

  ==============================================================================
*/

#include "ChangeClipLengthMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    length: the clip's new length
    trackId: the clip's track's id
    clipId: the clip's id
*/
ChangeClipLengthMessage::ChangeClipLengthMessage(const float length, const int trackId, const int clipId) :
     _length(length), _trackId(trackId), _clipId(clipId)
{
}

/*
    usage: message logic
*/
void ChangeClipLengthMessage::operator()()
{
    Engine::getInstance()->getOpenedProjectObject()->getTrackObject(this->_trackId)->getClipObject(this->_clipId)->cutFromEnd(this->_length);
}