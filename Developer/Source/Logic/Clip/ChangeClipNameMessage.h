/*
  ==============================================================================

    ChangeClipNameMessage.h
    Created: 16 Feb 2021 2:59:36pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for changing clip's name logical message
*/
class ChangeClipNameMessage : public LogicMessage
{
public:
    ChangeClipNameMessage(const std::string name, const int trackId, const int clipId);

    void operator()() override; //message logic

private:
    const std::string _name;
    const int _trackId;
    const int _clipId;
};