/*
  ==============================================================================

    ChangeClipPathMessage.h
    Created: 16 Feb 2021 3:00:07pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for changing clip's path logical message
*/
class ChangeClipPathMessage : public LogicMessage
{
public:
    ChangeClipPathMessage(const std::string path, const int trackId, const int clipId);

    void operator()() override; //message logic

private:
    const std::string _path;
    const int _trackId;
    const int _clipId;
};