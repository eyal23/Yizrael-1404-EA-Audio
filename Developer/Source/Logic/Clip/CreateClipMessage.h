/*
  ==============================================================================

    CreateClipMessage.h
    Created: 4 Dec 2020 2:42:50pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"
#include "../../Helper/Models.h"


/*
    a class for creating a new clip logical message
*/
class CreateClipMessage : public LogicMessage
{
public:
    CreateClipMessage(Models::Clip& clipData);

    void operator()() override; //message logic

private:
    Models::Clip _clipData; //the clip's data
};