/*
  ==============================================================================

    DeleteClipMessage.h
    Created: 4 Dec 2020 2:43:05pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for deleting a clip logical message
*/
class DeleteClipMessage : public LogicMessage
{
public:
    DeleteClipMessage(const int id, const int trackId);

    void operator()() override; //message logic

private:
    const int _id;
    const int _trackId;
};