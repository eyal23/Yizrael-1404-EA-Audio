/*
  ==============================================================================

    ChangeStartingSecondMessage.h
    Created: 11 Jan 2021 2:01:40pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for changing clip's starting second logical message
*/
class ChangeClipStartingSecondMessage : public LogicMessage
{
public:
    ChangeClipStartingSecondMessage(const float startingSecond, const int trackId, const int clipId);

    void operator()() override; //message logic

private:
    const float _startingSecond;
    const int _trackId;
    const int _clipId;
};

