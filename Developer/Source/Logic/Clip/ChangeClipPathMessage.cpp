/*
  ==============================================================================

    ChangeClipPathMessage.cpp
    Created: 16 Feb 2021 3:00:07pm
    Author:  משתמש

  ==============================================================================
*/

#include "ChangeClipPathMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    path: the clip's new path
    trackId: the clip's track's id
    clipId: the clip's id
*/
ChangeClipPathMessage::ChangeClipPathMessage(const std::string path, const int trackId, const int clipId) :
    _path(path), _trackId(trackId), _clipId(clipId)
{
}

/*
    usage: message logic
*/
void ChangeClipPathMessage::operator()()
{
    Engine::getInstance()->getOpenedProjectObject()->getTrackObject(this->_trackId)->getClipObject(this->_clipId)->setPath(this->_path);
}
