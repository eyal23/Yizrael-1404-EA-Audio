/*
  ==============================================================================

    RetrieveClipMessage.h
    Created: 4 Dec 2020 2:43:47pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include <future>
#include <vector>

#include "../LogicMessage.h"
#include "../../Helper/Models.h"


/*
    a class for retrieving a track's all clips data logical message
*/
class RetrieveClipMessage : public LogicMessage
{
public:
    RetrieveClipMessage(const int id);

    std::future<std::vector<Models::Clip>> getFutureResponse();

    void operator()() override; //message logic

private:
    const int _id;
    std::promise<std::vector<Models::Clip>> _logicResponse; //the message's future response
};