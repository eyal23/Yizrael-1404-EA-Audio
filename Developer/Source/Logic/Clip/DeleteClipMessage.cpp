/*
  ==============================================================================

    DeleteClipMessage.cpp
    Created: 4 Dec 2020 2:43:05pm
    Author:  משתמש

  ==============================================================================
*/

#include "DeleteClipMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    id: the clip's id
    trackId: the clip's track's id
*/
DeleteClipMessage::DeleteClipMessage(const int id, const int trackId) :
    _id(id), _trackId(trackId)
{
}

/*
    usage: message logic
*/
void DeleteClipMessage::operator()()
{
    Engine::getInstance()->getOpenedProjectObject()->getTrackObject(this->_trackId)->deleteClip(this->_id);
}
