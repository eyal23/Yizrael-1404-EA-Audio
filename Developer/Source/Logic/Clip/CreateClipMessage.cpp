/*
  ==============================================================================

    CreateClipMessage.cpp
    Created: 4 Dec 2020 2:42:50pm
    Author:  משתמש

  ==============================================================================
*/

#include "CreateClipMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    clipData: teh new clip's data
*/
CreateClipMessage::CreateClipMessage(Models::Clip& clipData) :
    _clipData(clipData)
{
}

/*
    usage: message logic
*/
void CreateClipMessage::operator()()
{
    Engine::getInstance()->getOpenedProjectObject()->getTrackObject(this->_clipData.trackId)->createClip(this->_clipData);
}