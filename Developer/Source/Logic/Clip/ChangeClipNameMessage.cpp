/*
  ==============================================================================

    ChangeClipNameMessage.cpp
    Created: 16 Feb 2021 2:59:36pm
    Author:  משתמש

  ==============================================================================
*/

#include "ChangeClipNameMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    name: the clip's new name
    trackId: the clip's track's id
    clipId: the clip's id
*/
ChangeClipNameMessage::ChangeClipNameMessage(const std::string name, const int trackId, const int clipId) :
    _name(name), _trackId(trackId), _clipId(clipId)
{
}

/*
    usage: message logic
*/
void ChangeClipNameMessage::operator()()
{
    Engine::getInstance()->getOpenedProjectObject()->getTrackObject(this->_trackId)->getClipObject(this->_clipId)->setName(this->_name);
}
