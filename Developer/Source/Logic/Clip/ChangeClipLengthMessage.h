/*
  ==============================================================================

    ChangeLengthMessage.h
    Created: 11 Jan 2021 2:00:21pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for changing clip's length logical message
*/
class ChangeClipLengthMessage : public LogicMessage
{
public:
    ChangeClipLengthMessage(const float length, const int trackId, const int clipId);

    void operator()() override; //message logic

private:
    const float _length;
    const int _trackId;
    const int _clipId;
};