/*
  ==============================================================================

    RetrieveClipMessage.cpp
    Created: 4 Dec 2020 2:43:47pm
    Author:  משתמש

  ==============================================================================
*/

#include "RetrieveClipMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    id: the track id
*/
RetrieveClipMessage::RetrieveClipMessage(const int id) :
    _id(id)
{
}

/*
    usage: gets the future response of the message
    return: the future response of the message
*/
std::future<std::vector<Models::Clip>> RetrieveClipMessage::getFutureResponse()
{
    return this->_logicResponse.get_future();
}

/*
    usage: message logic
*/
void RetrieveClipMessage::operator()()
{
    this->_logicResponse.set_value({ Engine::getInstance()->getOpenedProjectObject()->getTrackObject(this->_id)->retrieveClips()});
}
