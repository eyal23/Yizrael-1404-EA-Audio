/*
  ==============================================================================

    ChangeStartingSecondMessage.cpp
    Created: 11 Jan 2021 2:01:40pm
    Author:  משתמש

  ==============================================================================
*/

#include "ChangeClipStartingSecondMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    startingSecond: the clip's new starting second
    trackId: the clip's track's id
    clipId: the clip's id
*/
ChangeClipStartingSecondMessage::ChangeClipStartingSecondMessage(const float startingSecond, const int trackId, const int clipId) :
     _startingSecond(startingSecond), _trackId(trackId), _clipId(clipId)
{
}

/*
    usage: message logic
*/
void ChangeClipStartingSecondMessage::operator()()
{
    Engine::getInstance()->getOpenedProjectObject()->getTrackObject(this->_trackId)->getClipObject(this->_clipId)->changeStart(this->_startingSecond);
}

