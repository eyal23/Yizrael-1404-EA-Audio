/*
  ==============================================================================

    RetriveProjectsMessage.h
    Created: 7 Nov 2020 6:14:29pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <future>
#include <vector>

#include "../LogicMessage.h"
#include "../../Helper/Models.h"


/*
    a class for retrieving all projects data logical message
*/
class RetrieveProjectsMessage : public LogicMessage
{
public:
    std::future<std::vector<Models::Project>> getFutureResponse();

    void operator()() override; //message logic

private:
    std::promise<std::vector<Models::Project>> _logicResponse; //the message's future response
};