/*
  ==============================================================================

    OpenProjectMessage.h
    Created: 10 Nov 2020 8:38:18pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for opening a project logical message
*/
class OpenProjectsMessage : public LogicMessage
{
public:
    OpenProjectsMessage(const std::string& name);

    void operator()() override; //message logic

private:
    const std::string _name;
};