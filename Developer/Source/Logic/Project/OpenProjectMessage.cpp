/*
  ==============================================================================

    OpenProjectMessage.cpp
    Created: 10 Nov 2020 8:38:18pm
    Author:  eyal

  ==============================================================================
*/

#include "OpenProjectMessage.h"
#include "../../Engine/Engine.h"
#include "../../Audio/TimeLinePlayHead.h"


/*
    usage: constructor
    name: the project's name
*/
OpenProjectsMessage::OpenProjectsMessage(const std::string& name) :
    _name(name)
{
}

/*
    usage: message logic
*/
void OpenProjectsMessage::operator()()
{
    Engine::getInstance()->openProject(this->_name);
    TimeLinePlayHead::getInstance()->transportPlay(false);
    TimeLinePlayHead::getInstance()->transportRewind();
}
