/*
  ==============================================================================

    DeleteProjectMessage.cpp
    Created: 10 Nov 2020 8:39:09pm
    Author:  eyal

  ==============================================================================
*/

#include "DeleteProjectMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    name: the project's name
*/
DeleteProjectsMessage::DeleteProjectsMessage(const std::string& name) :
    _name(name)
{
}

/*
    usage: message logic
*/
void DeleteProjectsMessage::operator()()
{
    Engine::getInstance()->deleteProject(this->_name);
}
