/*
  ==============================================================================

    CreateProjectMessage.cpp
    Created: 10 Nov 2020 8:40:05pm
    Author:  eyal

  ==============================================================================
*/

#include "CreateProjectMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: constructor
    projectData: the project's data
*/
CreateProjectsMessage::CreateProjectsMessage(Models::Project& projectData) :
    _projectData(projectData)
{
}

/*
    usage: message logic
*/
void CreateProjectsMessage::operator()()
{
    Engine::getInstance()->createProject(this->_projectData);
}
