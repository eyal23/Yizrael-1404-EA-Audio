/*
  ==============================================================================

    DeleteProjectMessage.h
    Created: 10 Nov 2020 8:39:09pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for deleting a project logical message
*/
class DeleteProjectsMessage : public LogicMessage
{
public:
    DeleteProjectsMessage(const std::string& name);

    void operator()() override; //message logic

private:
    const std::string _name;
};