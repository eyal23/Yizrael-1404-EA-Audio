/*
  ==============================================================================

    CloseProjectMessage.cpp
    Created: 10 Nov 2020 8:29:40pm
    Author:  eyal

  ==============================================================================
*/

#include "CloseProjectMessage.h"
#include "../../Engine/Engine.h"
#include "../../Audio/TimeLinePlayHead.h"


/*
    usage: message logic
*/
void CloseProjectMessage::operator()()
{
    Engine::getInstance()->closeProject();
    TimeLinePlayHead::getInstance()->transportPlay(false);
    TimeLinePlayHead::getInstance()->transportRewind();
}
