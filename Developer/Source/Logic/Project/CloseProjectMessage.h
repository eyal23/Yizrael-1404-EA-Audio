/*
  ==============================================================================

    CloseProjectMessage.h
    Created: 10 Nov 2020 8:29:40pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../LogicMessage.h"


/*
    a class for closing the opened project logical message
*/
class CloseProjectMessage : public LogicMessage
{
public:
    void operator()() override; //message logic
};