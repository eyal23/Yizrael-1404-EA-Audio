/*
  ==============================================================================

    RetriveProjectsMessage.cpp
    Created: 7 Nov 2020 6:14:29pm
    Author:  eyal

  ==============================================================================
*/

#include "RetrieveProjectsMessage.h"
#include "../../Engine/Engine.h"


/*
    usage: gets the message's future response
    return: the message's future response
*/
std::future<std::vector<Models::Project>> RetrieveProjectsMessage::getFutureResponse()
{
    return this->_logicResponse.get_future();
}

/*
    usage: message logic
*/
void RetrieveProjectsMessage::operator()()
{
    this->_logicResponse.set_value({ Engine::getInstance()->retrieveProjects() });
}
