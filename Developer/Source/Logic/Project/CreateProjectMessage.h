/*
  ==============================================================================

    CreateProjectMessage.h
    Created: 10 Nov 2020 8:40:05pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"
#include "../../Helper/Models.h"


/*
    a class for creating a project logical message
*/
class CreateProjectsMessage : public LogicMessage
{
public:
    CreateProjectsMessage(Models::Project& projectData);

    void operator()() override; //message logic

private:
    Models::Project _projectData; //the project's data
};