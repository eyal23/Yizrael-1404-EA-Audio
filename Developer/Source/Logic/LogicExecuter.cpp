/*
  ==============================================================================

    LogicExecuter.cpp
    Created: 7 Nov 2020 2:41:37pm
    Author:  eyal

  ==============================================================================
*/

#include <future>

#include "LogicExecuter.h"
#include "LogicMessage.h"

LogicExecuter* LogicExecuter::_instance{ nullptr };
std::mutex LogicExecuter::_mutex;
std::condition_variable LogicExecuter::_cond;
std::queue<LogicMessage*> LogicExecuter::_messages;
bool LogicExecuter::_shouldExecute{ true };


/*
    usage: constructor
*/
LogicExecuter::LogicExecuter() :
    juce::Thread("executer")
{
    //starts the execution thread
    startThread();
}

/*
    usage: destructor
*/
LogicExecuter::~LogicExecuter()
{
    //safely stops the execution thread
    this->stopExecuting();
}

/*
    usage: the execution thread
*/
void LogicExecuter::run()
{
    while (this->_shouldExecute)
    {
        std::unique_lock<std::mutex> locker(_mutex);

        //wait until there's a message to execute
        _cond.wait(locker, [this]() { return !_messages.empty() || !this->_shouldExecute; });
        if (this->_shouldExecute)
        {
            //gets the message to execute
            auto*& pMessage = _messages.front();
            _messages.pop();
            auto& message = *pMessage;

            //executes the message
            message();

            //deletes the message
            delete pMessage;
            locker.unlock();
        }
    }
}

/*
    usage: pushes a new logical message to the messages queue
    message: the new message
*/
void LogicExecuter::pushMessage(LogicMessage* message)
{
    std::unique_lock<std::mutex> locker(_mutex);
    this->_messages.push(message);
    _cond.notify_one();
}

/*
    usage: safely stops the execution thread
*/
void LogicExecuter::stopExecuting()
{
    if (this->isThreadRunning())
    {
        this->_shouldExecute = false;
        std::unique_lock<std::mutex> locker(_mutex);
        while (!this->_messages.empty())
        {
            delete this->_messages.front();
            this->_messages.pop();
        }
        locker.unlock();
        _cond.notify_one();

        this->stopThread(-1);
    }
}

/*
    usage: gets the class instance
    return: the class instance
*/
LogicExecuter* LogicExecuter::getInstance()
{
    if (_instance == nullptr)
    {
        _instance = new LogicExecuter();
    }

    return _instance;
}
