/*
  ==============================================================================

    LogicExecuter.h
    Created: 7 Nov 2020 2:41:37pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include <mutex>
#include <queue>
#include <condition_variable>

#include "LogicMessage.h"


/*
    a class for executing logical messages on a different thread (singletone)
*/
class LogicExecuter : private juce::Thread
{
public:
    //part of the singletone design pattern
    LogicExecuter(LogicExecuter& other) = delete;
    void operator=(const LogicExecuter& other) = delete;

    static LogicExecuter* getInstance();     //gets the class instance

    void pushMessage(LogicMessage* message); //pushes a logical message to the queue 
    void stopExecuting();                    //safely stops executing messages

private:
    LogicExecuter();
    ~LogicExecuter();

    void run() override; //the execution thread

private:
    static LogicExecuter* _instance;            //the class instance
    static std::mutex _mutex;                   //the class mutex
    static std::condition_variable _cond;       //the class condition variable
    static std::queue<LogicMessage*> _messages; //the logical messages queue
    static bool _shouldExecute;                 
};