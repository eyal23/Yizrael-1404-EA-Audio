/*
  ==============================================================================

    RewindMessage.h
    Created: 2 Jan 2021 1:31:39pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for rewind logical message
*/
class RewindMessage : public LogicMessage
{
public:
    void operator()() override; //message logic
};