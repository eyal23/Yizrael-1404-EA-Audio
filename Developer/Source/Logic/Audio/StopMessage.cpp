/*
  ==============================================================================

    StopMessage.cpp
    Created: 2 Jan 2021 1:31:23pm
    Author:  eyal

  ==============================================================================
*/

#include "StopMessage.h"
#include "../../Audio/TimeLinePlayHead.h"


/*
    usage: message logic
*/
void StopMessage::operator()()
{
    TimeLinePlayHead::getInstance()->transportPlay(false);
}
