/*
  ==============================================================================

    StopMessage.h
    Created: 2 Jan 2021 1:31:23pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for stop logical message
*/
class StopMessage : public LogicMessage
{
public:
    void operator()() override; //message logic
};