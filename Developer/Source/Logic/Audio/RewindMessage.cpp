/*
  ==============================================================================

    RewindMessage.cpp
    Created: 2 Jan 2021 1:31:39pm
    Author:  eyal

  ==============================================================================
*/

#include "RewindMessage.h"
#include "../../Audio/TimeLinePlayHead.h"


/*
    usage: message logic
*/
void RewindMessage::operator()()
{
    TimeLinePlayHead::getInstance()->transportRewind();
}
