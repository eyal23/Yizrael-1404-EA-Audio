/*
  ==============================================================================

    PlayMessage.cpp
    Created: 2 Jan 2021 1:31:09pm
    Author:  eyal

  ==============================================================================
*/

#include "PlayMessage.h"
#include "../../Audio/TimeLinePlayHead.h"


/*
    usage: message logic
*/
void PlayMessage::operator()()
{
    TimeLinePlayHead::getInstance()->transportPlay(true);
}
