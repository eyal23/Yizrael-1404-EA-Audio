/*
  ==============================================================================

    PlayMessage.h
    Created: 2 Jan 2021 1:31:09pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include "../LogicMessage.h"


/*
    a class for a play logical message
*/
class PlayMessage : public LogicMessage
{
public:
    void operator()() override; //message logic
};