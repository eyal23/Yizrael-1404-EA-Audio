/*
  ==============================================================================

    LogicMessage.hpp
    Created: 7 Nov 2020 6:33:25pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>


/*
    base class for all logical messages
*/
class LogicMessage
{
public:
    virtual void operator()() = 0; //message logic
};
