/*
  ==============================================================================

    MainMenuBar.h
    Created: 14 Nov 2020 7:01:45pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>


/*
    the main window's menu bar
*/
class MainMenuBar  : public juce::MenuBarModel
{
public:
    //all possible actions
    enum menuBarActions
    {
        fileMenu = 0,
        settingsMenu,
        numberOfActions,
        
    };

    MainMenuBar();
    ~MainMenuBar() override;

    juce::StringArray getMenuBarNames() override;                                                  //the names of the menu's options
    juce::PopupMenu getMenuForIndex(int topLevelMenuIndex, const juce::String& menuName) override; //gets menu by index
    void menuItemSelected(int menuItemID, int topLevelMenuIndex) override;                         //unused
    void launchPopUpWindow(const std::string& title, juce::Component* mainComponent);              //launches a pop up window

private:
    juce::Component::SafePointer<juce::DialogWindow> _currentlyPopedWindow; //the currently poped window
    juce::CachedValue<juce::String> _openedProjectName;                     //the currently opened project name

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainMenuBar)
};
