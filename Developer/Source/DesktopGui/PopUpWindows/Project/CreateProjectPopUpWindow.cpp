/*
  ==============================================================================

    createProjectPopUpWindow.cpp
    Created: 13 Nov 2020 12:07:51pm
    Author:  משתמש

  ==============================================================================
*/

#include <JuceHeader.h>

#include "createProjectPopUpWindow.h"
#include "../../../Logic/Project/CreateProjectMessage.h"
#include "../../../Logic/LogicExecuter.h"


/*
    usage: constructor
*/
CreateProjectPopUpWindow::CreateProjectPopUpWindow() :
    _header("Create Project")
{
    //adds and makes visible the header component
    addAndMakeVisible(this->_header);

    //adds and makes visible the input label header
    addAndMakeVisible(_inputLabelHeader);
    this->_inputLabelHeader.setText("Name:", juce::dontSendNotification);
    this->_inputLabelHeader.attachToComponent(&this->_inputLabel, true);
    this->_inputLabelHeader.setColour(juce::Label::textColourId, juce::Colours::orange);
    this->_inputLabelHeader.setJustificationType(juce::Justification::right);

    //adds and makes visible the create button
    addAndMakeVisible(this->_createButton);
    this->_createButton.setButtonText("Create");

    //adds and makes visible the input label
    addAndMakeVisible(this->_inputLabel);
    this->_inputLabel.setEditable(true);
    this->_inputLabel.setColour(juce::Label::backgroundColourId, juce::Colours::cornflowerblue);

    //listens to the create button
    this->_createButton.addListener(this);
    
    //resizes the component
    setSize(getParentWidth(), getParentHeight());
}

/*
    usage: paints the component
    g: the graghics object
*/
void CreateProjectPopUpWindow::paint (juce::Graphics& g)
{
    //fills the component with the "slate grey" color
    g.fillAll(juce::Colours::slategrey);
}

/*
    usage: resizes the component
*/
void CreateProjectPopUpWindow::resized()
{
    auto area = getLocalBounds();
    auto headerHeight = area.getHeight() * 0.15;

    //sets the child components sizes
    this->_header.setBounds(0, 0, area.getWidth(), headerHeight);
    area.removeFromTop(headerHeight);
    this->_createButton.setBounds(area.getX() + (area.getWidth() * 0.9), area.getY() + (area.getHeight() * 0.9), area.getWidth() * 0.1, area.getHeight() * 0.1);
    this->_inputLabel.setBounds(area.getX() + (area.getWidth() * 0.2), area.getY() + (area.getHeight() * 0.15), area.getWidth() * 0.75, 20);
}

/*
    usage: reacts to a click of a button
    button: the button which has been clicked
*/
void CreateProjectPopUpWindow::buttonClicked(juce::Button* button)
{
    //if the create button was clicked, create the project
    if (button == &this->_createButton)
    {
        const int MAXLEN = 80;
        char creationDate[MAXLEN];
        time_t t = time(0);
        strftime(creationDate, MAXLEN, "%m/%d/%Y", localtime(&t));

        LogicExecuter::getInstance()->pushMessage(new CreateProjectsMessage(Models::Project(
            this->_inputLabel.getText().toStdString(),
            creationDate
        )));

        //close the window
        ((juce::DialogWindow*)this->getParentComponent())->escapeKeyPressed();
    }
}