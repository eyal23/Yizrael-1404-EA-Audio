/*
  ==============================================================================

    DeleteProjectPopUpWindow.h
    Created: 13 Nov 2020 6:36:29pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../../ReusableComponents/PopUpWindowHeader.h"
#include "../../ReusableComponents/ButtonsFlexGroup.h"


/*
    the delete project pop up window's main component
*/
class DeleteProjectPopUpWindow  : public juce::Component
{
public:
    DeleteProjectPopUpWindow();

    void paint (juce::Graphics&) override; //paints the component
    void resized() override;               //resizes the component

private:
    PopUpWindowHeader _header;       //the header component
    ButtonsFlexGroup _deleteButtons; //the projects buttons

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (DeleteProjectPopUpWindow)
};
