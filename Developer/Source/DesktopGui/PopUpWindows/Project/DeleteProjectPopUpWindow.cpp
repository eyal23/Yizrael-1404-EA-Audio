/*
  ==============================================================================

    DeleteProjectPopUpWindow.cpp
    Created: 13 Nov 2020 6:36:29pm
    Author:  משתמש

  ==============================================================================
*/

#include <JuceHeader.h>

#include "DeleteProjectPopUpWindow.h"
#include "../../../Logic/Project/RetrieveProjectsMessage.h"
#include "../../../Logic/Project/DeleteProjectMessage.h"
#include "../../../Logic/LogicExecuter.h"


/*
    usage: constructor
*/
DeleteProjectPopUpWindow::DeleteProjectPopUpWindow() :
    _header("Delete Project")
{
    //adds and makes visible the header component
    addAndMakeVisible(this->_header);

    //retrieves all projects
    RetrieveProjectsMessage* msg = new RetrieveProjectsMessage();
    auto response = msg->getFutureResponse();
    LogicExecuter::getInstance()->pushMessage(msg);
    auto resInfo = response.get();

    juce::Array<juce::TextButton*> buttons;

    //for each project, map to a button
    for (auto& project : resInfo)
    {
        juce::TextButton* button = new juce::TextButton(project.name);

        //if the button is clicked, delete the project
        button->onClick = [this, project]() {
            LogicExecuter::getInstance()->pushMessage(new DeleteProjectsMessage(project.name));
            ((juce::DialogWindow*)this->getParentComponent())->escapeKeyPressed();
        };

        buttons.add(button);
    }

    //adds and makes visible the buttons
    this->_deleteButtons = buttons;
    addAndMakeVisible(this->_deleteButtons);

    //resizes the component
    setSize(getParentWidth(), getParentHeight());
}

/*
    usage: paints the component
    g: the graghics object
*/
void DeleteProjectPopUpWindow::paint (juce::Graphics& g)
{
    //fills the component with the "slate grey" color
    g.fillAll(juce::Colours::slategrey);
}

/*
    usage: resizes the component
*/
void DeleteProjectPopUpWindow::resized()
{
    auto area = getLocalBounds();
    auto headerHeight = area.getHeight() * 0.15;

    //resizes the header component
    this->_header.setBounds(0, 0, area.getWidth(), headerHeight);
    area.removeFromTop(headerHeight);

    //resizes the projects buttons
    this->_deleteButtons.setBounds(area.reduced(10));
}
