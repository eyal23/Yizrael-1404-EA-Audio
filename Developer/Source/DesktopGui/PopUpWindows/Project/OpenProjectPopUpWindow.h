/*
  ==============================================================================

    OpenProjectPopUpWindow.h
    Created: 13 Nov 2020 12:08:13pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../../ReusableComponents/PopUpWindowHeader.h"
#include "../../ReusableComponents/ButtonsFlexGroup.h"


/*
    the open project pop up window's main component
*/
class OpenProjectPopUpWindow  : public juce::Component
{
public:
    OpenProjectPopUpWindow();

    void paint (juce::Graphics&) override; //paints the component
    void resized() override;               //resizes the component

private:
    PopUpWindowHeader _header;     //the header component
    ButtonsFlexGroup _openButtons; //the projects buttons

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (OpenProjectPopUpWindow)
};
