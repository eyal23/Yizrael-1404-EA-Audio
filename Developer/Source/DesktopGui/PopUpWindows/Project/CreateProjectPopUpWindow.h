/*
  ==============================================================================

    createProjectPopUpWindow.h
    Created: 13 Nov 2020 12:07:51pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include <ctime>

#include "../../ReusableComponents/PopUpWindowHeader.h"


/*
    the create project pop up window's main component
*/
class CreateProjectPopUpWindow  : public juce::Component,
                                  private juce::Button::Listener
{
public:
    CreateProjectPopUpWindow();

    void paint (juce::Graphics&) override; //paints the component
    void resized() override;               //resizes the component

    void buttonClicked(juce::Button* button) override; //reacts to a buttons click
    
private:
    PopUpWindowHeader _header;      //the header component
    juce::Label _inputLabelHeader;  //the input label's header
    juce::Label _inputLabel;        //the input label
    juce::TextButton _createButton; //the create button
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (CreateProjectPopUpWindow)
};
