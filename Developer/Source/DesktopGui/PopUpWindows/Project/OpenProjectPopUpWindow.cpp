/*
  ==============================================================================

    OpenProjectPopUpWindow.cpp
    Created: 13 Nov 2020 12:08:13pm
    Author:  משתמש

  ==============================================================================
*/

#include <JuceHeader.h>

#include "OpenProjectPopUpWindow.h"
#include "../../../Logic/LogicExecuter.h"
#include "../../../Logic/Project/OpenProjectMessage.h"
#include "../../../Logic/Project/RetrieveProjectsMessage.h"


/*
    usage: constructor
*/
OpenProjectPopUpWindow::OpenProjectPopUpWindow() :
    _header("Open Project")
{
    //adds and makes visible the header component
    addAndMakeVisible(this->_header);

    //retrieves all projects
    RetrieveProjectsMessage* msg = new RetrieveProjectsMessage();
    auto response = msg->getFutureResponse();
    LogicExecuter::getInstance()->pushMessage(msg);
    auto resInfo = response.get();

    juce::Array<juce::TextButton*> buttons;

    //for each project, map to a button
    for (auto& project : resInfo)
    {
        juce::TextButton* button = new juce::TextButton(project.name);

        //if the button is clicked, open the project
        button->onClick = [this, project]() {
            LogicExecuter::getInstance()->pushMessage(new OpenProjectsMessage(project.name));
            ((juce::DialogWindow*)this->getParentComponent())->escapeKeyPressed();
        };

        buttons.add(button);
    }

    //adds and makes visible the project buttons
    this->_openButtons = buttons;
    addAndMakeVisible(this->_openButtons);

    //resizes the component
    setSize(getParentWidth(), getParentHeight());
}

/*
    usage: paints the component
    g: the graghics object
*/
void OpenProjectPopUpWindow::paint (juce::Graphics& g)
{
    //fills the component with the "slate grey" color
    g.fillAll(juce::Colours::slategrey);
}

/*
    usage: resizes the component
*/
void OpenProjectPopUpWindow::resized()
{
    auto area = getLocalBounds();
    auto headerHeight = area.getHeight() * 0.15;

    //resizes the header component
    this->_header.setBounds(0, 0, area.getWidth(), headerHeight);
    area.removeFromTop(headerHeight);

    //resizes the project buttons
    this->_openButtons.setBounds(area.reduced(10));
}

