/*
  ==============================================================================

    DeleteClipPopUpWindow.cpp
    Created: 5 Dec 2020 12:39:45pm
    Author:  eyal

  ==============================================================================
*/

#include "DeleteClipPopUpWindow.h"

#include <JuceHeader.h>
#include <vector>

#include "../../../Logic/LogicExecuter.h"
#include "../../../Logic/Track/RetrieveTrackMessage.h"
#include "../../../Logic/Clip/RetrieveClipMessage.h"
#include "../../../Logic/Clip/DeleteClipMessage.h"


/*
    usage: constructor
*/
DeleteClipPopUpWindow::DeleteClipPopUpWindow() :
    _header("Select Track"), _isSelecting(true)
{
    //adds and makes visible the header component
    addAndMakeVisible(this->_header);

    //retrieves all tracks of the opened project
    RetrieveTrackMessage* msg = new RetrieveTrackMessage();
    auto response = msg->getFutureResponse();
    LogicExecuter::getInstance()->pushMessage(msg);
    auto resInfo = response.get();

    juce::Array<juce::TextButton*> buttons;

    //for each track, map to a button
    for (auto& track : resInfo)
    {
        juce::TextButton* button = new juce::TextButton(track.name);

        //when the button is clicked, select the track
        button->onClick = [this, track]() {
            this->trackSelected(track.id);
        };

        buttons.add(button);
    }

    //adds and makes visible the tracks buttons
    this->_selectTrackButtons = buttons;
    addAndMakeVisible(this->_selectTrackButtons);
    
    setSize(getParentWidth(), getParentHeight());
}

/*
    usage: paints the component
    g: the graghics object
*/
void DeleteClipPopUpWindow::paint(juce::Graphics& g)
{
    //fills the component with the "slate grey" color
    g.fillAll(juce::Colours::slategrey);
}

/*
    usage: resizes the component
*/
void DeleteClipPopUpWindow::resized()
{
    auto area = getLocalBounds();
    auto headerHeight = area.getHeight() * 0.15;
    area.removeFromTop(headerHeight);

    //if currently selecting a track, resize the matching components
    if (this->_isSelecting)
    {
        //resize the header component
        this->_header.setBounds(0, 0, area.getWidth(), headerHeight);

        //resize the tracks buttons
        this->_selectTrackButtons.setBounds(area.reduced(10));
    }
    else
    {
        //resize the clips buttons
        this->_deleteClipButtons.setBounds(area.reduced(10));
    }
}

/*
    usage: refreshes the component when a track was selected
    trackId: the selected track's id
*/
void DeleteClipPopUpWindow::trackSelected(const int trackId)
{
    //removes the tracks buttons
    this->removeChildComponent(&this->_selectTrackButtons);

    this->_header.setTitle("Delete Clip");

    //retrieves the selected track's clips
    RetrieveClipMessage* msg = new RetrieveClipMessage(trackId);
    auto response = msg->getFutureResponse();
    LogicExecuter::getInstance()->pushMessage(msg);
    auto resInfo = response.get();

    juce::Array<juce::TextButton*> buttons;

    //for each clip, map to a button
    for (auto& clip : resInfo)
    {
        juce::TextButton* button = new juce::TextButton(clip.name);

        //if the button is clicked, delete the clip
        button->onClick = [this, clip, trackId]() {
            LogicExecuter::getInstance()->pushMessage(new DeleteClipMessage(clip.id, trackId));
            ((juce::DialogWindow*)this->getParentComponent())->escapeKeyPressed();
        };

        buttons.add(button);
    }

    //adds and makes visible the clips buttons
    this->_deleteClipButtons = buttons;
    this->_isSelecting = false;
    addAndMakeVisible(this->_deleteClipButtons);
    resized();
}
