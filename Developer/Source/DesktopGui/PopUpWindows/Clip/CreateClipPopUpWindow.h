/*
  ==============================================================================

    CreateClipPopUpWindow.h
    Created: 5 Dec 2020 12:39:56pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include <ctime>

#include "../../ReusableComponents/PopUpWindowHeader.h"
#include "../../ReusableComponents/ButtonsFlexGroup.h"


/*
    usage: the create clip pop up window main component
*/
class CreateClipPopUpWindow : public juce::Component,
                            private juce::Button::Listener
{
public:
    CreateClipPopUpWindow();

    void paint(juce::Graphics&) override;              //paints the component
    void resized() override;                           //resizes the component
    void buttonClicked(juce::Button* button) override; //the component was clicked

    void trackSelected(const int trackId); //refreshes the component when a track was selected

private:
    PopUpWindowHeader _header;            //the header component
    ButtonsFlexGroup _selectTrackButtons; //the buttons that selected a track
    juce::Label _nameInputLabelHeader;    //the header of the name input label
    juce::Label _nameInputLabel;          //the name input label
    juce::Label _startInputLabelHeader;   //the header of the start input label
    juce::Label _startInputLabel;         //the start input label
    juce::Label _lengthInputLabelHeader;  //the header of the length input label
    juce::Label _lengthInputLabel;        //the length input label
    juce::TextButton _browseFileButton;   //the browse audio file button
    juce::TextButton _createButton;       //the create button
    juce::FileChooser _fileChooser;       //the audio file chooser
    bool _isSelecting;                    //is the user currently selecting a track
    int _trackId;                         //the selected track id

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(CreateClipPopUpWindow)
};