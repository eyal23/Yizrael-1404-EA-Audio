/*
  ==============================================================================

    DeleteClipPopUpWindow.h
    Created: 5 Dec 2020 12:39:45pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../../ReusableComponents/PopUpWindowHeader.h"
#include "../../ReusableComponents/ButtonsFlexGroup.h"


/*
    the delete clip pop up window's main component
*/
class DeleteClipPopUpWindow : public juce::Component
{
public:
    DeleteClipPopUpWindow();

    void paint(juce::Graphics&) override; //paints the component
    void resized() override;              //resizes the component

    void trackSelected(const int trackId); //refreshes the component when a track was selected

private:
    PopUpWindowHeader _header;            //the header component
    ButtonsFlexGroup _selectTrackButtons; //the select tracks buttons
    ButtonsFlexGroup _deleteClipButtons;  //the select clips buttons
    bool _isSelecting;                    //is the user currently selecting a track

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(DeleteClipPopUpWindow)
};