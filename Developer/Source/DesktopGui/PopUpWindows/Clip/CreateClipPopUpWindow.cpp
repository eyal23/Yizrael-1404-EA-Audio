/*
  ==============================================================================

    CreateClipPopUpWindow.cpp
    Created: 5 Dec 2020 12:39:56pm
    Author:  eyal

  ==============================================================================
*/

#include <string>

#include "CreateClipPopUpWindow.h"
#include "../../../Logic/LogicExecuter.h"
#include "../../../Logic/Track/RetrieveTrackMessage.h"
#include "../../../Logic/Clip/CreateClipMessage.h"
#include "../../../Engine/Engine.h"


/*
    usage: constructor
*/
CreateClipPopUpWindow::CreateClipPopUpWindow() :
    _header("Create Clip"), _fileChooser("Select a file...", {}, "*.wav;*.mp3")
{
    //adds and makes visible the header component
    addAndMakeVisible(this->_header);

    //retrieves all tracks of the currently opened project
    RetrieveTrackMessage* msg = new RetrieveTrackMessage();
    auto response = msg->getFutureResponse();
    LogicExecuter::getInstance()->pushMessage(msg);
    auto resInfo = response.get();

    juce::Array<juce::TextButton*> buttons;

    //for each track, map to a button
    for (auto& track : resInfo)
    {
        juce::TextButton* button = new juce::TextButton(track.name);

        //if clicked, select the track
        button->onClick = [this, track]() {
            this->trackSelected(track.id);
        };

        buttons.add(button);
    }

    //adds and makes visible the buttons
    this->_selectTrackButtons = buttons;
    addAndMakeVisible(this->_selectTrackButtons);

    //sets the component's size
    setSize(getParentWidth(), getParentHeight());
}

/*
    usage: paints the component
    g: the graghics object
*/
void CreateClipPopUpWindow::paint(juce::Graphics& g)
{
    //fills the component with the "slate grey" color
    g.fillAll(juce::Colours::slategrey);
}

/*
    usage: resizes the component
*/
void CreateClipPopUpWindow::resized()
{
    auto area = getLocalBounds();
    auto headerHeight = area.getHeight() * 0.15;
    area.removeFromTop(headerHeight);

    //if selecting a track, set the matching component's sizes
    if (this->_isSelecting)
    {
        //sets the header component's size
        this->_header.setBounds(0, 0, area.getWidth(), headerHeight);

        //sets the tracks buttons size
        this->_selectTrackButtons.setBounds(area.reduced(10));
    }
    else
    {
        //sets the header component's size
        this->_header.setBounds(0, 0, area.getWidth(), headerHeight);

        //sets all input labels sizes
        auto headerToNameLabelHeight = (area.getHeight() * 0.15) + 20;
        this->_nameInputLabel.setBounds(area.getX() + (area.getWidth() * 0.2), area.getY() + (area.getHeight() * 0.15), area.getWidth() * 0.75, 20);
        this->_startInputLabel.setBounds(area.getX() + (area.getWidth() * 0.2), area.getY() + (area.getHeight() * 0.25), area.getWidth() * 0.75, 20);
        this->_lengthInputLabel.setBounds(area.getX() + (area.getWidth() * 0.2), area.getY() + (area.getHeight() * 0.35), area.getWidth() * 0.75, 20);
        this->_browseFileButton.setBounds(area.getX() + (area.getWidth() * 0.35), area.getY() + (area.getHeight() * 0.55), area.getWidth() * 0.3, area.getHeight() * 0.2);
        this->_createButton.setBounds(area.getX() + (area.getWidth() * 0.9), area.getY() + (area.getHeight() * 0.9), area.getWidth() * 0.1, area.getHeight() * 0.1);

        area.removeFromTop(headerToNameLabelHeight);
    }
}

/*
    usage: reacts to a click of a button
    button: the button which has been clicked
*/
void CreateClipPopUpWindow::buttonClicked(juce::Button* button)
{
    //checks if the create button was clicked
    if (button == &this->_createButton)
    {
        //creates the clip
        LogicExecuter::getInstance()->pushMessage(new CreateClipMessage(Models::Clip(
            -1,
            this->_nameInputLabel.getText().toStdString(),
            this->_fileChooser.getResult().getFullPathName().toStdString(),
            -1,
            std::stof(this->_lengthInputLabel.getText().toStdString()),
            std::stof(this->_startInputLabel.getText().toStdString()),
            this->_trackId
        )));
        
        //closes the window
        ((juce::DialogWindow*)this->getParentComponent())->escapeKeyPressed();
    }
    //checks if the browse file button was clicked
    else if (button == &this->_browseFileButton)
    {
        //browses for a file
        this->_fileChooser.browseForFileToOpen();
    }
}

/*
    usage: refreshes the component when a track was selected
    trackId: the selected track's id
*/
void CreateClipPopUpWindow::trackSelected(const int trackId)
{
    //removes all select track buttons
    this->removeChildComponent(&this->_selectTrackButtons);

    this->_header.setTitle("Create Clip");

    this->_isSelecting = false;
    this->_trackId = trackId;
    
    //setting up all input lables
    addAndMakeVisible(_nameInputLabelHeader);
    this->_nameInputLabelHeader.setText("Name:", juce::dontSendNotification);
    this->_nameInputLabelHeader.attachToComponent(&this->_nameInputLabel, true);
    this->_nameInputLabelHeader.setColour(juce::Label::textColourId, juce::Colours::orange);
    this->_nameInputLabelHeader.setJustificationType(juce::Justification::right);

    addAndMakeVisible(this->_createButton);
    this->_createButton.setButtonText("Create");

    addAndMakeVisible(this->_nameInputLabel);
    this->_nameInputLabel.setEditable(true);
    this->_nameInputLabel.setColour(juce::Label::backgroundColourId, juce::Colours::cornflowerblue);

    addAndMakeVisible(_startInputLabelHeader);
    this->_startInputLabelHeader.setText("Start Point:", juce::dontSendNotification);
    this->_startInputLabelHeader.attachToComponent(&this->_startInputLabel, true);
    this->_startInputLabelHeader.setColour(juce::Label::textColourId, juce::Colours::orange);
    this->_startInputLabelHeader.setJustificationType(juce::Justification::right);

    addAndMakeVisible(this->_startInputLabel);
    this->_startInputLabel.setEditable(true);
    this->_startInputLabel.setColour(juce::Label::backgroundColourId, juce::Colours::cornflowerblue);

    addAndMakeVisible(_lengthInputLabelHeader);
    this->_lengthInputLabelHeader.setText("Length:", juce::dontSendNotification);
    this->_lengthInputLabelHeader.attachToComponent(&this->_lengthInputLabel, true);
    this->_lengthInputLabelHeader.setColour(juce::Label::textColourId, juce::Colours::orange);
    this->_lengthInputLabelHeader.setJustificationType(juce::Justification::right);

    addAndMakeVisible(this->_lengthInputLabel);
    this->_lengthInputLabel.setEditable(true);
    this->_lengthInputLabel.setColour(juce::Label::backgroundColourId, juce::Colours::cornflowerblue);

    addAndMakeVisible(this->_browseFileButton);
    this->_browseFileButton.setButtonText("Browse File");

    //listening to the buttons
    this->_createButton.addListener(this);
    this->_browseFileButton.addListener(this);

    resized();
}
