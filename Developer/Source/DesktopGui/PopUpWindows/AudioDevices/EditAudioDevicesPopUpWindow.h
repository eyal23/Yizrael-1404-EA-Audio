/*
  ==============================================================================

    EditAudioDevicesComponent.h
    Created: 18 Dec 2020 8:14:55pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../../ReusableComponents/PopUpWindowHeader.h"


/*
    the edit audio devices pop up window main component
*/
class EditAudioDevicesPopUpWindow : public juce::Component
{
public:
    EditAudioDevicesPopUpWindow();

    void paint(juce::Graphics& g) override; //paints the component
    void resized() override;                //resizes the component

private:
    juce::AudioDeviceManager& _deviceManager;           //the audio device manager to edit
    PopUpWindowHeader _header;                          //the header component
    juce::AudioDeviceSelectorComponent _audioSetupComp; //the devices editor component

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(EditAudioDevicesPopUpWindow)
};
