/*
  ==============================================================================

    EditAudioDevicesComponent.cpp
    Created: 18 Dec 2020 8:14:55pm
    Author:  משתמש

  ==============================================================================
*/

#include "EditAudioDevicesPopUpWindow.h"
#include "../../../Engine/Engine.h"


/*
    usage: constructor
*/
EditAudioDevicesPopUpWindow::EditAudioDevicesPopUpWindow() :
    _header("Audio Devices"),
    _deviceManager(Engine::getInstance()->getDeviceManager()),
    _audioSetupComp(this->_deviceManager,
        0,     // minimum input channels
        256,   // maximum input channels
        0,     // minimum output channels
        256,   // maximum output channels
        false, // ability to select midi inputs
        false, // ability to select midi output device
        false, // treat channels as stereo pairs
        false  // hide advanced options
    )
{
    //adds child components and make them visible
    addAndMakeVisible(this->_header);
    addAndMakeVisible(this->_audioSetupComp);

    //sets the components size
    setSize(getParentWidth(), getParentHeight());
}

/*
    usage: paints the component
    g: the graphics object
*/
void EditAudioDevicesPopUpWindow::paint(juce::Graphics& g)
{
    //fills the component with the "slate grey" color
    g.fillAll(juce::Colours::slategrey);
}

/*
    usage: resizes the component
*/
void EditAudioDevicesPopUpWindow::resized()
{
    auto area = getLocalBounds();
    auto headerHeight = area.getHeight() * 0.15;
    area.removeFromTop(headerHeight);

    //sets the cild component's sizes
    this->_header.setBounds(0, 0, area.getWidth(), headerHeight);
    this->_audioSetupComp.setBounds(area.getX() + area.proportionOfWidth(0.1), area.getY() + area.proportionOfHeight(0.1), area.proportionOfWidth(0.8), area.proportionOfHeight(0.8));
}