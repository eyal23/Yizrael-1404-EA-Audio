/*
  ==============================================================================

    CreateTrackPopUpWindow.cpp
    Created: 28 Nov 2020 3:53:43pm
    Author:  eyal

  ==============================================================================
*/

#include "CreateTrackPopUpWindow.h"

#include "../../../Logic/LogicExecuter.h"
#include "../../../Logic/Track/CreateTrackMessage.h"


/*
    usage: constructor
*/
CreateTrackPopUpWindow::CreateTrackPopUpWindow() :
    _header("Create Track"), _colorSelector(juce::ColourSelector::showAlphaChannel | juce::ColourSelector::showColourAtTop | juce::ColourSelector::showColourspace)
{
    //adds and makes visible the header component
    addAndMakeVisible(this->_header);

    //adds and makes visible the input label header
    addAndMakeVisible(_inputLabelHeader);
    this->_inputLabelHeader.setText("Name:", juce::dontSendNotification);
    this->_inputLabelHeader.attachToComponent(&this->_inputLabel, true);
    this->_inputLabelHeader.setColour(juce::Label::textColourId, juce::Colours::orange);
    this->_inputLabelHeader.setJustificationType(juce::Justification::right);

    //adds and makes visible the create button
    addAndMakeVisible(this->_createButton);
    this->_createButton.setButtonText("Create");

    //adds and makes visible the input label
    addAndMakeVisible(this->_inputLabel);
    this->_inputLabel.setEditable(true);
    this->_inputLabel.setColour(juce::Label::backgroundColourId, juce::Colours::cornflowerblue);

    //adds and makes visible the color selector
    addAndMakeVisible(this->_colorSelector);
    this->_colorSelector.setCurrentColour(juce::Colours::black);
    this->_colorSelector.setColour(juce::ColourSelector::backgroundColourId, juce::Colours::transparentWhite);
    
    //listens to the create button
    this->_createButton.addListener(this);

    //resizes the component
    setSize(getParentWidth(), getParentHeight());
}

/*
    usage: paints the component
    g: the graghics object
*/
void CreateTrackPopUpWindow::paint(juce::Graphics& g)
{
    //fills the component with the "slate grey" color
    g.fillAll(juce::Colours::slategrey);
}

/*
    usage: resizes the component
*/
void CreateTrackPopUpWindow::resized()
{
    auto area = getLocalBounds();
    auto headerHeight = area.getHeight() * 0.15;

    //resizes the header component
    this->_header.setBounds(0, 0, area.getWidth(), headerHeight);
    area.removeFromTop(headerHeight);

    //resizes the input label
    auto headerToNameLabelHeight = (area.getHeight() * 0.15) + 20;
    this->_inputLabel.setBounds(area.getX() + (area.getWidth() * 0.2), area.getY() + (area.getHeight() * 0.15), area.getWidth() * 0.75, 20);

    //resizes the create button
    this->_createButton.setBounds(area.getX() + (area.getWidth() * 0.9), area.getY() + (area.getHeight() * 0.9), area.getWidth() * 0.1, area.getHeight() * 0.1);

    area.removeFromTop(headerToNameLabelHeight);

    //resizes the color selector
    this->_colorSelector.setBounds(area.getX() + (area.getWidth() * 0.3), area.getY() + (area.getHeight() * 0.1), area.getWidth() * 0.4, area.getHeight() * 0.8);
}

/*
    usage: reacts to a button click
    button: the button which has been clicked
*/
void CreateTrackPopUpWindow::buttonClicked(juce::Button* button)
{
    //if the create button was clicked, create the track
    if (button == &this->_createButton)
    {
        LogicExecuter::getInstance()->pushMessage(new CreateTrackMessage(Models::Track(
            -1,
            this->_inputLabel.getText().toStdString(),
            this->_colorSelector.getCurrentColour().toString().toStdString(),
            "",
            -1,
            -1,
            0
        )));
        
        //close the window
        ((juce::DialogWindow*)this->getParentComponent())->escapeKeyPressed();
    }
}