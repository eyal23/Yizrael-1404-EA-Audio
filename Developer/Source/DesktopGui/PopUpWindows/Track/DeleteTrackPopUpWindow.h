/*
  ==============================================================================

    DeleteTrackComponent.h
    Created: 28 Nov 2020 3:53:27pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../../ReusableComponents/PopUpWindowHeader.h"
#include "../../ReusableComponents/ButtonsFlexGroup.h"


/*
    the delete track pop up window's main component
*/
class DeleteTrackPopUpWindow : public juce::Component
{
public:
    DeleteTrackPopUpWindow();

    void paint(juce::Graphics&) override; //paints the component
    void resized() override;              //resizes the component

private:
    PopUpWindowHeader _header;       //the header component
    ButtonsFlexGroup _deleteButtons; //the tracks buttons

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(DeleteTrackPopUpWindow)
};