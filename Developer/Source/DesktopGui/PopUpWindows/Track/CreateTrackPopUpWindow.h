/*
  ==============================================================================

    CreateTrackPopUpWindow.h
    Created: 28 Nov 2020 3:53:43pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include <ctime>

#include "../../ReusableComponents/PopUpWindowHeader.h"


/*
    the create track pop window's main component
*/
class CreateTrackPopUpWindow : public juce::Component,
    private juce::Button::Listener
{
public:
    CreateTrackPopUpWindow();

    void paint(juce::Graphics&) override; //paints the component
    void resized() override;              //resizes the component

    void buttonClicked(juce::Button* button) override; //reacts to a button click

private:
    PopUpWindowHeader _header;           //the header component
    juce::Label _inputLabelHeader;       //the input label header
    juce::Label _inputLabel;             //the input label
    juce::TextButton _createButton;      //the create button
    juce::ColourSelector _colorSelector; //the color selector

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(CreateTrackPopUpWindow)
};