/*
  ==============================================================================

    DeleteTrackPopUpWindow.cpp
    Created: 28 Nov 2020 3:53:27pm
    Author:  eyal

  ==============================================================================
*/

#include "DeleteTrackPopUpWindow.h"

#include <JuceHeader.h>


#include "../../../Logic/LogicExecuter.h"
#include "../../../Logic/Project/RetrieveProjectsMessage.h"
#include "../../../Logic/Track/RetrieveTrackMessage.h"
#include "../../../Logic/Track/DeleteTrackMessage.h"


/*
    usage: constructor
*/
DeleteTrackPopUpWindow::DeleteTrackPopUpWindow() :
    _header("Delete Track")
{
    //adds and makes visible the header component
    addAndMakeVisible(this->_header);

    //retrieves the opened project's tracks
    RetrieveTrackMessage* msg = new RetrieveTrackMessage();
    auto response = msg->getFutureResponse();
    LogicExecuter::getInstance()->pushMessage(msg);
    auto resInfo = response.get();

    juce::Array<juce::TextButton*> buttons;

    //for each track, map to a button
    for (auto& track : resInfo)
    {
        juce::TextButton* button = new juce::TextButton(track.name);

        //if the button is clicked, delete the track
        button->onClick = [this, track]() {
            LogicExecuter::getInstance()->pushMessage(new DeleteTrackMessage(track.id));
            ((juce::DialogWindow*)this->getParentComponent())->escapeKeyPressed();
        };

        buttons.add(button);
    }

    //adds and makes visible the tracks buttons
    this->_deleteButtons = buttons;
    addAndMakeVisible(this->_deleteButtons);

    //resizes the component
    setSize(getParentWidth(), getParentHeight());
}

/*
    usage: paints the component
    g: the graghics object
*/
void DeleteTrackPopUpWindow::paint(juce::Graphics& g)
{
    //fills the component with the "slate grey" color
    g.fillAll(juce::Colours::slategrey);
}

/*
    usage: resizes the component
*/
void DeleteTrackPopUpWindow::resized()
{
    auto area = getLocalBounds();
    auto headerHeight = area.getHeight() * 0.15;

    //resizes the header component
    this->_header.setBounds(0, 0, area.getWidth(), headerHeight);
    area.removeFromTop(headerHeight);

    //resizes the tracks buttons
    this->_deleteButtons.setBounds(area.reduced(10));
}
