/*
  ==============================================================================

    MainWindow.h
    Created: 4 Nov 2020 8:24:53pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "MainComponent.h"
#include "MainMenuBar.h"


/*
    the app's main window
*/
class MainWindow : public juce::DocumentWindow
{
public:
    MainWindow(const juce::String& name);

    void closeButtonPressed() override;

private:
    std::unique_ptr<MainMenuBar> _menuBar; //the window's menu bar

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(MainWindow)
};