#include "MainComponent.h"
#include "../Engine/Engine.h"
#include "../Helper/Ids.h"
#include "../Helper/Gui.h"


/*
    usage: constructor
*/
MainComponent::MainComponent() :
    _engineState(Engine::getInstance()->addListener(this)),
    _openedProjectName(this->_engineState, IDs::Engine::openedProjectName, nullptr)
{
    //adds the actions section
    addChildComponent(this->_actionSection);
    this->_viewPort.setScrollBarsShown(false, true, false, false);

    //resizes the component
    setSize(this->getParentWidth(), this->getParentHeight());
}

/*
    Destructs a MainComponent.
*/
MainComponent::~MainComponent()
{
    //stops listening to changes from the engine's state
    Engine::getInstance()->removeListener(this);
}

/*
    usage: paints the component
    g: the graghics object
*/
void MainComponent::paint(juce::Graphics& g)
{
}

/*
    usage: resizes the component
*/
void MainComponent::resized()
{
    //if a project is opened, resize all sections
    if (this->_openedProjectName.get().toStdString() != "")
    {
        auto area = getLocalBounds();

        //resize the timeline section
        this->_timeLineSection->setBounds(
            area.withTrimmedBottom(area.proportionOfHeight(0.35))
        );

        this->_viewPort.setBounds(
            area.withTrimmedBottom(area.proportionOfHeight(0.1))
            .withTrimmedTop(area.proportionOfHeight(0.65))
        );

        //resize the mixer section
        this->_mixerSection->setBounds(
            area.withTrimmedBottom(area.proportionOfHeight(0.1))
            .withTrimmedTop(area.proportionOfHeight(0.65))
            .withWidth(this->_mixerSection->getTracksCount() * Gui::getTrackMixerWidth())
        );

        this->_viewPort.setViewedComponent(this->_mixerSection, true);

        //resize the actions section
        this->_actionSection.setBounds(
            area.withTrimmedTop(area.proportionOfHeight(0.9))
        );
    }
}

/*
    usage: the opened project has changed
*/
void MainComponent::projectChanged()
{
    //if a project is opened, set up all sections
    if (this->_openedProjectName.get().toStdString() != "")
    {
        //set up the timeline section
        this->_timeLineSection.reset(new TimeLineSection());
        addAndMakeVisible(this->_timeLineSection.get());

        //set up the mixer section
        this->_mixerSection = new MixerSection();
        addAndMakeVisible(this->_viewPort);

        //set up the actions section
        this->_actionSection.setVisible(true);
        resized();
    }
    //if a project is not opened, clear all sections
    else
    {
        this->_timeLineSection.reset();
        this->_actionSection.setVisible(false);
        this->_viewPort.setViewedComponent(nullptr);
    }
}

/*
    usage: callback to when a broadcasting ValueTree's property changed
    treeWhosePropertyHasChanged: the tree whose property has changed
    property: the property that has changed
*/
void MainComponent::valueTreePropertyChanged(juce::ValueTree& treeWhosePropertyHasChanged, const juce::Identifier& property)
{
    //if the opened project has changed, refresh the component
    if (treeWhosePropertyHasChanged == this->_engineState)
    {
        juce::MessageManager::callAsync([=]() {
            this->projectChanged();
        });
    }
}
