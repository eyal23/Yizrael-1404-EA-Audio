/*
  ==============================================================================

    PopUpWindowHeader.h
    Created: 17 Nov 2020 10:32:51am
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>


/*
    the pop up window's header
*/
class PopUpWindowHeader  : public juce::Component
{
public:
    PopUpWindowHeader(const std::string& title);

    void paint (juce::Graphics&) override; //paints the component
    void resized() override;               //resizes the component

    void setTitle(const std::string& title);

private:
    juce::Label _titleLabel; //the title label

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PopUpWindowHeader)
};
