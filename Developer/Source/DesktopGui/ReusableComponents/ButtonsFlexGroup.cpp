/*
  ==============================================================================

    ButtonsFlexGroup.cpp
    Created: 15 Nov 2020 1:03:45pm
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "ButtonsFlexGroup.h"


/*
    usage: constructor
    buttons: the buttons to gather
*/
ButtonsFlexGroup::ButtonsFlexGroup(const juce::Array<juce::TextButton*>& buttons)
{
    *this = buttons;
}

/*
    usage: constructor
*/
ButtonsFlexGroup::ButtonsFlexGroup()
{
}

/*
    usage: sets the buttons of the group
    buttons: the buttons to gather
    return: this
*/
ButtonsFlexGroup& ButtonsFlexGroup::operator=(const juce::Array<juce::TextButton*>& buttons)
{
    //for each button, add and make visible
    for (auto button : buttons)
    {
        addAndMakeVisible(this->_buttons.add(button));
    }

    return *this;
}

/*
    usage: paints the component
    g: the graghics object
*/
void ButtonsFlexGroup::paint (juce::Graphics& g)
{
}

/*
    usage: resizes the component
*/
void ButtonsFlexGroup::resized()
{
    juce::FlexBox flexBox;

    //setting up the flex box
    flexBox.flexWrap = juce::FlexBox::Wrap::wrap;
    flexBox.flexDirection = juce::FlexBox::Direction::row;
    flexBox.justifyContent = juce::FlexBox::JustifyContent::flexStart;
    flexBox.alignContent = juce::FlexBox::AlignContent::flexStart;

    //for each button, set up it's flex properties
    for (auto* button : this->_buttons)
    {
        juce::FlexItem buttonFlexItem(*button);
        buttonFlexItem.minWidth = 65.0f;
        buttonFlexItem.minHeight = 65.0f;
        buttonFlexItem.maxWidth = 130.0f;
        buttonFlexItem.maxHeight = 65.0f;
        buttonFlexItem.margin = 5;
        buttonFlexItem.flexGrow = 1.0f;

        flexBox.items.add(buttonFlexItem);
    }

    flexBox.performLayout(getBounds().toFloat());
}
