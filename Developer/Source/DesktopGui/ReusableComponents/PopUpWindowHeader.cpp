/*
  ==============================================================================

    PopUpWindowHeader.cpp
    Created: 17 Nov 2020 10:32:51am
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "PopUpWindowHeader.h"


/*
    usage: constructor
    title: the title of the header
*/
PopUpWindowHeader::PopUpWindowHeader(const std::string& title)
{
    //adds and makes visible the title label
    addAndMakeVisible(this->_titleLabel);
    this->_titleLabel.setFont(juce::Font(30.0f, juce::Font::bold));
    this->_titleLabel.setText(title, juce::dontSendNotification);
    this->_titleLabel.setColour(juce::Label::textColourId, juce::Colours::lightgreen);
    this->_titleLabel.setJustificationType(juce::Justification::centred);
}

/*
    usage: paints the component
    g: the graghics object
*/
void PopUpWindowHeader::paint (juce::Graphics& g)
{
    //fills the component with the "dark slate grey" color
    g.fillAll(juce::Colours::darkslategrey);
}

/*
    usage: resizes the component
*/
void PopUpWindowHeader::resized()
{
    //resizes the title label
    this->_titleLabel.setBounds(10, 10, getWidth() - 20, getHeight() - 20);
}

/*
    usage: sets the title label
    title: the title
*/
void PopUpWindowHeader::setTitle(const std::string& title)
{
    this->_titleLabel.setText(title, juce::dontSendNotification);
}
