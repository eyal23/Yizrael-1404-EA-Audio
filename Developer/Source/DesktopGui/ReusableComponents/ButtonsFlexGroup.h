/*
  ==============================================================================

    ButtonsFlexGroup.h
    Created: 15 Nov 2020 1:03:45pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>


/*
    gathers buttons into flex box
*/
class ButtonsFlexGroup  : public juce::Component
{
public:
    ButtonsFlexGroup(const juce::Array<juce::TextButton*>& buttons);
    ButtonsFlexGroup();

    ButtonsFlexGroup& operator=(const juce::Array<juce::TextButton*>& buttons);

    void paint (juce::Graphics&) override; //paints the component
    void resized() override;               //resizes the component

private:
    juce::OwnedArray<juce::TextButton> _buttons; //the buttons

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ButtonsFlexGroup)
};
