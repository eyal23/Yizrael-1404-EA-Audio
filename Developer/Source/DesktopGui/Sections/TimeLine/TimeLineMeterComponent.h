/*
  ==============================================================================

    TimeLineMeterComponent.h
    Created: 4 Dec 2020 3:19:42pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>


/*
    a meter representing the the timeline
*/
class TimeLineMeterComponent  : public juce::Component
{
public:
    TimeLineMeterComponent(juce::ListBox& listBox);

    void paint (juce::Graphics&) override; //paints the component
    void resized() override;               //resizes the component

private:
    juce::ListBox& _listBox; //the timeline section's listbox

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TimeLineMeterComponent)
};
