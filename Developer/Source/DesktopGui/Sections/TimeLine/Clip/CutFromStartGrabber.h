/*
  ==============================================================================

    CutFromStartGrabber.h
    Created: 13 Jan 2021 10:11:13pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../../../../Helper/Models.h"


/*
    the clip timeline component's cut from start grabber
*/
class CutFromStartGrabber  : public juce::Component
{
public:
    CutFromStartGrabber(const Models::Clip& clipData);

    void paint (juce::Graphics&) override; //paints the component
    void resized() override;               //resizes the component

    void mouseDrag(const juce::MouseEvent& event) override; //callback to when the mouse was dragged on the component
    void mouseUp(const juce::MouseEvent& event) override;   //callback to when the mouse was lifted up from the component

    void setStart(const float start);
    void setLength(const float length);

private:
    Models::Clip _clipData; //the clip's data

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (CutFromStartGrabber)
};
