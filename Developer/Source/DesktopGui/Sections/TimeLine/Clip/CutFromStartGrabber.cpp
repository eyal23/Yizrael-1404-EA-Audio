/*
  ==============================================================================

    CutFromStartGrabber.cpp
    Created: 13 Jan 2021 10:11:13pm
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "CutFromStartGrabber.h"
#include "ClipTimeLineComponent.h"
#include "../../../../Logic/LogicExecuter.h"
#include "../../../../Logic/Clip/ChangeClipStartingSecondMessage.h"
#include "../../../../Logic/Clip/ChangeClipLengthMessage.h"
#include "../../../../Helper/Gui.h"


/*
    usage: constructor
    clipData: the clip's data
*/
CutFromStartGrabber::CutFromStartGrabber(const Models::Clip& clipData) :
    _clipData(clipData)
{
    //the component's mouse clicks properties
    this->setInterceptsMouseClicks(true, true);
    this->setWantsKeyboardFocus(true);
    this->setMouseCursor(juce::MouseCursor(juce::MouseCursor::DraggingHandCursor));
}

/*
    usage: paints the component
    g: the graphics object
*/
void CutFromStartGrabber::paint (juce::Graphics& g)
{
    //fills the component with a variant of the "black" color
    g.fillAll(juce::Colours::black.withAlpha(0.9f));

    //draws two lines
    g.setColour(juce::Colours::lightgrey.withMultipliedBrightness(1.75));

    g.drawLine(
        this->getWidth() * 0.52, this->getHeight() * 0.4,
        this->getWidth() * 0.52, this->getHeight() * 0.6
    );

    g.drawLine(
        this->getWidth() * 0.35, this->getHeight() * 0.45,
        this->getWidth() * 0.35, this->getHeight() * 0.55
    );
}

/*
    usage: resizes the component
*/
void CutFromStartGrabber::resized()
{

}

/*
    usage: callback to when the mouse was dragged on the component
    event: unused
*/
void CutFromStartGrabber::mouseDrag(const juce::MouseEvent& event)
{
    //casts the parent component to a clip timeline component
    ClipTimeLineComponent* parentComponent = (ClipTimeLineComponent*)this->getParentComponent();
    float mouseXBySeconds = parentComponent->getMouseXYRelative().getX() / Gui::getTimeLineSpaceBetweenSeconds();
    bool hasMovedToRight = true;

    //if the clip hasn't cut enough, dont move it
    if (this->_clipData.length - mouseXBySeconds > this->_clipData.maxLength + 0.25 || std::abs(mouseXBySeconds) < 0.25)
    {
        return;
    }

    //if the clip was cut to the left, increment it's starting second, and decrement it's length by a quarter second
    if (mouseXBySeconds > 0)
    {
        this->_clipData.length -= 0.25;
        this->_clipData.start += 0.25;
    }
    //if the clip was cut to the left, increment it's length, and decrement it's starting second by a quarter second
    else
    {
        hasMovedToRight = false;
        this->_clipData.length += 0.25;
        this->_clipData.start -= 0.25;
    }

    //resize the clip component
    if (hasMovedToRight)
    {
        parentComponent->setBounds(parentComponent->getBounds().withTrimmedLeft(0.25 * Gui::getTimeLineSpaceBetweenSeconds())); 
    }
    else
    {
        auto area = parentComponent->getBounds();
        area.translate((-0.25) * Gui::getTimeLineSpaceBetweenSeconds(), 0);
        area.setWidth(area.getWidth() + (0.25 * Gui::getTimeLineSpaceBetweenSeconds()));

        parentComponent->setBounds(area);
    }

    parentComponent->setStartSecond(this->_clipData.start);
    parentComponent->setLength(this->_clipData.length);
}

/*
    usage: callback to when the mouse was lifted up from the component
    event: unused
*/
void CutFromStartGrabber::mouseUp(const juce::MouseEvent& event)
{
    //casts the parent component to a clip timeline component
    ClipTimeLineComponent* parentComponent = (ClipTimeLineComponent*)this->getParentComponent();

    //sets the clip's starting second and length
    LogicExecuter::getInstance()->pushMessage(new ChangeClipStartingSecondMessage(this->_clipData.start, this->_clipData.trackId, this->_clipData.id));
    LogicExecuter::getInstance()->pushMessage(new ChangeClipLengthMessage(this->_clipData.length, this->_clipData.trackId, this->_clipData.id));
}

/*
    usage: sets the clip's starting second
    start: the clip's starting second
*/
void CutFromStartGrabber::setStart(const float start)
{
    this->_clipData.start = start;
}

/*
    usage: sets the clip's length
    length: the clip's length
*/
void CutFromStartGrabber::setLength(const float length)
{
    this->_clipData.length = length;
}
