/*
  ==============================================================================

    ClipTimeLineComponent.cpp
    Created: 4 Dec 2020 10:25:29am
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "ClipTimeLineComponent.h"
#include "../../../../Logic/LogicExecuter.h"
#include "../../../../Logic/Clip/DeleteClipMessage.h"
#include "../../../../Helper/Gui.h"
#include "../Source/DesktopGui/Sections/Actions/SelectorSyncer.h"

/*
    usage: constructor
    clipData: the clip's data
    color: the clip's color
*/
ClipTimeLineComponent::ClipTimeLineComponent(const Models::Clip& clipData, const std::string& colour) :
    _clipData(clipData),
    _colour(colour), 
    _header(clipData.name),
    _cutFromEndGrabber(clipData),
    _cutFromStartGrabber(clipData),
    _thumbnailCache(5), 
    _thumbnail(512, this->_formatManager, this->_thumbnailCache)
{
    this->_formatManager.registerBasicFormats();

    //the component's mouse clicks properties
    this->setInterceptsMouseClicks(true, true);
    this->setWantsKeyboardFocus(true);

    //sets up the audio thumbnail
    juce::File audioFile(this->_clipData.path);
    this->_thumbnail.setSource(new juce::FileInputSource(audioFile));

    //adds and makes visible all child components
    addAndMakeVisible(this->_header);
    addAndMakeVisible(this->_cutFromEndGrabber);
    addAndMakeVisible(this->_cutFromStartGrabber);
}

/*
    usage: paints the component
    g: the graphics object
*/
void ClipTimeLineComponent::paint (juce::Graphics& g)
{
    //fills the component with the clip's color
    g.fillAll(juce::Colour::fromString(this->_colour).withAlpha(0.6f));

    //draws the component's borders
    g.setColour(juce::Colours::black.withAlpha(0.0f));
    g.drawRect(getLocalBounds(), 2.0);

    //draws the audio thumbnail
    g.setColour(juce::Colour::fromString(this->_colour).withMultipliedBrightness(3));
    this->_thumbnail.drawChannels(g, getLocalBounds().reduced(2), 0.0, this->_clipData.length, 1.0f);
}

/*
    usage: resizes the component
*/
void ClipTimeLineComponent::resized()
{
    //resizes the header component
    this->_header.setBounds(getLocalBounds().withTrimmedTop(getHeight() * 0.75).withTrimmedRight(Gui::getTimeLineSpaceBetweenSeconds() / 6).withTrimmedLeft(Gui::getTimeLineSpaceBetweenSeconds() / 6));

    //resizes the cut from end grabber component
    auto cutFromEndArea = getLocalBounds().withTrimmedLeft(getWidth() - (Gui::getTimeLineSpaceBetweenSeconds() / 6));
    this->_cutFromEndGrabber.setBounds(cutFromEndArea);

    //resizes the cut from start grabber component
    auto cutFromStartArea = getLocalBounds().withTrimmedRight(getWidth() - (Gui::getTimeLineSpaceBetweenSeconds() / 6));
    this->_cutFromStartGrabber.setBounds(cutFromStartArea);
}

/*
    usage: sets the clip's data
    clipData: the clip's data
*/
void ClipTimeLineComponent::setData(const Models::Clip& clipData)
{
    this->_clipData = clipData;
    this->_header.setName(clipData.name);
    this->_cutFromEndGrabber.setStart(clipData.start);
    this->_cutFromEndGrabber.setLength(clipData.length);
    this->_cutFromStartGrabber.setStart(clipData.start);
    this->_cutFromStartGrabber.setLength(clipData.length);

    repaint();
}

/*
    usage: sets the clip's color
    clipData: the clip's color
*/
void ClipTimeLineComponent::setColor(const std::string& color)
{
    this->_colour = color;

    repaint();
}

/*
    usage: sets the clip's length
    clipData: the clip's length
*/
void ClipTimeLineComponent::setLength(const float length)
{
    this->_clipData.length = length;
    this->_cutFromEndGrabber.setLength(length);
    this->_cutFromStartGrabber.setLength(length);
    repaint();
}

/*
    usage: sets the clip's starting second
    clipData: the clip's starting second
*/
void ClipTimeLineComponent::setStartSecond(const float start)
{
    this->_clipData.start = start;
    this->_cutFromEndGrabber.setStart(start);
    this->_cutFromStartGrabber.setStart(start);
}

/*
    usage: gets the clip's data
    return: the clip's data
*/
Models::Clip ClipTimeLineComponent::getData() const
{
    return this->_clipData;
}

/*
    usage: gets the clip's color
    return: the clip's color
*/
std::string ClipTimeLineComponent::getColor() const
{
    return this->_colour;
}

/*
    usage: gets the clip's current grab position
    return: the clip's current grab position
*/
int ClipTimeLineComponent::getCurrentGrabPosition() const
{
    return this->_header.getCurrentGrabPosition();
}

/*
    usage: callback to when the mouse is down on the component
    event: unused
*/
void ClipTimeLineComponent::mouseDown(const juce::MouseEvent& event)
{
    //sets the clip as selected
    SelectorSyncer::getInstance()->selectClip(this->_clipData);
}
