/*
  ==============================================================================

    CutFromEndGrabber.cpp
    Created: 13 Jan 2021 4:42:25pm
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "ClipTimeLineComponent.h"
#include "CutFromEndGrabber.h"
#include "../../../../Logic/LogicExecuter.h"
#include "../../../../Logic/Clip/ChangeClipLengthMessage.h"
#include "../../../../Helper/Gui.h"


/*
    usage: constructor
    clipData: teh clip's data
*/
CutFromEndGrabber::CutFromEndGrabber(const Models::Clip& clipData) :
    _clipData(clipData)
{
    //the component's mouse clicks properties
    this->setInterceptsMouseClicks(true, true);
    this->setWantsKeyboardFocus(true);
    this->setMouseCursor(juce::MouseCursor(juce::MouseCursor::DraggingHandCursor));
}

/*
    usage: paints the component
    g: the graphics object
*/
void CutFromEndGrabber::paint (juce::Graphics& g)
{
    //fills the component with a variant of the "black" color
    g.fillAll(juce::Colours::black.withAlpha(0.9f));

    //draws two lines
    g.setColour(juce::Colours::lightgrey.withMultipliedBrightness(1.75));

    g.drawLine(
        this->getWidth() * 0.48, this->getHeight() * 0.4,
        this->getWidth() * 0.48, this->getHeight() * 0.6
    );

    g.drawLine(
        this->getWidth() * 0.65, this->getHeight() * 0.45,
        this->getWidth() * 0.65, this->getHeight() * 0.55
    );
}

/*
    usage: resizes the component
*/
void CutFromEndGrabber::resized()
{
}

/*
    usage: callback to when the mouse drags on the component
    event: unused
*/
void CutFromEndGrabber::mouseDrag(const juce::MouseEvent& event)
{
    //casts parent component to a clip timeline component
    ClipTimeLineComponent* parentComponent = (ClipTimeLineComponent*)this->getParentComponent();
    float mouseXBySeconds = parentComponent->getMouseXYRelative().getX() / Gui::getTimeLineSpaceBetweenSeconds();
    bool hasMovedToRight = true;

    //if has'nt dragged enough, dont move the clip component
    if (mouseXBySeconds > this->_clipData.maxLength + 0.25 || std::abs(this->_clipData.length - mouseXBySeconds) < 0.25)
    {
        return;
    }

    //if the clip has been cut to the right, decrement it's length by quarter second
    if (this->_clipData.length - mouseXBySeconds > 0)
    {
        hasMovedToRight = false;
        this->_clipData.length -= 0.25;
    }
    //if the clip has been cut to the left, increment it's length by quarter second
    else
    {
        this->_clipData.length += 0.25;
    }

    //resize the clip component
    if (hasMovedToRight)
    {
        auto area = parentComponent->getBounds();
        area.setWidth(area.getWidth() + (0.25 * Gui::getTimeLineSpaceBetweenSeconds()));

        parentComponent->setBounds(area);
    }
    else
    {
        parentComponent->setBounds(parentComponent->getBounds().withTrimmedRight(0.25 * Gui::getTimeLineSpaceBetweenSeconds()));
    }

    parentComponent->setLength(this->_clipData.length);
}

/*
    usage: callback when the mouse lifts up from the component
    event: unused
*/
void CutFromEndGrabber::mouseUp(const juce::MouseEvent& event)
{
    //sets the clip's new length
    LogicExecuter::getInstance()->pushMessage(new ChangeClipLengthMessage(this->_clipData.length, this->_clipData.trackId, this->_clipData.id));
}

/*
    usage: sets the starting second
    start: the new startin second
*/
void CutFromEndGrabber::setStart(const float start)
{
    this->_clipData.start = start;
}

/*
    usage: sets the length
    length: the new lenth
*/
void CutFromEndGrabber::setLength(const float length)
{
    this->_clipData.length = length;
}

