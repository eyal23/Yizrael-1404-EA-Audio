/*
  ==============================================================================

    ClipTimeLineHeaderComponent.h
    Created: 4 Dec 2020 10:29:56am
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>


/*
    the clip timeline component's header
*/
class ClipTimeLineHeaderComponent  : public juce::Component
{
public:
    ClipTimeLineHeaderComponent(const std::string& name);

    void paint (juce::Graphics&) override; //paints the component
    void resized() override;               //resizes the component

    void setName(const std::string& name);
    int getCurrentGrabPosition() const;

    void mouseDown(const juce::MouseEvent& event) override; //callback to when the mouse is down on the component

private:
    int _currentGrabPosition; //the current grab position
    juce::Label _nameLabel;   //the name label

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ClipTimeLineHeaderComponent)
};
