/*
  ==============================================================================

    ClipTimeLineHeaderComponent.cpp
    Created: 4 Dec 2020 10:29:56am
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "ClipTimeLineHeaderComponent.h"


/*
    usage: constructor
    name: the name
*/
ClipTimeLineHeaderComponent::ClipTimeLineHeaderComponent(const std::string& name)
{
    //the components mouse clicks properties
    this->setInterceptsMouseClicks(true, true);
    this->setWantsKeyboardFocus(true);

    //adds ans makes visible the name label
    addAndMakeVisible(this->_nameLabel);
    this->_nameLabel.setText(name, juce::dontSendNotification);
    this->_nameLabel.setFont(juce::Font(15.0, juce::Font::bold));
    this->_nameLabel.setColour(juce::Label::textColourId, juce::Colours::white);
    this->_nameLabel.setJustificationType(juce::Justification::centred);
    this->_nameLabel.setMouseCursor(juce::MouseCursor(juce::MouseCursor::DraggingHandCursor));
    this->_nameLabel.addMouseListener(this, false);
}

/*
    usage: paints the component
    g: the graphics object
*/
void ClipTimeLineHeaderComponent::paint (juce::Graphics& g)
{
    //fills the component with a variant of the "black" color
    g.fillAll(juce::Colours::black.withAlpha(0.7f));
}

/*
    usage: resizes the component
*/
void ClipTimeLineHeaderComponent::resized()
{
    //resizes the name label
    this->_nameLabel.setBounds(getLocalBounds());
}

/*
    usage: callback to when the mouse is down on the component
    event: unused
*/
void ClipTimeLineHeaderComponent::mouseDown(const juce::MouseEvent& event)
{
    //finds where the component is currently "dropped" at 
    juce::DragAndDropContainer* dragContainer = juce::DragAndDropContainer::findParentDragContainerFor(this);

    //starts dragging the clip timeline component
    this->_currentGrabPosition = this->getParentComponent()->getMouseXYRelative().getX();
    dragContainer->startDragging("", this->getParentComponent());
    dragContainer->setCurrentDragImage(juce::Image());
}

/*
    usage: sets the name
    name: the name
*/
void ClipTimeLineHeaderComponent::setName(const std::string& name)
{
    this->_nameLabel.setText(name, juce::dontSendNotification);
}

/*
    usage: gets the current grab position
    return: the current grab position
*/
int ClipTimeLineHeaderComponent::getCurrentGrabPosition() const
{
    return this->_currentGrabPosition;
}
