/*
  ==============================================================================

    ClipTimeLineComponent.h
    Created: 4 Dec 2020 10:25:29am
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "ClipTimeLineHeaderComponent.h"
#include "CutFromEndGrabber.h"
#include "CutFromStartGrabber.h"
#include "../../../../Helper/Models.h"


/*
    the timeline section clip component
*/
class ClipTimeLineComponent  : public juce::Component
{
public:
    ClipTimeLineComponent(const Models::Clip& clipData, const std::string& colour);

    void paint (juce::Graphics&) override; //paints the component
    void resized() override;               //resizes the component

    void setData(const Models::Clip& clipData);
    void setColor(const std::string& color);
    void setLength(const float length);
    void setStartSecond(const float start);
    Models::Clip getData() const;
    std::string getColor() const;
    int getCurrentGrabPosition() const;

    void mouseDown(const juce::MouseEvent& event) override; //callback to when the mouse is down on the component

private:
    Models::Clip _clipData;                    //the clip's data
    std::string _colour;                       //the clip's color
    ClipTimeLineHeaderComponent _header;       //the header component
    CutFromEndGrabber _cutFromEndGrabber;      //the cut from end grabber component
    CutFromStartGrabber _cutFromStartGrabber;  //the cut from start grabber component
    juce::AudioFormatManager _formatManager;   //the format manager
    juce::AudioThumbnailCache _thumbnailCache; //internal object
    juce::AudioThumbnail _thumbnail;           //the clip's audio thumbnail (visual representation)

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ClipTimeLineComponent)
};
