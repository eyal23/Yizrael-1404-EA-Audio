/*
  ==============================================================================

    TimeLineSection.cpp
    Created: 26 Nov 2020 8:47:08pm
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "TimeLineSection.h"
#include "Track/TrackTimeLineComponent.h"
#include "TimeLineMeterComponent.h"
#include "../../../Helper/Ids.h"
#include "../../../Helper/Gui.h"
#include "../../../Engine/Engine.h"
#include "../../../Logic/Track/DeleteTrackMessage.h"
#include "../../../Logic/Track/RetrieveTrackMessage.h"
#include "../../../Logic/LogicExecuter.h"


/*
    usage: constructor
*/
TimeLineSection::TimeLineSection() :
    _listBox("Tracks List Box", nullptr),
    _projectState(Engine::getInstance()->getOpenedProjectObject()->addListener(this)),
    _projectName(this->_projectState, IDs::Project::name, nullptr),
    _timeLineLength(this->_projectState, IDs::Project::timeLineLength, nullptr)
{
    //adds and makes visible the listbox
    addAndMakeVisible(this->_listBox);
    this->_listBox.setModel(this);
    this->_listBox.getViewport()->setScrollBarsShown(true, true);

    loadTracksData();
}

/*
    usage: destructor
*/
TimeLineSection::~TimeLineSection()
{
    auto* projectObject = Engine::getInstance()->getOpenedProjectObject();

    //if listening to a track, stop
    if (projectObject != nullptr)
    {
        projectObject->removeListener(this);
    }
}

/*
    usage: paints the component
    g: the graphics object
*/
void TimeLineSection::paint(juce::Graphics& g)
{
}

/*
    usage: resizes the component
*/
void TimeLineSection::resized()
{
    //sets up the list box and it's rows
    this->_listBox.setBounds(this->getLocalBounds());
    this->_listBox.setRowHeight(150);
    int newWidth = this->_tracks.size() == 0 ? 0 : (this->_timeLineLength.get() + 2) * Gui::getTimeLineSpaceBetweenSeconds();
    this->_listBox.setMinimumContentWidth(newWidth);
}

/*
    usage: gets the number of rows in the listbox (track count)
    retrun: the number of rows in the listbox (track count)
*/
int TimeLineSection::getNumRows()
{
    return this->_tracks.size();
}

/*
    usage: unused
*/
void TimeLineSection::paintListBoxItem(int rowNumber, juce::Graphics& g, int width, int height, bool rowIsSelected)
{
}

/*
    usage: refreshes a row's component (track)
    rowNumber: the row's index
    isRowSelected: unused
    existingComponentToUpdate: the component to update (track component)
*/
juce::Component* TimeLineSection::refreshComponentForRow(int rowNumber, bool isRowSelected, juce::Component* existingComponentToUpdate)
{
    //casting the component to a track timeline component
    TrackTimeLineComponent* trackComponent = (TrackTimeLineComponent*)existingComponentToUpdate;

    if (rowNumber < getNumRows())
    {
        //if there's no component, create one
        if (existingComponentToUpdate == nullptr)
        {
            return new TrackTimeLineComponent(this->_tracks[rowNumber], this->_listBox);
        }

        //handling wierd JUCE bug
        if (trackComponent->getId() != this->_tracks[rowNumber].id)
        {
            delete trackComponent;

            return new TrackTimeLineComponent(this->_tracks[rowNumber], this->_listBox);
        }

        switch (this->_currentOperation)
        {
        //if currently adding track, create a new component
        case Operation::ADD_TRACK:
            if (rowNumber == getNumRows() - 1)
            {
                delete trackComponent;

                return new TrackTimeLineComponent(this->_tracks[rowNumber], this->_listBox);
            }
            break;
        
        //if currently removing track, move each track "upwards"
        case Operation::REMOVE_TRACK:
            if (rowNumber >= this->_lastRemovedIndex)
            {
                trackComponent->reset(this->_tracks[rowNumber]);
            }
            break;
        }

        return trackComponent;
    }
    //if the row is non-existing, delete it's component
    else
    {
        delete trackComponent;

        return nullptr;
    }
}

/*
    usage: callback to when the listbox was scrolled
*/
void TimeLineSection::listWasScrolled()
{
    //for each track component, refresh it's header
    for (int i = 0; i < getNumRows(); i++)
    {
        if (((TrackTimeLineComponent*)this->_listBox.getComponentForRowNumber(i)) != nullptr)
        {
            ((TrackTimeLineComponent*)this->_listBox.getComponentForRowNumber(i))->updateHeader();
        }
    }
}

/*
    usage: loads the opened project's tracks
*/
void TimeLineSection::loadTracksData()
{
    //retrieves the opened project's tracks
    RetrieveTrackMessage* msg = new RetrieveTrackMessage();
    auto response = msg->getFutureResponse();
    LogicExecuter::getInstance()->pushMessage(msg);
    this->_tracks = response.get();

    //setting up the timeline meter component
    auto* timeLineMeter = new TimeLineMeterComponent(this->_listBox);
    timeLineMeter->setSize(0, 60);
    this->_listBox.setHeaderComponent(std::unique_ptr<juce::Component>(timeLineMeter));
}

/*
    usage: resize the playback length
*/
void TimeLineSection::resizeTimeLineLength()
{
    this->_listBox.setMinimumContentWidth(this->_tracks.size() == 0 ? 0 : (this->_timeLineLength.get() + 2) * Gui::getTimeLineSpaceBetweenSeconds());
}

/*
    usage: adds a track
    id: the track's id
    name: the track's name
    color: the track's color
    fader: the track's fader
    panner: the track's panner
    mute: is the track muted
*/
void TimeLineSection::addTrack(const int id, const std::string& name, const std::string& color, const float fader, const float panner, const bool mute)
{
    //adding the new track
    this->_tracks.push_back(Models::Track(
        id,
        name,
        color, 
        this->_projectName.get().toStdString(),
        fader,
        panner,
        mute
    ));

    //refreshing the listbox
    this->_currentOperation = Operation::ADD_TRACK;
    this->_listBox.updateContent();
    this->_currentOperation = Operation::NO_OPERATION;
}

/*
    usage: removes a track
    name: the track's name
*/
void TimeLineSection::removeTrack(const std::string& name)
{
    //find the track, and remove it
    for (int i = 0; i < this->_tracks.size(); i++)
    {
        if (this->_tracks[i].name == name)
        {
            this->_lastRemovedIndex = i;
            this->_tracks.erase(this->_tracks.begin() + this->_lastRemovedIndex);
            break;
        }
    }

    //refresh the component
    this->_currentOperation = Operation::REMOVE_TRACK;
    this->_listBox.updateContent();
    this->_currentOperation = Operation::NO_OPERATION;
}

/*
    usage: callback to when a broadcasting ValueTree's property changed
    treeWhosePropertyHasChanged: the tree whose property has changed
    property: the property that has changed
*/
void TimeLineSection::valueTreePropertyChanged(juce::ValueTree& treeWhosePropertyHasChanged, const juce::Identifier& property)
{
    //if the playback length of the project has changed, refresh the component
    if (treeWhosePropertyHasChanged == this->_projectState)
    {
        juce::MessageManager::callAsync([=]() {
            this->resizeTimeLineLength();
        });
    }
}

/*
    usage: callback to when a child was added to a ValueTree
    parentTree: the ValueTree which has been added to
    childWhichHasBeenAdded: the child which has been added
*/
void TimeLineSection::valueTreeChildAdded(juce::ValueTree& parentTree, juce::ValueTree& childWhichHasBeenAdded)
{
    //if a track was added, create it's component
    if (parentTree == this->_projectState)
    {
        juce::MessageManager::callAsync([=]() {
            this->addTrack(
                childWhichHasBeenAdded[IDs::Track::id],
                childWhichHasBeenAdded[IDs::Track::name].toString().toStdString(),
                childWhichHasBeenAdded[IDs::Track::color].toString().toStdString(),
                childWhichHasBeenAdded[IDs::Track::fader],
                childWhichHasBeenAdded[IDs::Track::panner],
                childWhichHasBeenAdded[IDs::Track::mute]
            );
        });
    }
}

/*
    usage: callback to when a child was removed from a ValueTree
    parentTree: the ValueTree which has been removed from
    childWhichHasBeenRemoved: the child which has been removed
    indexFromWhichChildWasRemoved: the index which the child was at
*/
void TimeLineSection::valueTreeChildRemoved(juce::ValueTree& parentTree, juce::ValueTree& childWhichHasBeenRemoved, int indexFromWhichChildWasRemoved)
{
    //if a track was removed, remove it's component
    if (parentTree == this->_projectState)
    {
        juce::MessageManager::callAsync([=]() {
            this->removeTrack(childWhichHasBeenRemoved[IDs::Track::name].toString().toStdString());
        });
    }
}

