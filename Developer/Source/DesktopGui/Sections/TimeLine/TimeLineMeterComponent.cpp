/*
  ==============================================================================

    TimeLineMeterComponent.cpp
    Created: 4 Dec 2020 3:19:42pm
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "TimeLineMeterComponent.h"
#include "../../../Helper/Gui.h"


/*
    usage: constructor
    listBox: the timeline section's listbox
*/
TimeLineMeterComponent::TimeLineMeterComponent(juce::ListBox& listBox) :
    _listBox(listBox)
{
}

/*
    usage: paints the component
*/
void TimeLineMeterComponent::paint (juce::Graphics& g)
{
    //fills the component with the default color
    g.setColour(juce::Desktop::getInstance().getDefaultLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId).withMultipliedBrightness(0.7));
    g.fillRect(getLocalBounds());

    auto headerWidth = Gui::getTrackHeaderArea(this->_listBox).getWidth();
    auto area = getLocalBounds();

    float spaceBetweenQuarterSeconds = Gui::getTimeLineSpaceBetweenSeconds() / 4;
    area.translate(headerWidth, 0);

    g.setColour(juce::Colours::lightgrey);

    //for each quarter second, draw it on the meter
    for (int i = 0; area.getWidth() > spaceBetweenQuarterSeconds; i++)
    {
        // if it's 1/4 or 3/4 quarter seconds
        if (i % 4 == 0)
        {
            g.drawLine(
                area.getX(),
                area.getY() + area.getHeight(),
                area.getX(),
                area.getY() + (area.getHeight() * 0.66)
            );

            g.drawText(
                juce::String(i / 4),
                area.getX() - (Gui::getTimeLineSpaceBetweenSeconds() / 2),
                area.getY() + (area.getHeight() * 0.4),
                Gui::getTimeLineSpaceBetweenSeconds(), area.getHeight() * 0.16,
                juce::Justification::centred
            );
        }
        //if it's half a second
        else if (i % 4 == 2)
        {
            g.drawLine(
                area.getX(),
                area.getY() + area.getHeight(),
                area.getX(),
                area.getY() + (area.getHeight() * 0.74)
            );
        }
        //if it's a second
        else
        {
            g.drawLine(
                area.getX(),
                area.getY() + area.getHeight(),
                area.getX(),
                area.getY() + (area.getHeight() * 0.82)
            );

            g.drawLine(
                area.getX(),
                area.getY() + area.getHeight(),
                area.getX(),
                area.getY() + (area.getHeight() * 0.82)
            );
        }

        area.removeFromLeft(spaceBetweenQuarterSeconds);
    }
}

/*
    usage: resizes the component
*/
void TimeLineMeterComponent::resized()
{
}
