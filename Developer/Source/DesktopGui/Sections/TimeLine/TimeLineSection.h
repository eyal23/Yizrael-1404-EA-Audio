/*
  ==============================================================================

    TimeLineSection.h
    Created: 26 Nov 2020 8:47:08pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../../../Helper/Models.h"


/*
    the timeline section
*/
class TimeLineSection : public juce::Component,
                        public juce::ListBoxModel,
                        private juce::ValueTree::Listener,
                        public juce::DragAndDropContainer
{
private:
    //all possible operations
    enum Operation
    {
        ADD_TRACK = 0,
        REMOVE_TRACK,
        NO_OPERATION
    };

public:
    TimeLineSection();
    ~TimeLineSection() override;

    void paint(juce::Graphics&) override; //paints the component
    void resized() override;              //resizes the component

    int getNumRows() override;                                                                                                       //gets the number of rows in the listbox (track count)
    void paintListBoxItem(int rowNumber, juce::Graphics& g, int width, int height, bool rowIsSelected) override;                     //unused
    juce::Component* refreshComponentForRow(int rowNumber, bool isRowSelected, juce::Component* existingComponentToUpdate) override; //refreshes a row's component (track)
    void listWasScrolled() override;                                                                                                 //callback to when the listbox was scrolled

    void resizeTimeLineLength();
    void loadTracksData();
    void addTrack(const int id, const std::string& name, const std::string& color, const float fader, const float panner, const bool mute);
    void removeTrack(const std::string& name);

private:
    //callbacks to when a ValueTree has changed
    void valueTreePropertyChanged(juce::ValueTree& treeWhosePropertyHasChanged, const juce::Identifier& property) override;
    void valueTreeChildAdded(juce::ValueTree& parentTree, juce::ValueTree& childWhichHasBeenAdded) override;
    void valueTreeChildRemoved(juce::ValueTree& parentTree, juce::ValueTree& childWhichHasBeenRemoved, int indexFromWhichChildWasRemoved) override;

private:
    std::vector<Models::Track> _tracks;           //the opened project's tracks
    juce::ListBox _listBox;                       //the internal listbox
    juce::ValueTree _projectState;                //the opened project's state (ValueTree)
    juce::CachedValue<float> _timeLineLength;     //the opened project's playback length
    juce::CachedValue<juce::String> _projectName; //the opened project's name
    Operation _currentOperation;                  //the current operation
    int _lastRemovedIndex;                        //the last track removed index

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(TimeLineSection)
};
