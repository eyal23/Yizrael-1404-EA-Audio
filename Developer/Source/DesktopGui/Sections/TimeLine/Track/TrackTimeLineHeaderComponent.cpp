/*
  ==============================================================================

    TrackTimeLineHeaderComponent.cpp
    Created: 4 Dec 2020 10:29:33am
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "TrackTimeLineHeaderComponent.h"
#include "../../Actions/SelectorSyncer.h"


/*
    usage: constructor
    trackData: the track's data
*/
TrackTimeLineHeaderComponent::TrackTimeLineHeaderComponent(const Models::Track& trackData) : 
    _trackData(trackData)
{
    //component's mouse clicks properties
    this->setAlwaysOnTop(true);
    this->setInterceptsMouseClicks(true, false);
    this->setWantsKeyboardFocus(true);

    //adds and makes visible the name label
    this->_nameLabel.setInterceptsMouseClicks(false, false);
    this->_nameLabel.setWantsKeyboardFocus(true);
    addAndMakeVisible(this->_nameLabel);
    this->_nameLabel.setText(this->_trackData.name, juce::dontSendNotification);
    this->_nameLabel.setFont(juce::Font(30.0, juce::Font::bold));
    this->_nameLabel.setColour(juce::Label::textColourId, juce::Colours::orange);
    this->_nameLabel.setJustificationType(juce::Justification::centred);
}

/*
    usage: paints the component
    g: the graphics object
*/
void TrackTimeLineHeaderComponent::paint (juce::Graphics& g)
{
    //fills the component with the "dark grey" color
    g.setColour(juce::Colours::darkgrey);
    g.fillRect(getLocalBounds());
}

/*
    usage: resizes component
*/
void TrackTimeLineHeaderComponent::resized()
{
    //resizes the name label
    this->_nameLabel.setBounds(getLocalBounds());
}

/*
    usage: sets the name
    name: the name
*/
void TrackTimeLineHeaderComponent::setName(const std::string& name)
{
    this->_nameLabel.setText(name, juce::dontSendNotification);
}

/*
    usage: gets the name
    return: the name
*/
std::string TrackTimeLineHeaderComponent::getName() const
{
    return this->_nameLabel.getText().toStdString();
}

/*
    usage: callback to when the mouse is down on the component
    event: unused
*/
void TrackTimeLineHeaderComponent::mouseDown(const juce::MouseEvent& event)
{
    //sets the track as selected
    SelectorSyncer::getInstance()->selectTrack(this->_trackData);
}