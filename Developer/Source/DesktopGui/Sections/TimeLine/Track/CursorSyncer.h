/*
  ==============================================================================

    CursorSyncer.h
    Created: 9 Jan 2021 5:22:35pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>


/*
    a class to sync the playback cursor's placement across the UI (singletone)
*/
class CursorSyncer : public juce::ChangeBroadcaster
{
public:
    //part of the singletone design pattern
    CursorSyncer(CursorSyncer& other) = delete;
    void operator=(const CursorSyncer& other) = delete;

    static CursorSyncer* getInstance(); //gets the class instance

    void tryUpdateCursor(const float newPosition); //tries to update the cursor's placement
    float getCurrentPosition() const;

private:
    CursorSyncer();
    ~CursorSyncer();

private:
    static CursorSyncer* _instance; //the class instance

    float _currentPosition; //the cursor's current placement
};