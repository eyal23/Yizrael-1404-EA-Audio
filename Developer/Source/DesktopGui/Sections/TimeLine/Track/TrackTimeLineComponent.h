/*
  ==============================================================================

    TrackTimeLineComponent.h
    Created: 26 Nov 2020 9:19:23pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "TrackTimeLineHeaderComponent.h"
#include "../Clip/ClipTimeLineComponent.h"
#include "../../../../Helper/Models.h"


/*
    a track's timeline component
*/
class TrackTimeLineComponent : public juce::Component,
                               private juce::ValueTree::Listener,
                               private juce::ChangeListener,
                               public juce::DragAndDropTarget
{
public:
    TrackTimeLineComponent(const Models::Track& trackData, juce::ListBox& listBox);
    ~TrackTimeLineComponent() override;

    void paint(juce::Graphics&) override; //paints the component
    void resized() override;              //resizes the component

    void mouseDown(const juce::MouseEvent& event) override;                         //callback to when the mouse is down on the component
    void mouseMove(const juce::MouseEvent& event) override;                         //callback to when the mouse moves in component
    bool isInterestedInDragSource(const SourceDetails& dragSourceDetails) override; //is the component intrested in drag&drop
    void itemDropped(const SourceDetails& dragSourceDetails) override;              //callback to when a component was dropped in the component
    void itemDragMove(const SourceDetails& dragSourceDetails) override;             //callback to when a component is draged over the component
    bool shouldDrawDragImageWhenOver() override;                                    //unused

    void reset(const Models::Track& trackData);   //resets the component with a new track
    void updateHeader();                          //updates the header component
    void updateCursor();                          //updates the cursor's placement in the track
    void updateFadedCursor(const float position); //updates the faded cursor's placement in the track
    void addClip(Models::Clip& clipData);
    void removeClip(const int id);
    void updateClip(const Models::Clip& clipData);
    void updateTrackColor(const std::string& newColor);
    void updateTrackName(const std::string& newName);
    int getId() const;

private:
    //callbacks to when a ValueTree has changed
    void valueTreeChildAdded(juce::ValueTree& parentTree, juce::ValueTree& childWhichHasBeenAdded) override;
    void valueTreeChildRemoved(juce::ValueTree& parentTree, juce::ValueTree& childWhichHasBeenRemoved, int indexFromWhichChildWasRemoved) override;
    void valueTreePropertyChanged(juce::ValueTree& treeWhosePropertyHasChanged, const juce::Identifier& property) override;
    
    //callback to when a change broadcaster changed
    void changeListenerCallback(juce::ChangeBroadcaster* source) override;

private:
    Models::Track _trackData;                       //the track's data
    juce::ListBox& _listBox;                        //the timeline section's listbox
    TrackTimeLineHeaderComponent _header;           //the header component
    juce::OwnedArray<ClipTimeLineComponent> _clips; //the track's clips components
    juce::ValueTree _state;                         //the track's state (ValueTree)
    float _prevPosition;                            //the previus position on the playback cursor
    float _fadedCursorPosition;                     //the position of the faded cursor
    float _dragPosition;                            //the drag position of a clip inside the component

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(TrackTimeLineComponent)
};
