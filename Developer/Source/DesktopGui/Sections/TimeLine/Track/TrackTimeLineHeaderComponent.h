/*
  ==============================================================================

    TrackTimeLineHeaderComponent.h
    Created: 4 Dec 2020 10:29:33am
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../../../../Helper/Models.h"


/*
    the track timeline component's header
*/
class TrackTimeLineHeaderComponent  : public juce::Component
{
public:
    TrackTimeLineHeaderComponent(const Models::Track& trackData);

    void paint (juce::Graphics&) override; //paints the component
    void resized() override;               //resizes the component

    void setName(const std::string& name);
    std::string getName() const;

    void mouseDown(const juce::MouseEvent& event) override; //callback to when the mouse is down on the component

private:
    juce::Label _nameLabel;   //the name label
    Models::Track _trackData; //the track's data

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TrackTimeLineHeaderComponent)
};
