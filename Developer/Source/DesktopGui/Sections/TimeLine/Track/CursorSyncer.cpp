/*
  ==============================================================================

    CursorSyncer.cpp
    Created: 9 Jan 2021 5:22:35pm
    Author:  eyal

  ==============================================================================
*/

#include "CursorSyncer.h"

CursorSyncer* CursorSyncer::_instance{ nullptr };


/*
    usage: gets the class instance
*/
CursorSyncer* CursorSyncer::getInstance()
{
    if (_instance == nullptr)
    {
        _instance = new CursorSyncer();
    }

    return _instance;
}

/*
    usage: tries to update the cursor's placement (locks on quarter seconds)
    newPositionL the cursor's new position
*/
void CursorSyncer::tryUpdateCursor(const float newPosition)
{
    //if the position difference is bigger than half a second, set a totaly new position
    if (std::abs(this->_currentPosition - newPosition) > 0.5)
    {
        float floatPart = newPosition - std::floor(newPosition);

        //is on full second
        if (floatPart < 0.25)
        {
            this->_currentPosition = std::floor(newPosition);
        }
        //is on 1/4 quarter seconds
        else if (floatPart < 0.5)
        {
            this->_currentPosition = std::floor(newPosition) + 0.25;
        }
        //is on half a second
        else if (floatPart < 0.75)
        {
            this->_currentPosition = std::floor(newPosition) + 0.5;
        }
        //is on 3/4 quarter seconds
        else
        {
            this->_currentPosition = std::floor(newPosition) + 0.75;
        }

        this->sendSynchronousChangeMessage();
    }
    //if the position has decreased between quarter and half a second, decrement the position by a quarter second
    else if (this->_currentPosition - newPosition > 0.25)
    {
        this->_currentPosition -= 0.25;
        this->sendSynchronousChangeMessage();
    }
    //if the position has grown between quarter and half a second, increment the position by a quarter second
    else if (newPosition - this->_currentPosition > 0.25)
    {
        this->_currentPosition += 0.25;
        this->sendSynchronousChangeMessage();
    }
}

/*
    usage: gets the cursor's current placement
    return: the cursor's current placement
*/
float CursorSyncer::getCurrentPosition() const
{
    return this->_currentPosition;
}

/*
    usage: constructor
*/
CursorSyncer::CursorSyncer()
{
}

/*
    usage: destructor
*/
CursorSyncer::~CursorSyncer()
{
}
