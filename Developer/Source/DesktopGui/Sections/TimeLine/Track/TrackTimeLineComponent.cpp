/*
  ==============================================================================

    TrackTimeLineComponent.cpp
    Created: 26 Nov 2020 9:19:23pm
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>
#include <vector>

#include "TrackTimeLineComponent.h"
#include "CursorSyncer.h"
#include "../../../../Engine/Engine.h"
#include "../../../../Logic/LogicExecuter.h"
#include "../../../../Logic/Track/DeleteTrackMessage.h"
#include "../../../../Logic/Clip/RetrieveClipMessage.h"
#include "../../../../Logic/Clip/ChangeClipStartingSecondMessage.h"
#include "../../../../Helper/Ids.h"
#include "../../../../Helper/Gui.h"
#include "../../../../Audio/TimeLinePlayHead.h"
#include "../../../../DesktopGui/Sections/Actions/SelectorSyncer.h"


/*
    usage: constructor
    trackData: the track's data
    listBox: the timeline section's listbox
*/
TrackTimeLineComponent::TrackTimeLineComponent(const Models::Track& trackData, juce::ListBox& listBox) :
    _trackData(trackData), 
    _listBox(listBox),
    _header(trackData), 
    _state(Engine::getInstance()->getOpenedProjectObject()->getTrackObject(trackData.id)->addListener(this)),
    _prevPosition(0),
    _fadedCursorPosition(0),
    _dragPosition(0)
{
    //listens to the timeline playhead and the cursor syncer
    TimeLinePlayHead::getInstance()->addChangeListener(this);
    CursorSyncer::getInstance()->addChangeListener(this);

    //component's mouse clicks properties
    this->setInterceptsMouseClicks(true, true);
    this->setWantsKeyboardFocus(true);

    //retrieves the track's clips
    RetrieveClipMessage* msg = new RetrieveClipMessage(this->_trackData.id);
    auto response = msg->getFutureResponse();
    LogicExecuter::getInstance()->pushMessage(msg);
    auto resInfo = response.get();

    //for each clip, map to a clip component
    for (auto& clip : resInfo)
    {
        Engine::getInstance()->getOpenedProjectObject()->getTrackObject(this->_trackData.id)->getClipObject(clip.id)->addListener(this);
        auto* newClipComponent = new ClipTimeLineComponent(clip, this->_trackData.color);
        addAndMakeVisible(newClipComponent);
        
        this->_clips.add(newClipComponent);
    }

    //adds and makes visible the header component
    addAndMakeVisible(this->_header);
}

/*
    usage: destructor
*/
TrackTimeLineComponent::~TrackTimeLineComponent()
{
    auto* projectObject = Engine::getInstance()->getOpenedProjectObject();

    //if listening to a track, stops
    if (projectObject != nullptr)
    {
        auto* trackObject = projectObject->getTrackObject(this->_trackData.id);

        if (trackObject != nullptr)
        {
            trackObject->removeListener(this);
        };
    }

    //stops listening to the timeline playhead and the cursor syncer
    TimeLinePlayHead::getInstance()->removeChangeListener(this);
    CursorSyncer::getInstance()->removeChangeListener(this);
}

/*
    usage: paints the component
    g: the graphics object
*/
void TrackTimeLineComponent::paint(juce::Graphics& g)
{
    auto headerWidth = Gui::getTrackHeaderArea(this->_listBox).getWidth();
    auto area = getLocalBounds();

    float spaceBetweenQuarterSeconds = Gui::getTimeLineSpaceBetweenSeconds() / 4;
    area.translate(headerWidth + spaceBetweenQuarterSeconds, 0);

    g.setColour(juce::Colours::lightgrey);

    //draws each quarter second on the track component
    for (int i = 1; area.getWidth() > spaceBetweenQuarterSeconds; i++)
    {
        g.drawLine(
            area.getX(),
            area.getY(),
            area.getX(),
            area.getY() + area.getHeight(),
            1
        );

        area.removeFromLeft(spaceBetweenQuarterSeconds);
    }

    //fills the component with the default color
    g.fillAll(juce::Desktop::getInstance().getDefaultLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId).withAlpha(0.5f));

    //draws the component's borders
    g.setColour(juce::Colours::black.withAlpha(0.5f));
    g.drawRect(getLocalBounds(), 2.0);

    area = getLocalBounds();
    area.translate(headerWidth, 0);

    //gets the current play position
    juce::AudioPlayHead::CurrentPositionInfo currentInfo;
    TimeLinePlayHead::getInstance()->getCurrentPosition(currentInfo);

    g.setColour(juce::Colours::lightgrey.withMultipliedBrightness(2));

    //draws the playback cursor
    g.drawLine(
        area.getX() + (currentInfo.timeInSeconds * Gui::getTimeLineSpaceBetweenSeconds()),
        area.getY(),
        area.getX() + (currentInfo.timeInSeconds * Gui::getTimeLineSpaceBetweenSeconds()),
        area.getY() + area.getHeight(),
        3
    );

    area = getLocalBounds();
    area.translate(headerWidth, 0);

    g.setColour(juce::Colours::slategrey.withMultipliedBrightness(1.25));

    //draws the faded cursor
    g.drawLine(
        area.getX() + (this->_fadedCursorPosition * Gui::getTimeLineSpaceBetweenSeconds()),
        area.getY(),
        area.getX() + (this->_fadedCursorPosition * Gui::getTimeLineSpaceBetweenSeconds()),
        area.getY() + area.getHeight(),
        2.5f
    );

    this->updateHeader();
}

/*
    usage: resizes the component
*/
void TrackTimeLineComponent::resized()
{
    auto headerWidth = Gui::getTrackHeaderArea(this->_listBox).getWidth();
    auto area = getLocalBounds();

    area.removeFromLeft(headerWidth);

    //for each clip, resize it by it's starting second and length
    for (auto* clip : this->_clips)
    {
        clip->setBounds(
            area.withTrimmedBottom(2).withTrimmedTop(2).getX() + (clip->getData().start * Gui::getTimeLineSpaceBetweenSeconds()),
            area.reduced(2).getY(),
            clip->getData().length * Gui::getTimeLineSpaceBetweenSeconds(),
            area.reduced(2).getHeight()
        );
    }
}

/*
    usage: callback to when the mouse is down on the component
    event: unused
*/
void TrackTimeLineComponent::mouseDown(const juce::MouseEvent& event)
{
    //sets the play position according to the mouse
    TimeLinePlayHead::getInstance()->setPlayPosition(this->_fadedCursorPosition);
}

/*
    usage: callback to when the mouse moves in component
*/
void TrackTimeLineComponent::mouseMove(const juce::MouseEvent& event)
{
    auto headerWidth = Gui::getTrackHeaderArea(this->_listBox).getWidth();
    float position = (this->getMouseXYRelative().getX() - headerWidth) / Gui::getTimeLineSpaceBetweenSeconds();

    //sets the faded cursor's position according to the mouse
    CursorSyncer::getInstance()->tryUpdateCursor(position);
}

/*
    usage: is the component interested in drag&drop
    dragSourceDetails: the details of the drag&drop operation
    return: is the component interested in drag&drop
*/
bool TrackTimeLineComponent::isInterestedInDragSource(const SourceDetails& dragSourceDetails)
{
    ClipTimeLineComponent* clipComponent = (ClipTimeLineComponent*)(dragSourceDetails.sourceComponent.get());

    return this->isParentOf(clipComponent);
}

/*
    usage: callback to when a component was dropped in the component
    dragSourceDetails: the details of the drag&drop operation
*/
void TrackTimeLineComponent::itemDropped(const SourceDetails& dragSourceDetails)
{
    //casts to a clip timeline component
    ClipTimeLineComponent* clipComponent = (ClipTimeLineComponent*)(dragSourceDetails.sourceComponent.get());

    //sets the clip's starting second
    LogicExecuter::getInstance()->pushMessage(new ChangeClipStartingSecondMessage(this->_dragPosition, this->_trackData.id, clipComponent->getData().id));
}

/*
    usage: callback to when a component is draged over the component
    dragSourceDetails: the details of the drag&drop operation
*/
void TrackTimeLineComponent::itemDragMove(const SourceDetails& dragSourceDetails)
{
    //casts to a clip component
    ClipTimeLineComponent* clipComponent = (ClipTimeLineComponent*)(dragSourceDetails.sourceComponent.get());
    float newPosition = (dragSourceDetails.localPosition.getX() - clipComponent->getCurrentGrabPosition() - this->_header.getWidth()) / Gui::getTimeLineSpaceBetweenSeconds();
    bool hasMoved = false;

    //if the position difference is bigger than half a second, set a totaly new position
    if (std::abs(this->_dragPosition - newPosition) > 0.5)
    {
        hasMoved = true;
        float floatPart = newPosition - std::floor(newPosition);

        //is a full second
        if (floatPart < 0.25)
        {
            this->_dragPosition = std::floor(newPosition);
        }
        //is a 1/4 quarter seconds
        else if (floatPart < 0.5)
        {
            this->_dragPosition = std::floor(newPosition) + 0.25;
        }
        //is half a second
        else if (floatPart < 0.75)
        {
            this->_dragPosition = std::floor(newPosition) + 0.5;
        }
        //is 3/4 qurater seconds
        else
        {
            this->_dragPosition = std::floor(newPosition) + 0.75;
        }
    }
    //if the position has decreased by a quarter to half a second, decrement the position by qurater second 
    else if (this->_dragPosition - newPosition > 0.25)
    {
        hasMoved = true;
        this->_dragPosition -= 0.25;
    }
    //if the position has grown by a quarter to half a second, increment the position by qurater second 
    else if (newPosition - this->_dragPosition > 0.25)
    {
        hasMoved = true;
        this->_dragPosition += 0.25;
    }

    //if the position changed, resize the clip component
    if (hasMoved)
    {
        clipComponent->setBounds(clipComponent->getBounds().withX(this->_dragPosition * Gui::getTimeLineSpaceBetweenSeconds() + this->_header.getWidth()));
    }
}

/*
    usage: unused
*/
bool TrackTimeLineComponent::shouldDrawDragImageWhenOver()
{
    return false;
}

/*
    usage: resets the component with a new track
    trackData: the track's data
*/
void TrackTimeLineComponent::reset(const Models::Track& trackData)
{
    //sets the header's name
    this->_header.setName(trackData.name);
    this->_trackData = trackData;

    //retrieves the track's clips
    RetrieveClipMessage* msg = new RetrieveClipMessage(this->_trackData.id);
    auto response = msg->getFutureResponse();
    LogicExecuter::getInstance()->pushMessage(msg);
    auto resInfo = response.get();

    this->_clips.clear();

    //for each clip, map to a component
    for (auto& clip : resInfo)
    {
        auto* newClipComponent = new ClipTimeLineComponent(clip, this->_trackData.color);
        addAndMakeVisible(newClipComponent);

        this->_clips.add(newClipComponent);
    }

    resized();
}

/*
    usage: update the track's header
*/
void TrackTimeLineComponent::updateHeader()
{
    auto headerArea = Gui::getTrackHeaderArea(this->_listBox);

    //resizes the header component
    this->_header.setBounds(headerArea);
}

/*
    usage: updates the playback cursor's placement in the track
*/
void TrackTimeLineComponent::updateCursor()
{
    //gets the current play position
    juce::AudioPlayHead::CurrentPositionInfo currentInfo;
    TimeLinePlayHead::getInstance()->getCurrentPosition(currentInfo);

    auto headerWidth = Gui::getTrackHeaderArea(this->_listBox).getWidth();

    auto area = getLocalBounds();
    area.removeFromLeft(headerWidth + (Gui::getTimeLineSpaceBetweenSeconds() * (this->_prevPosition - 0.5)));
    area.setWidth(Gui::getTimeLineSpaceBetweenSeconds());

    //repaints the playback cursor's area
    this->repaint(area);

    area = getLocalBounds();
    area.removeFromLeft(headerWidth + (Gui::getTimeLineSpaceBetweenSeconds() * (currentInfo.timeInSeconds - 0.5)));
    area.setWidth(Gui::getTimeLineSpaceBetweenSeconds());

    //repaints the playbackcursor's area again
    this->repaint(area);

    this->_prevPosition = currentInfo.timeInSeconds;
}

/*
    usage: updates the faded cursor's placement in the track
    position: the new position
*/
void TrackTimeLineComponent::updateFadedCursor(const float position)
{
    float temp = this->_fadedCursorPosition;
    this->_fadedCursorPosition = position;
    
    auto headerWidth = Gui::getTrackHeaderArea(this->_listBox).getWidth();

    auto area = getLocalBounds();
    area.removeFromLeft(juce::jmax((float)(Gui::getTimeLineSpaceBetweenSeconds() * (temp - 0.5f)), 0.0f) + headerWidth);
    area.setWidth(Gui::getTimeLineSpaceBetweenSeconds());

    //repaints the faded cursor's area
    this->repaint(area);

    area = getLocalBounds();
    area.removeFromLeft(juce::jmax((float)(Gui::getTimeLineSpaceBetweenSeconds() * (this->_fadedCursorPosition - 0.5f)), 0.0f) + headerWidth);
    area.setWidth(Gui::getTimeLineSpaceBetweenSeconds());

    //repaints the faded cursor's area again
    this->repaint(area);
}

/*
    usage: adds a clip
    clipData: the clip's data
*/
void TrackTimeLineComponent::addClip(Models::Clip& clipData)
{
    clipData.trackId = this->_trackData.id;

    //creates the clip component
    auto* newClipComponent = new ClipTimeLineComponent(clipData, this->_trackData.color);
    //adds and makes it visible
    addAndMakeVisible(newClipComponent);

    auto headerWidth = Gui::getTrackHeaderArea(this->_listBox).getWidth();
    auto area = getLocalBounds();

    area.removeFromLeft(headerWidth);

    //resizes the clip component
    newClipComponent->setBounds(
        area.withTrimmedBottom(2).withTrimmedTop(2).getX() + (clipData.start * Gui::getTimeLineSpaceBetweenSeconds()),
        area.reduced(2).getY(),
        newClipComponent->getData().length * Gui::getTimeLineSpaceBetweenSeconds(),
        area.reduced(2).getHeight()
    );

    this->_clips.add(newClipComponent);
}

/*
    usage: removes a clip
    id: the clip's id
*/
void TrackTimeLineComponent::removeClip(const int id)
{
    //finds the clip, and removes it
    for (int i = 0; i < this->_clips.size(); i++)
    {
        if (this->_clips[i]->getData().id == id)
        {
            auto* removedClip = this->_clips.removeAndReturn(i);
            removeChildComponent(removedClip);
            delete removedClip;
            break;
        }
    }
}

/*
    usage: updates a clip's data
    clipData: the clip's new data
*/
void TrackTimeLineComponent::updateClip(const Models::Clip& clipData)
{
    //finds the clip
    for (auto* clipComponent : this->_clips)
    {
        if (clipComponent->getData().id == clipData.id)
        {
            //sets the clip's new data in the clip component
            clipComponent->setData(clipData);
            auto headerWidth = Gui::getTrackHeaderArea(this->_listBox).getWidth();
            auto area = getLocalBounds();

            area.removeFromLeft(headerWidth);

            //resizes the clip according to the new starting second and length
            clipComponent->setBounds(
                area.withTrimmedBottom(2).withTrimmedTop(2).getX() + (clipComponent->getData().start * Gui::getTimeLineSpaceBetweenSeconds()),
                area.reduced(2).getY(),
                clipComponent->getData().length * Gui::getTimeLineSpaceBetweenSeconds(),
                area.reduced(2).getHeight()
            );

            break;
        }    
    }
}

/*
    usage: updates the track's color
    newColor: the track's new color
*/
void TrackTimeLineComponent::updateTrackColor(const std::string& newColor)
{
    this->_trackData.color = newColor;

    //for each clip, update it's color
    for (auto* clipComponent : this->_clips)
    {
        clipComponent->setColor(this->_trackData.color);
    }
}

/*
    usage: updates the track's name
    newName: the track's new name
*/
void TrackTimeLineComponent::updateTrackName(const std::string& newName)
{
    this->_trackData.name = newName;
    this->_header.setName(newName);
}

/*
    usage: gets the track's id
*/
int TrackTimeLineComponent::getId() const
{
    return this->_trackData.id;
}

/*
    usage: callback to when a child was added to a ValueTree
    parentTree: the ValueTree which has been added to
    childWhichHasBeenAdded: the child which has been added
*/
void TrackTimeLineComponent::valueTreeChildAdded(juce::ValueTree& parentTree, juce::ValueTree& childWhichHasBeenAdded)
{
    //if a clip was added, create it's component
    if (parentTree == this->_state)
    {
        juce::MessageManager::callAsync([=]() {
            this->addClip(Models::Clip(
                (int)childWhichHasBeenAdded[IDs::Clip::id],
                childWhichHasBeenAdded[IDs::Clip::name].toString().toStdString(),
                childWhichHasBeenAdded[IDs::Clip::path].toString().toStdString(),
                (float)childWhichHasBeenAdded[IDs::Clip::maxLength],
                (float)childWhichHasBeenAdded[IDs::Clip::length],
                (float)childWhichHasBeenAdded[IDs::Clip::StartingSecond],
                -1
            ));
        });
    }
}

/*
    usage: callback to when a child was removed from a ValueTree
    parentTree: the ValueTree which has been removed from
    childWhichHasBeenRemoved: the child which has been removed
    indexFromWhichChildWasRemoved: the index which the child was at
*/
void TrackTimeLineComponent::valueTreeChildRemoved(juce::ValueTree& parentTree, juce::ValueTree& childWhichHasBeenRemoved, int indexFromWhichChildWasRemoved)
{
    //if a clip was removed, remove it's component
    if (parentTree == this->_state)
    {
        juce::MessageManager::callAsync([=]() {
            this->removeClip((int)childWhichHasBeenRemoved[IDs::Clip::id]);
            });
    }
}

/*
    usage: callback to when a broadcasting ValueTree's property changed
    treeWhosePropertyHasChanged: the tree whose property has changed
    property: the property that has changed
*/
void TrackTimeLineComponent::valueTreePropertyChanged(juce::ValueTree& treeWhosePropertyHasChanged, const juce::Identifier& property)
{
    if (treeWhosePropertyHasChanged.getType() == IDs::Track::object)
    {
        //if the track's name has changed, update it in the component
        if (property == IDs::Track::name)
        {
            juce::MessageManager::callAsync([=]() {
                this->updateTrackName(treeWhosePropertyHasChanged[property].toString().toStdString());
            });
        }
        //if the track's color has changed, update it in the component
        else if(property == IDs::Track::color)
        {
            juce::MessageManager::callAsync([=]() {
                this->updateTrackColor(treeWhosePropertyHasChanged[property].toString().toStdString());
            });
        }
    }
    //if clip has changed, update it's data
    else if (treeWhosePropertyHasChanged.getType() == IDs::Clip::object)
    {
        juce::MessageManager::callAsync([=]() {
            this->updateClip(Models::Clip(
                int(treeWhosePropertyHasChanged[IDs::Clip::id]),
                treeWhosePropertyHasChanged[IDs::Clip::name].toString().toStdString(),
                treeWhosePropertyHasChanged[IDs::Clip::path].toString().toStdString(),
                float(treeWhosePropertyHasChanged[IDs::Clip::maxLength]),
                float(treeWhosePropertyHasChanged[IDs::Clip::length]),
                float(treeWhosePropertyHasChanged[IDs::Clip::StartingSecond]),
                this->getId()
            ));
        });
    }
}

/*
    usage: callback to when a change broadcaster changed
    source: the change broadcaster
*/
void TrackTimeLineComponent::changeListenerCallback(juce::ChangeBroadcaster* source)
{
    //if the play position has changed, update the playback cursor
    if (source == TimeLinePlayHead::getInstance())
    {
        juce::MessageManager::callAsync([=]() {
            this->updateCursor();
        });
    }
    //if the cursor syncer has changed, update the faded cursor
    else if (source == CursorSyncer::getInstance())
    {
        juce::MessageManager::callAsync([=]() {
            this->updateFadedCursor(CursorSyncer::getInstance()->getCurrentPosition());
        });
    }
}
