/*
  ==============================================================================

    MixerSection.cpp
    Created: 18 Jan 2021 9:38:56pm
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "MixerSection.h"
#include "../../../Helper/Ids.h"
#include "../../../Helper/Gui.h"
#include "../../../Engine/Engine.h"
#include "../../../Logic/Track/DeleteTrackMessage.h"
#include "../../../Logic/Track/RetrieveTrackMessage.h"
#include "../../../Logic/LogicExecuter.h"


/*
    usage: constructor
*/
MixerSection::MixerSection() :
    _projectState(Engine::getInstance()->getOpenedProjectObject()->addListener(this)),
    _projectName(this->_projectState, IDs::Project::name, nullptr)
{
    loadTracksData();
}

/*
    usage: destructor
*/
MixerSection::~MixerSection()
{
    //stops listening to the engine
    auto* projectObject = Engine::getInstance()->getOpenedProjectObject();

    //if listening to a project, stop
    if (projectObject != nullptr)
    {
        projectObject->removeListener(this);
    }
}

/*
    usage: paints the component
    g: the graphics object
*/
void MixerSection::paint(juce::Graphics& g)
{
}

/*
    usage: resizes the component
*/
void MixerSection::resized()
{
    //for each track, resize it's component
    for (int i = 0; i < this->_tracks.size(); i++)
    {
        this->_tracks[i]->setBounds(i * Gui::getTrackMixerWidth(), 0, Gui::getTrackMixerWidth(), this->getHeight());
    }
}

/*
    usage: loads the opened project's tracks
*/
void MixerSection::loadTracksData()
{
    //retrieves the opened project's tracks
    RetrieveTrackMessage* msg = new RetrieveTrackMessage();
    auto response = msg->getFutureResponse();
    LogicExecuter::getInstance()->pushMessage(msg);

    //for each track, map to a mixer component
    for (const auto& trackData : response.get())
    {
        auto* trackComponent = new TrackMixerComponent(trackData);
        addAndMakeVisible(trackComponent);
        this->_tracks.add(trackComponent);
    }
}

/*
    usage: adds a new track
    id: the track's id
    name: the track's name
    color: the track's color
    fader: the track's fader
    panner: the track's panner
    mute: is the track muted
*/
void MixerSection::addTrack(const int id, const std::string& name, const std::string& color, const float fader, const float panner, const bool mute)
{
    //creating the component
    auto* trackComponent = new TrackMixerComponent(Models::Track(
        id, 
        name,
        color,
        this->_projectName.get().toStdString(),
        fader,
        panner,
        mute
    ));

    //adds and makes visible the track component
    addAndMakeVisible(trackComponent);
    this->_tracks.add(trackComponent);

    //resizes the track component
    this->setBounds(getLocalBounds().withWidth(this->_tracks.size() * Gui::getTrackMixerWidth()));
}

/*
    usage: removes a track
    name: the track's name
*/
void MixerSection::removeTrack(const std::string& name)
{
    //for each track, check if names matchs
    for (int i = 0; i < this->_tracks.size(); i++)
    {
        //if name matchs, remove the track
        if (this->_tracks[i]->getName() == name)
        {
            removeChildComponent(this->_tracks[i]);
            this->_tracks.remove(i);
            this->setBounds(getLocalBounds().withWidth(this->_tracks.size() * Gui::getTrackMixerWidth()));
            break;
        }
    }
}

/*
    usage: gets the tracks count
    return: the tracks count
*/
int MixerSection::getTracksCount() const
{
    return this->_tracks.size();
}

/*
    usage: callback to when a child was added to a ValueTree
    parentTree: the ValueTree which has been added to
    childWhichHasBeenAdded: the child which has been added
*/
void MixerSection::valueTreeChildAdded(juce::ValueTree& parentTree, juce::ValueTree& childWhichHasBeenAdded)
{
    //if a track was added, create it's component
    if (parentTree == this->_projectState)
    {
        juce::MessageManager::callAsync([=]() {
            this->addTrack(
                childWhichHasBeenAdded[IDs::Track::id],
                childWhichHasBeenAdded[IDs::Track::name].toString().toStdString(),
                childWhichHasBeenAdded[IDs::Track::color].toString().toStdString(),
                childWhichHasBeenAdded[IDs::Track::fader],
                childWhichHasBeenAdded[IDs::Track::panner],
                childWhichHasBeenAdded[IDs::Track::mute]
            );
        });
    }
}

/*
    usage: callback to when a child was removed from a ValueTree
    parentTree: the ValueTree which has been removed from
    childWhichHasBeenRemoved: the child which has been removed
    indexFromWhichChildWasRemoved: the index which the child was at
*/
void MixerSection::valueTreeChildRemoved(juce::ValueTree& parentTree, juce::ValueTree& childWhichHasBeenRemoved, int indexFromWhichChildWasRemoved)
{
    //if a track was removed, remove it's component
    if (parentTree == this->_projectState)
    {
        juce::MessageManager::callAsync([=]() {
            this->removeTrack(childWhichHasBeenRemoved[IDs::Track::name].toString().toStdString());
        });
    }
}