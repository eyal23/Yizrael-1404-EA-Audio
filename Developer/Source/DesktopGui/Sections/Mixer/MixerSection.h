/*
  ==============================================================================

    MixerSection.h
    Created: 18 Jan 2021 9:38:56pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "Track/TrackMixerComponent.h"


/*
    the mixer section
*/
class MixerSection : public juce::Component,
                     private juce::ValueTree::Listener
{
public:
    MixerSection();
    ~MixerSection() override;

    void paint(juce::Graphics&) override; //paints the component
    void resized() override;              //resizes the component

    void loadTracksData();
    void addTrack(const int id, const std::string& name, const std::string& color, const float fader, const float panner, const bool mute);
    void removeTrack(const std::string& name);
    int getTracksCount() const;

private:
    //callbacks to when a ValueTree has changed
    void valueTreeChildAdded(juce::ValueTree& parentTree, juce::ValueTree& childWhichHasBeenAdded) override;
    void valueTreeChildRemoved(juce::ValueTree& parentTree, juce::ValueTree& childWhichHasBeenRemoved, int indexFromWhichChildWasRemoved) override;

private:
    juce::OwnedArray<TrackMixerComponent> _tracks; //the opened project's tracks mixer components
    juce::ValueTree _projectState;                 //the opened project's state (ValueTree)
    juce::CachedValue<juce::String> _projectName;  //the opened project's name

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(MixerSection)
};