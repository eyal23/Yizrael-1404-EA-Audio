/*
  ==============================================================================

    TrackMixerHeaderComponent.cpp
    Created: 19 Jan 2021 8:17:28pm
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "TrackMixerHeaderComponent.h"


/*
    usage: constructor
    name: the name of the track
*/
TrackMixerHeaderComponent::TrackMixerHeaderComponent(const std::string& name)
{
    //adds and makes visible the name label
    addAndMakeVisible(this->_nameLabel);
    this->_nameLabel.setText(name, juce::dontSendNotification);
    this->_nameLabel.setFont(juce::Font(20.0, juce::Font::italic));
    this->_nameLabel.setColour(juce::Label::textColourId, juce::Colours::white);
    this->_nameLabel.setJustificationType(juce::Justification::centred);
}

/*
    usage: paints the component
    g: the graphics object
*/
void TrackMixerHeaderComponent::paint(juce::Graphics& g)
{
    //fills the component with a variant of the "black" color
    g.fillAll(juce::Colours::black.withAlpha(0.7f));
}

/*
    usage: resizes the component
*/
void TrackMixerHeaderComponent::resized()
{
    //resizes the name label
    this->_nameLabel.setBounds(getLocalBounds());
}

/*
    usage: sets the header's name
    name: the header's name
*/
void TrackMixerHeaderComponent::setName(const std::string& name)
{
    this->_nameLabel.setText(name, juce::dontSendNotification);
}
