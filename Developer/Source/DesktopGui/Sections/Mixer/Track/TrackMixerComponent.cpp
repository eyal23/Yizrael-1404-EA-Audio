/*
  ==============================================================================

    TrackMixerComponent.cpp
    Created: 18 Jan 2021 9:40:22pm
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "TrackMixerComponent.h"
#include "../../../../Engine/Engine.h"
#include "../../../../Helper/Ids.h"


/*
    usage: constructor
    trackData: the track's data
*/
TrackMixerComponent::TrackMixerComponent(const Models::Track& trackData) :
    _state(Engine::getInstance()->getOpenedProjectObject()->getTrackObject(trackData.id)->addListener(this)),
    _trackData(trackData), 
    _header(trackData.name), 
    _fader(trackData),
    _panner(trackData),
    _muteButton(trackData)
{
    //adds and makes visible all child components
    addAndMakeVisible(this->_header);
    addAndMakeVisible(this->_fader);
    addAndMakeVisible(this->_panner);
    addAndMakeVisible(this->_muteButton);
}

/*
    usage: destructor
*/
TrackMixerComponent::~TrackMixerComponent()
{
    auto* projectObject = Engine::getInstance()->getOpenedProjectObject();

    //if listening to a track, stop
    if (projectObject != nullptr)
    {
        auto* trackObject = projectObject->getTrackObject(this->_trackData.id);

        if (trackObject != nullptr)
        {
            trackObject->removeListener(this);
        };
    }
}

/*
    usage: paints the component
*/
void TrackMixerComponent::paint (juce::Graphics& g)
{
    //fills the component with the color of the track
    g.fillAll(juce::Colour::fromString(this->_trackData.color).withMultipliedBrightness(0.9));

    //draws the component's borders
    g.setColour(juce::Colours::black);
    g.drawRect(getLocalBounds(), 2.0);
}

/*
    usage: resizes the component
*/
void TrackMixerComponent::resized()
{
    auto area = getLocalBounds();

    //resizes the header component
    this->_header.setBounds(area.withTrimmedBottom(area.getHeight() * 0.85));
    area.removeFromTop(area.getHeight() * 0.15);

    //resizes the mute button
    this->_muteButton.setBounds(area.withTrimmedTop(area.getHeight() * 0.85));
    area.removeFromBottom(area.getHeight() * 0.15);

    //resizes the fader component
    this->_fader.setBounds(
        area.withTrimmedLeft(area.getWidth() * 0.7)
        .withTrimmedTop(area.getHeight() * 0.05)
    );

    //resizes the panner component
    this->_panner.setBounds(
        area.withTrimmedLeft(area.getWidth() * 0.4)
        .withTrimmedRight(area.getWidth() * 0.4)
        .withTrimmedTop(area.getHeight() * 0.025)
        .withTrimmedBottom(area.getHeight() * 0.775)
    );   

    area.removeFromTop((area.getHeight() * 0.225));
    area.removeFromRight(area.getHeight() * 0.3);
    area.removeFromLeft(20);
}

/*
    usage: sets the track's name
    name: the track's name
*/
void TrackMixerComponent::setName(const std::string& name)
{
    this->_trackData.name = name;
    this->_header.setName(name);
}

/*
    usage: sets the track's color
    name: the track's color
*/
void TrackMixerComponent::setColor(const std::string& color)
{
    this->_trackData.color = color;
    repaint();
}

/*
    usage: gets the track's name
    return: the track's name
*/
std::string TrackMixerComponent::getName() const
{
    return this->_trackData.name;
}

/*
    usage: callback to when a broadcasting ValueTree's property changed
    treeWhosePropertyHasChanged: the tree whose property has changed
    property: the property that has changed
*/
void TrackMixerComponent::valueTreePropertyChanged(juce::ValueTree& treeWhosePropertyHasChanged, const juce::Identifier& property)
{
    if (treeWhosePropertyHasChanged.getType() == IDs::Track::object)
    {
        //if the name has changed, set the name
        if (property == IDs::Track::name)
        {
            juce::MessageManager::callAsync([=]() {
                this->setName(treeWhosePropertyHasChanged[property].toString().toStdString());
                });
        }
        //if the color has changed, set the color
        else if (property == IDs::Track::color)
        {
            juce::MessageManager::callAsync([=]() {
                this->setColor(treeWhosePropertyHasChanged[property].toString().toStdString());
                });
        }
    }
}
