/*
  ==============================================================================

    PannerComponent.cpp
    Created: 19 Jan 2021 7:49:54pm
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "PannerComponent.h"
#include "../../../../Logic/Track/UpdateTrackPannerMessage.h"
#include "../../../../Logic/LogicExecuter.h"


/*
    usage: constructor
    trackData: the track's data
*/
PannerComponent::PannerComponent(const Models::Track& trackData) :
    _panSlider(juce::Slider::SliderStyle::RotaryHorizontalDrag, juce::Slider::TextEntryBoxPosition::NoTextBox),
    _trackData(trackData)
{
    //sets the custon design of the slider
    setLookAndFeel(&this->_pannerLookAndFeel);

    //adds and makes visible the pann slider
    addAndMakeVisible(this->_panSlider);
    this->_panSlider.setRange(-1, 1, 0.01);
    this->_panSlider.setValue(trackData.panner, juce::dontSendNotification);
    this->_panSlider.addListener(this);
}

/*
    usage: destructor
*/
PannerComponent::~PannerComponent()
{
    //stops listening
    this->_panSlider.removeListener(this);
    setLookAndFeel(nullptr);
}

/*
    usage: paints the component
    g: the graphics object
*/
void PannerComponent::paint(juce::Graphics& g)
{
}

/*
    usage: resizes the component
*/
void PannerComponent::resized()
{
    //resizes the pann slider
    this->_panSlider.setBounds(getLocalBounds());
}

/*
    usage: callback to when a slider has changed
    slider: the slider
*/
void PannerComponent::sliderValueChanged(juce::Slider* slider)
{
    //sets the track's pann
    this->_trackData.panner = this->_panSlider.getValue();
    LogicExecuter::getInstance()->pushMessage(new UpdateTrackPannerMessage(this->_trackData.panner, this->_trackData.id));
}
