/*
  ==============================================================================

    MuteButton.cpp
    Created: 7 Feb 2021 11:07:51am
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "MuteButton.h"
#include "../../../../Logic/LogicExecuter.h"
#include "../../../../Logic/Track/UpdateTrackMuteMessage.h"


/*
    usage: constructor
*/
MuteButton::MuteButton(const Models::Track& trackData) :
    _trackData(trackData)
{
    //adds and makes visible the mute button
    addAndMakeVisible(this->_muteButton);
    this->_muteButton.setButtonText("M");
    this->_muteButton.setColour(juce::TextButton::ColourIds::textColourOffId, juce::Colours::red.withMultipliedBrightness(2));
    this->_muteButton.setColour(juce::TextButton::ColourIds::buttonColourId, juce::Colours::darkgrey.withMultipliedBrightness(1.25));
    this->_muteButton.setColour(juce::TextButton::ColourIds::textColourOnId, juce::Colours::red.withMultipliedBrightness(0.5));
    this->_muteButton.setColour(juce::TextButton::ColourIds::buttonOnColourId, juce::Colours::darkgrey.withMultipliedBrightness(0.75));

    //listens to the mute button
    this->_muteButton.addListener(this);
    this->_muteButton.setToggleState(this->_trackData.mute, juce::dontSendNotification);
    
}

/*
    usage: destructor
*/
MuteButton::~MuteButton()
{
    //stops listening to the mute button
    this->_muteButton.removeListener(this);
}

/*
    usage: paints the component
    g: the graphics object
*/
void MuteButton::paint (juce::Graphics& g)
{
}

/*
    usage: resizes the component
*/
void MuteButton::resized()
{
    //resizes the mute button
    this->_muteButton.setBounds(getLocalBounds());
}

/*
    usage: reacts to a button click
    button: the button which has been clicked
*/
void MuteButton::buttonClicked(juce::Button* button)
{
    //sets the track's mute
    this->_trackData.mute = !this->_trackData.mute;
    LogicExecuter::getInstance()->pushMessage(new UpdateTrackMuteMessage(this->_trackData.mute, this->_trackData.id));
    this->_muteButton.setToggleState(this->_trackData.mute, juce::dontSendNotification);
    repaint();
}
