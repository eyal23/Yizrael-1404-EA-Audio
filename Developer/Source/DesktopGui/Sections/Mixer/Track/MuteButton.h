/*
  ==============================================================================

    MuteButton.h
    Created: 7 Feb 2021 11:07:51am
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../../../../Helper/Models.h"


/*
    the mute button
*/
class MuteButton  : public juce::Component,
                    private juce::TextButton::Listener
{
public:
    MuteButton(const Models::Track& trackData);
    ~MuteButton() override;

    void paint (juce::Graphics&) override; //paints the component
    void resized() override;               //resizes the component

private:
    void buttonClicked(juce::Button* button) override; //reacts to a button click

private:
    juce::TextButton _muteButton; //the mute button
    Models::Track _trackData;     //the track's data

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MuteButton)
};
