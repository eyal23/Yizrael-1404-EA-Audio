/*
  ==============================================================================

    PannerComponent.h
    Created: 19 Jan 2021 7:49:54pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../../../../Helper/Models.h"


/*
    the panner component
*/
class PannerComponent : public juce::Component,
                        public juce::Slider::Listener
{
public:
    PannerComponent(const Models::Track& trackData);
    ~PannerComponent() override;

    void paint(juce::Graphics&) override; //paints the component
    void resized() override;              //resizes the component

private:
    //custom design of the built in "slider" component
    class PannerLookAndFeel : public juce::LookAndFeel_V4
    {
    public:
        PannerLookAndFeel()
        {
        }

        void drawRotarySlider(juce::Graphics& g, int x, int y, int width, int height, float sliderPos,
            const float rotaryStartAngle, const float rotaryEndAngle, juce::Slider&) override
        {
            auto radius = (float)juce::jmin(width / 2, height / 2) - 4.0f;
            auto centreX = (float)x + (float)width * 0.5f;
            auto centreY = (float)y + (float)height * 0.5f;
            auto rx = centreX - radius;
            auto ry = centreY - radius;
            auto rw = radius * 2.0f;
            auto angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);

            // fill
            g.setColour(juce::Colours::grey);
            g.fillEllipse(rx, ry, rw, rw);

            // outline
            g.setColour(juce::Colours::black);
            g.drawEllipse(rx, ry, rw, rw, 1.0f);

            juce::Path p;
            auto pointerLength = radius * 0.33f;
            auto pointerThickness = 2.0f;
            p.addRectangle(-pointerThickness * 0.5f, -radius, pointerThickness, pointerLength);
            p.applyTransform(juce::AffineTransform::rotation(angle).translated(centreX, centreY));

            // pointer
            g.setColour(juce::Colours::white);
            g.fillPath(p);
        }
    };

    void sliderValueChanged(juce::Slider* slider) override; //callback to when a slider has changed

private:
    PannerLookAndFeel _pannerLookAndFeel; //the custom design of the slider
    juce::Slider _panSlider;              //the pann slider
    Models::Track _trackData;             //the track's data

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(PannerComponent)
};
