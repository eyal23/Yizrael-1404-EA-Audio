/*
  ==============================================================================

    FaderComponent.h
    Created: 19 Jan 2021 7:25:46pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../../../../Helper/Models.h"


/*
    the fader component
*/
class FaderComponent  : public juce::Component,
                        public juce::Slider::Listener
{
public:
    FaderComponent(const Models::Track& trackData);
    ~FaderComponent() override;

    void paint (juce::Graphics&) override; //paints the component
    void resized() override;               //resizes the component

private:
    void sliderValueChanged(juce::Slider* slider) override; //callback to when a slider has changed

private:
    juce::Slider _fadeSlider; //the fade slider
    Models::Track _trackData; //the track's data

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (FaderComponent)
};
