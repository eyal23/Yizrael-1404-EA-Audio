/*
  ==============================================================================

    FaderComponent.cpp
    Created: 19 Jan 2021 7:25:46pm
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "FaderComponent.h"
#include "../../../../Logic/Track/UpdateTrackFaderMessage.h"
#include "../../../../Logic/LogicExecuter.h"


/*
    usage: constructor
    trackData: the track's data
*/
FaderComponent::FaderComponent(const Models::Track& trackData) :
    _fadeSlider(juce::Slider::SliderStyle::LinearVertical, juce::Slider::TextEntryBoxPosition::TextBoxAbove),
    _trackData(trackData)
{
    //adds and makes visible the fade slider
    addAndMakeVisible(this->_fadeSlider);
    this->_fadeSlider.setRange(-48, 18, 1);
    this->_fadeSlider.setValue(this->_trackData.fader, juce::dontSendNotification);
    this->_fadeSlider.setColour(juce::Slider::ColourIds::textBoxOutlineColourId, juce::Colours::transparentWhite);
    this->_fadeSlider.setTextValueSuffix(" Db");

    //listens to the fade slider
    this->_fadeSlider.addListener(this);
}

/*
    usage: destructor
*/
FaderComponent::~FaderComponent()
{
    //stops listening to the fade slider
    this->_fadeSlider.removeListener(this);
}

/*
    usage: paints the component
    g: the graphics object
*/
void FaderComponent::paint (juce::Graphics& g)
{
}

/*
    usage: resizes the component
*/
void FaderComponent::resized()
{
    //resizes the fade slider
    this->_fadeSlider.setBounds(getLocalBounds());
}

/*
    usage: callback to when a slider has changed
    slider: the slider
*/
void FaderComponent::sliderValueChanged(juce::Slider* slider)
{
    //sets the track's fade
    this->_trackData.fader = this->_fadeSlider.getValue();
    LogicExecuter::getInstance()->pushMessage(new UpdateTrackFaderMessage(this->_trackData.fader, this->_trackData.id));
}
