/*
  ==============================================================================

    TrackMixerHeaderComponent.h
    Created: 19 Jan 2021 8:17:28pm
    Author:  eyal

  ==============================================================================
*/


#pragma once

#include <JuceHeader.h>


/*
    a track mixer component's header
*/
class TrackMixerHeaderComponent : public juce::Component
{
public:
    TrackMixerHeaderComponent(const std::string& name);

    void paint(juce::Graphics&) override; //paints the component
    void resized() override;              //resizes the component

    void setName(const std::string& name);

private:
    juce::Label _nameLabel; //the name label

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(TrackMixerHeaderComponent)
};
