/*
  ==============================================================================

    TrackMixerComponent.h
    Created: 18 Jan 2021 9:40:22pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "TrackMixerHeaderComponent.h"
#include "FaderComponent.h"
#include "PannerComponent.h"
#include "MuteButton.h"
#include "../../../../Helper/Models.h"


/*
    a track's mixer component
*/
class TrackMixerComponent  : public juce::Component,
                             private juce::ValueTree::Listener
{
public:
    TrackMixerComponent(const Models::Track& trackData);
    ~TrackMixerComponent() override;

    void paint (juce::Graphics&) override; //paints the component
    void resized() override;               //resizes the component

    void setName(const std::string& name);
    void setColor(const std::string& color);
    std::string getName() const;

private: 
    //callback to when a ValueTree property has changed
    void valueTreePropertyChanged(juce::ValueTree& treeWhosePropertyHasChanged, const juce::Identifier& property) override;

private:
    juce::ValueTree _state;            //the track's state (ValueTree)
    Models::Track _trackData;          //the track's data
    TrackMixerHeaderComponent _header; //the header component
    FaderComponent _fader;             //the fader component
    PannerComponent _panner;           //the panner component
    MuteButton _muteButton;            //the mute button

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TrackMixerComponent)
};
