/*
  ==============================================================================

    TrackActionsComponent.h
    Created: 25 Feb 2021 4:17:09pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#pragma once

#include <JuceHeader.h>
#include "../Source/Helper/Models.h"


/*
    a track's actions component
*/
class TrackActionsComponent : public juce::Component,
                              private juce::Label::Listener,
                              private juce::ChangeListener
{
public:
    TrackActionsComponent(const Models::Track& trackData);
    ~TrackActionsComponent() override;

    void paint(juce::Graphics& g) override; //paints the component
    void resized() override;                //resizes the component

    void labelTextChanged(juce::Label* labelThatHasChanged) override;      //callback to when a label has changed
    void changeListenerCallback(juce::ChangeBroadcaster* source) override; //callcack to when a change broadcaster

private:
    juce::Label _nameLabel;              //the name input label header
    juce::Label _nameText;               //the name input label
    juce::ColourSelector _colorSelector; //the color selector
    Models::Track _trackData;            //the track's data

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(TrackActionsComponent)
};
