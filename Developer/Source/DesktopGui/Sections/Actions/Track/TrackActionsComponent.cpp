/*
  ==============================================================================

    TrackActionsComponent.cpp
    Created: 25 Feb 2021 4:17:09pm
    Author:  משתמש

  ==============================================================================
*/

#include "TrackActionsComponent.h"
#include "../Source/Logic/LogicExecuter.h"
#include "../Source/Logic/Track/UpdateTrackColorMessage.h"
#include "../Source/Logic/Track/UpdateTrackNameMessage.h"


/*
    usage: constructor
    trackData: the track's data
*/
TrackActionsComponent::TrackActionsComponent(const Models::Track& trackData) : 
    _trackData(trackData), _colorSelector(juce::ColourSelector::showAlphaChannel | juce::ColourSelector::showColourAtTop | juce::ColourSelector::showColourspace)
{
    //adds and makes visible the name input label
    addAndMakeVisible(_nameLabel);
    this->_nameLabel.setText("Name:", juce::dontSendNotification);
    this->_nameLabel.attachToComponent(&this->_nameText, true);
    this->_nameLabel.setColour(juce::Label::textColourId, juce::Colours::orange);
    this->_nameLabel.setJustificationType(juce::Justification::right);

    //adds and makes visible the name input label header
    addAndMakeVisible(this->_nameText);
    this->_nameText.setText(this->_trackData.name, juce::dontSendNotification);
    this->_nameText.setEditable(true);
    this->_nameText.setColour(juce::Label::backgroundColourId, juce::Colours::grey);

    //adds and makes visible the color selector
    addAndMakeVisible(this->_colorSelector);
    this->_colorSelector.setCurrentColour(juce::Colour::fromString(trackData.color));
    this->_colorSelector.setColour(juce::ColourSelector::backgroundColourId, juce::Colours::transparentWhite);

    //listens to the color selector and the name input label
    this->_colorSelector.addChangeListener(this);
    this->_nameText.addListener(this);
}

/*
    usage: destructor
*/
TrackActionsComponent::~TrackActionsComponent()
{
    //stops listening
    this->_nameText.removeListener(this);
    this->_colorSelector.removeChangeListener(this);
}

/*
    usage: paints the component
    g: the graphics object
*/
void TrackActionsComponent::paint(juce::Graphics& g)
{
}

/*
    usage: resizes the component
*/
void TrackActionsComponent::resized()
{
    auto area = getLocalBounds();

    //resizes the name input label
    this->_nameText.setBounds(area.getX() + 100, area.getY() + area.proportionOfHeight(0.1), area.proportionOfWidth(0.5) - 100, area.proportionOfHeight(0.15));

    //resizes the color selector
    this->_colorSelector.setBounds(area.getX() + area.proportionOfWidth(0.55), area.getY(), area.proportionOfWidth(0.25), area.getHeight());
}

/*
    usage: callback to when a label has changed
    labelThatHasChanged: the label that has changed
*/
void TrackActionsComponent::labelTextChanged(juce::Label* labelThatHasChanged)
{
    //sets the track name
    this->_trackData.name = labelThatHasChanged->getText().toStdString();
    LogicExecuter::getInstance()->pushMessage(new UpdateTrackNameMessage(this->_trackData.name, this->_trackData.id));
    this->_nameText.setText(this->_trackData.name, juce::dontSendNotification);
}

/*
    usage: callcack to when a change broadcaster
    source: the change broadcaster
*/
void TrackActionsComponent::changeListenerCallback(juce::ChangeBroadcaster* source)
{
    //sets the track's color
    this->_trackData.color = this->_colorSelector.getCurrentColour().toString().toStdString();
    LogicExecuter::getInstance()->pushMessage(new UpdateTrackColorMessage(this->_trackData.color, this->_trackData.id));
}
