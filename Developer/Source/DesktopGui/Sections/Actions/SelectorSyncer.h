/*
  ==============================================================================

    SelectorSyncer.h
    Created: 25 Feb 2021 5:25:46pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "../Source/Helper/Models.h"


/*
    a class to sync the selection of a clip/track with the entire UI (singletone)
*/
class SelectorSyncer : public juce::ChangeBroadcaster
{
public:
    //part of the singletone design pattern
    SelectorSyncer(SelectorSyncer& other) = delete;
    void operator=(const SelectorSyncer& other) = delete;

    static SelectorSyncer* getInstance(); //gets the class instance

    void selectClip(const Models::Clip& newClip);
    void selectTrack(const Models::Track& newTrack);

    Models::Clip* getClip();
    Models::Track* getTrack();

private:
    SelectorSyncer();
    ~SelectorSyncer();

private:
    static SelectorSyncer* _instance;                        //the class instance
    std::unique_ptr<Models::Clip> _selectedClip = nullptr;   //the selected clip
    std::unique_ptr<Models::Track> _selectedTrack = nullptr; //the selected track
};