/*
  ==============================================================================

    ActionsBarComponent.h
    Created: 19 Dec 2020 3:38:42pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once


#include <JuceHeader.h>

#include "../Source/DesktopGui/Sections/Actions/Clip/ClipActionsComponent.h"
#include "../Source/DesktopGui/Sections/Actions/Track/TrackActionsComponent.h"


/*
    the actions section
*/
class ActionsSection : public juce::Component,
                       private juce::TextButton::Listener,
                       private juce::ChangeListener
{
public:
    ActionsSection();
    ~ActionsSection() override;

    void paint(juce::Graphics& g) override; //paints the component
    void resized() override;                //resizes the component

private:
    void buttonClicked(juce::Button* button) override; //reacts to a button click

    juce::Path getPlayButtonShape() const; //creates the shape of the play button
    juce::Path getStopButtonShape() const; //creates the shape of the stop button

    void selectionChanged();                                               //a selected clip/track has changed
    void changeListenerCallback(juce::ChangeBroadcaster* source) override; //callback to a change broadcaster changes

private:
    std::unique_ptr<TrackActionsComponent> _trackComponent; //the currently selected track component
    std::unique_ptr<ClipActionsComponent> _clipComponent;   //the currently selected clip component
    juce::ShapeButton _playButton; //the play button
    juce::ShapeButton _stopButton; //the stop button
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(ActionsSection)
};
