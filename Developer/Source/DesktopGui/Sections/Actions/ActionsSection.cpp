/*
  ==============================================================================

    ActionsBarComponent.cpp
    Created: 19 Dec 2020 3:38:42pm
    Author:  משתמש

  ==============================================================================
*/

#include "ActionsSection.h"
#include "../../../Helper/Ids.h"
#include "../../../Engine/Engine.h"
#include "../../../Logic/LogicExecuter.h"
#include "../../../Logic/Audio/PlayMessage.h"
#include "../../../Logic/Audio/StopMessage.h"
#include "../../../Logic/Audio/RewindMessage.h"
#include "SelectorSyncer.h"


/*
    usage: constructor
*/
ActionsSection::ActionsSection() :
    _playButton("play button", juce::Colours::green.withMultipliedBrightness(1.25), juce::Colours::green.withMultipliedBrightness(1.75), juce::Colours::green.withMultipliedBrightness(2)),
    _stopButton("stop button", juce::Colours::red.withMultipliedBrightness(0.5), juce::Colours::red.withMultipliedBrightness(1.75), juce::Colours::red.withMultipliedBrightness(2))
{
    setVisible(false);

    //listens to the selector syncer
    SelectorSyncer::getInstance()->addChangeListener(this);

    //adds and makes visible the play button
    addAndMakeVisible(this->_playButton);
    this->_playButton.setEnabled(true);

    //adds and makes visible the stop button
    addAndMakeVisible(this->_stopButton);
    this->_stopButton.setEnabled(false);

    //listens to the play and stop buttons
    this->_playButton.addListener(this);
    this->_stopButton.addListener(this);
}

/*
    usage: destructor
*/
ActionsSection::~ActionsSection()
{
    //stops listening
    this->_playButton.removeListener(this);
    this->_stopButton.removeListener(this);
    SelectorSyncer::getInstance()->removeChangeListener(this);
}

/*
    usage: paints the component
    g: the graphics object
*/
void ActionsSection::paint(juce::Graphics& g)
{
    //fills the component with a variant of the "slate grey" color
    g.fillAll(juce::Colours::darkslategrey.withMultipliedBrightness(0.3));
}

/*
    usage: resizes the component
*/
void ActionsSection::resized()
{
    auto area = getLocalBounds();

    //resizes the play button
    this->_playButton.setBounds(area.getX() + area.proportionOfWidth(0.52), area.getY() + area.proportionOfHeight(0.4), 0, 0);
    //sets the play button's shape
    this->_playButton.setShape(this->getPlayButtonShape(), true, true, true);

    //resizes the stop button
    this->_stopButton.setBounds(area.getX() + area.proportionOfWidth(0.48), area.getY() + area.proportionOfHeight(0.4), 0, 0);
    //sets the stop button's shape
    this->_stopButton.setShape(this->getStopButtonShape(), true, true, true);
    
    //if a clip is selected, resize it's component
    if (SelectorSyncer::getInstance()->getClip() != nullptr)
    {
        this->_clipComponent->setBounds(area.getX() + area.proportionOfHeight(0.1), area.getY() + area.proportionOfHeight(0.1), area.proportionOfWidth(0.45), area.proportionOfHeight(0.8));
    }
    //if a track is selected, resize it's component
    else if (SelectorSyncer::getInstance()->getTrack() != nullptr)
    {
        this->_trackComponent->setBounds(area.getX() + area.proportionOfHeight(0.1), area.getY() + area.proportionOfHeight(0.1), area.proportionOfWidth(0.45), area.proportionOfHeight(0.8));
    }
}

/*
    usage: reacts to a button click
    button: the button which has been clicked
*/
void ActionsSection::buttonClicked(juce::Button* button)
{
    //if the play button was clicked, start playback
    if (button == &this->_playButton)
    {
        //starts the playback
        LogicExecuter::getInstance()->pushMessage(new PlayMessage());

        //adjusts the buttons colors
        this->_playButton.setColours(juce::Colours::green.withMultipliedBrightness(0.5), juce::Colours::green.withMultipliedBrightness(1.75), juce::Colours::green.withMultipliedBrightness(2));
        this->_stopButton.setColours(juce::Colours::red.withMultipliedBrightness(1.25), juce::Colours::red.withMultipliedBrightness(1.75), juce::Colours::red.withMultipliedBrightness(2));

        this->_playButton.setEnabled(false);
        this->_stopButton.setEnabled(true);
    }
    //if the play button was clicked, stop playback
    else
    {
        //stops the playback
        LogicExecuter::getInstance()->pushMessage(new StopMessage());

        //adjusts the buttons colors
        this->_playButton.setColours(juce::Colours::green.withMultipliedBrightness(1.25), juce::Colours::green.withMultipliedBrightness(1.75), juce::Colours::green.withMultipliedBrightness(2));
        this->_stopButton.setColours(juce::Colours::red.withMultipliedBrightness(0.5), juce::Colours::red.withMultipliedBrightness(1.75), juce::Colours::red.withMultipliedBrightness(2));

        this->_stopButton.setEnabled(false);
        this->_playButton.setEnabled(true);
    }
}

/*
    usage: create the play button's shape
    return: the play button's shape
*/
juce::Path ActionsSection::getPlayButtonShape() const
{
    auto area = getLocalBounds();
    juce::Path shape;

    shape.addTriangle(
        0, area.proportionOfHeight(0.4),
        area.proportionOfHeight(0.2), area.proportionOfHeight(0.5),
        0, area.proportionOfHeight(0.6)
    );

    return shape;
}

/*
    usage: create the stop button's shape
    return: the stop button's shape
*/
juce::Path ActionsSection::getStopButtonShape() const
{
    auto area = getLocalBounds();
    juce::Path shape;

    shape.addRectangle(
        0,
        0,
        area.proportionOfHeight(0.075),
        area.proportionOfHeight(0.2)
    );

    shape.addRectangle(
        area.proportionOfHeight(0.125),
        0,
        area.proportionOfHeight(0.075),
        area.proportionOfHeight(0.2)
    );

    return shape;
}

/*
    usage: reacts to a selection of a clip/track
*/
void ActionsSection::selectionChanged()
{
    //resets the previus component
    this->_clipComponent.reset();
    this->_trackComponent.reset();
    
    //if a clip was selected, set up it's component
    if (SelectorSyncer::getInstance()->getClip() != nullptr)
    {
        //sets up the clip component
        this->_clipComponent.reset(new ClipActionsComponent(*SelectorSyncer::getInstance()->getClip()));
        //adds and makes visible the clip component
        addAndMakeVisible(this->_clipComponent.get());

        resized();
    }
    //if a track was selected, set up it's component
    else if (SelectorSyncer::getInstance()->getTrack() != nullptr)
    {
        //sets up the track component
        this->_trackComponent.reset(new TrackActionsComponent(*SelectorSyncer::getInstance()->getTrack()));
        //adds and makes visible the track component
        addAndMakeVisible(this->_trackComponent.get());

        resized();
    }
}

/*
    usage: callback to a change broadcaster changes
    source: the change broadcaster
*/
void ActionsSection::changeListenerCallback(juce::ChangeBroadcaster* source)
{
    //if the change broadcaster is the selector syncer, refresh the component
    if (source == SelectorSyncer::getInstance())
    {
        juce::MessageManager::callAsync([=]() {
                this->selectionChanged();
            });
    }
}
