/*
  ==============================================================================

    ClipActionsComponent.h
    Created: 25 Feb 2021 4:17:35pm
    Author:  משתמש

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "../Source/Helper/Models.h"


/*
    a clip's actions component
*/
class ClipActionsComponent : public juce::Component,
                             private juce::Label::Listener
{
public:
    ClipActionsComponent(Models::Clip& clipData);
    ~ClipActionsComponent() override;

    void paint(juce::Graphics& g) override; //paints the component
    void resized() override;                //resizes the component
   
private:
    void labelTextChanged(juce::Label* labelThatHasChanged) override; //callback to when a label has changed

private:
    juce::Label _nameLabel;           //the name input label header
    juce::Label _nameText;            //the name input label
    juce::Label _startingSecondLabel; //the starting second input label header
    juce::Label _startingSecondText;  //the starting second input label
    juce::Label _lengthLabel;         //the length input label header
    juce::Label _lengthText;          //the length input label
    Models::Clip _clipData;           //the clip's data

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(ClipActionsComponent)
};
