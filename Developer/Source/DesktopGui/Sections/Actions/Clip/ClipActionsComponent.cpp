/*
  ==============================================================================

    ClipActionsComponent.cpp
    Created: 25 Feb 2021 4:17:35pm
    Author:  משתמש

  ==============================================================================
*/

#include "ClipActionsComponent.h"
#include "../../../../Logic/LogicExecuter.h"
#include "../../../../Logic/Clip/ChangeClipNameMessage.h"
#include "../../../../Logic/Clip/ChangeClipStartingSecondMessage.h"
#include "../../../../Logic/Clip/ChangeClipLengthMessage.h"


/*
    usage: constructor
*/
ClipActionsComponent::ClipActionsComponent(Models::Clip& clipData) :
    _clipData(clipData)
{
    //adds and makes visible the name input label header
    addAndMakeVisible(_nameLabel);
    this->_nameLabel.setText("Name:", juce::dontSendNotification);
    this->_nameLabel.attachToComponent(&this->_nameText, true);
    this->_nameLabel.setColour(juce::Label::textColourId, juce::Colours::orange);
    this->_nameLabel.setJustificationType(juce::Justification::right);

    //adds and makes visible the name input label
    addAndMakeVisible(this->_nameText);
    this->_nameText.setText(this->_clipData.name, juce::dontSendNotification);
    this->_nameText.setEditable(true);
    this->_nameText.setColour(juce::Label::backgroundColourId, juce::Colours::grey);

    //adds and makes visible the starting second input label header
    addAndMakeVisible(_startingSecondLabel);
    this->_startingSecondLabel.setText("Starting Second:", juce::dontSendNotification);
    this->_startingSecondLabel.attachToComponent(&this->_startingSecondText, true);
    this->_startingSecondLabel.setColour(juce::Label::textColourId, juce::Colours::orange);
    this->_startingSecondLabel.setJustificationType(juce::Justification::right);

    //adds and makes visible the starting second input label
    addAndMakeVisible(this->_startingSecondText);
    this->_startingSecondText.setText(juce::String(this->_clipData.start), juce::dontSendNotification);
    this->_startingSecondText.setEditable(true);
    this->_startingSecondText.setColour(juce::Label::backgroundColourId, juce::Colours::grey);
   
    //adds and makes visible the length input label header
    addAndMakeVisible(_lengthLabel);
    this->_lengthLabel.setText("Lenght:", juce::dontSendNotification);
    this->_lengthLabel.attachToComponent(&this->_lengthText, true);
    this->_lengthLabel.setColour(juce::Label::textColourId, juce::Colours::orange);
    this->_lengthLabel.setJustificationType(juce::Justification::right);

    //adds and makes visible the length input label
    addAndMakeVisible(this->_lengthText);
    this->_lengthText.setText(juce::String(this->_clipData.length), juce::dontSendNotification);
    this->_lengthText.setEditable(true);
    this->_lengthText.setColour(juce::Label::backgroundColourId, juce::Colours::grey);

    //listens to the input labels
    this->_nameText.addListener(this);
    this->_startingSecondText.addListener(this);
    this->_lengthText.addListener(this);
}

/*
    usage: destructor
*/
ClipActionsComponent::~ClipActionsComponent()
{
    //stops listening
    this->_nameText.removeListener(this);
    this->_startingSecondText.removeListener(this);
    this->_lengthText.removeListener(this);
}

/*
    usage: paints the component
*/
void ClipActionsComponent::paint(juce::Graphics& g)
{
}

/*
    usage: resizes the component
*/
void ClipActionsComponent::resized()
{
    auto area = getLocalBounds();
    
    //resizes the input labels
    this->_nameText.setBounds(area.getX() + 100, area.getY() + area.proportionOfHeight(0.2), area.getWidth() - 100, area.proportionOfHeight(0.15));
    this->_startingSecondText.setBounds(area.getX() + 100, area.getY() + area.proportionOfHeight(0.5), area.getWidth() - 100, area.proportionOfHeight(0.15));
    this->_lengthText.setBounds(area.getX() + 100, area.getY() + area.proportionOfHeight(0.8), area.getWidth() - 100, area.proportionOfHeight(0.15));
}

/*
    usage: callback to when a label has changed
*/
void ClipActionsComponent::labelTextChanged(juce::Label* labelThatHasChanged)
{
    //if the name input label has changed, set the clip's name
    if (labelThatHasChanged == &this->_nameText)
    {
        this->_clipData.name = this->_nameText.getText().toStdString();
        LogicExecuter::getInstance()->pushMessage(new ChangeClipNameMessage(this->_clipData.name, this->_clipData.trackId, this->_clipData.id));
        this->_nameText.setText(this->_clipData.name, juce::dontSendNotification);
    }
    //if the starting second input label has changed, set the clip's starting second
    else if (labelThatHasChanged == &this->_startingSecondText)
    {
        this->_clipData.start = std::stof(this->_startingSecondText.getText().toStdString());
        LogicExecuter::getInstance()->pushMessage(new ChangeClipStartingSecondMessage(this->_clipData.start, this->_clipData.trackId, this->_clipData.id));
        this->_startingSecondText.setText(juce::String(this->_clipData.start), juce::dontSendNotification);
    }
    //if the length input label has changed, set the clip's length
    else if (labelThatHasChanged == &this->_lengthText)
    {
        this->_clipData.length = std::stof(this->_lengthText.getText().toStdString());
        LogicExecuter::getInstance()->pushMessage(new ChangeClipLengthMessage(this->_clipData.length, this->_clipData.trackId, this->_clipData.id));
        this->_lengthText.setText(juce::String(this->_clipData.length), juce::dontSendNotification);
    }
}


