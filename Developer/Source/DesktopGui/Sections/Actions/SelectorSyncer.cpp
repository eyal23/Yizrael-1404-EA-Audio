/*
  ==============================================================================

    SelectorSyncer.cpp
    Created: 25 Feb 2021 5:25:46pm
    Author:  משתמש

  ==============================================================================
*/

#include "SelectorSyncer.h"

SelectorSyncer* SelectorSyncer::_instance{ nullptr };


/*
    usage: constructor
*/
SelectorSyncer::SelectorSyncer()
{
}

/*
    usage: destructor
*/
SelectorSyncer::~SelectorSyncer()
{
}

/*
    usage: gets the class instance
    return: the class instance
*/
SelectorSyncer* SelectorSyncer::getInstance()
{
    if (_instance == nullptr)
    {
        _instance = new SelectorSyncer();
    }

    return _instance;
}

/*
    usage: selects a clip
    newClip: the clip to select
*/
void SelectorSyncer::selectClip(const Models::Clip& newClip)
{
    this->_selectedTrack.reset();
    this->_selectedClip.reset(new Models::Clip(newClip));
    this->sendChangeMessage();
}

/*
    usage: selects a track
    newTrack: the track to select
*/
void SelectorSyncer::selectTrack(const Models::Track& newTrack)
{
    this->_selectedClip.reset();
    this->_selectedTrack.reset(new Models::Track(newTrack));
    this->sendChangeMessage();
}

/*
    usage: gets the currently selected clip
    return: the currently selected clip
*/
Models::Clip* SelectorSyncer::getClip()
{
    return this->_selectedClip.get();
}

/*
    usage: gets the currently selected track
    return: the currently selected track
*/
Models::Track* SelectorSyncer::getTrack()
{
    return this->_selectedTrack.get();
}
