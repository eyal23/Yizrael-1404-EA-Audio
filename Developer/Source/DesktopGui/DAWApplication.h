/*
  ==============================================================================

    DAWApplication.h
    Created: 4 Nov 2020 8:37:25pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "MainWindow.h"


/*
    the app
*/
class DAWApplication : public juce::JUCEApplication
{
public:
    const juce::String getApplicationName() override;
    const juce::String getApplicationVersion() override;
    bool moreThanOneInstanceAllowed() override;
    void initialise(const juce::String& commandLine) override;
    void shutdown() override;
    void systemRequestedQuit() override;

private:
    std::unique_ptr<MainWindow> mainWindow; //the app's main window
};