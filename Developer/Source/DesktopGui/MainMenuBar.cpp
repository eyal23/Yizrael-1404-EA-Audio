/*
  ==============================================================================

    MainMenuBar.cpp
    Created: 14 Nov 2020 7:01:45pm
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "MainMenuBar.h"
#include "PopUpWindows/Project/CreateProjectPopUpWindow.h"
#include "PopUpWindows/Project/DeleteProjectPopUpWindow.h"
#include "PopUpWindows/Project/OpenProjectPopUpWindow.h"
#include "PopUpWindows/Track/CreateTrackPopUpWindow.h"
#include "PopUpWindows/Track/DeleteTrackPopUpWindow.h"
#include "PopUpWindows/Clip/DeleteClipPopUpWindow.h"
#include "PopUpWindows/Clip/CreateClipPopUpWindow.h"
#include "PopUpWindows/AudioDevices/EditAudioDevicesPopUpWindow.h"
#include "../Logic/Project/CloseProjectMessage.h"
#include "../Logic/LogicExecuter.h"
#include "../Engine/Engine.h"
#include "../Helper/Ids.h"

#define DEFAULT_WIDTH 600
#define DEFAULT_HEIGHT 400


/*
    usage: constructor
*/
MainMenuBar::MainMenuBar() :
    _currentlyPopedWindow(nullptr), _openedProjectName(Engine::getInstance()->addListener(nullptr), IDs::Engine::openedProjectName, nullptr)
{
}

/*
    usage: destructor
*/
MainMenuBar::~MainMenuBar()
{
    //if a pop up window exists, close it
    if (this->_currentlyPopedWindow != nullptr)
    {
        this->_currentlyPopedWindow->exitModalState(0);

        delete this->_currentlyPopedWindow;
    }
}

/*
    usage: gets the menu bar's names
    return: the menu bar's names
*/
juce::StringArray MainMenuBar::getMenuBarNames()
{
    return { "File" ,"Settings"};
}

/*
    usage: gets a PopUpMenu, according to a given index and name
    topLevelMenuIndex: the menu's index
    menuName: the menu's name
    return: the PopUpMenu
*/
juce::PopupMenu MainMenuBar::getMenuForIndex(int topLevelMenuIndex, const juce::String& menuName)
{
    juce::PopupMenu popUpMenu; //the menu to pop
    bool isProjectOpened;
   
    switch (topLevelMenuIndex)
    {
    //if the file menu was chosen
    case menuBarActions::fileMenu:

        //add "project" seperator
        popUpMenu.addSectionHeader("Project");
        popUpMenu.addSeparator();

        //add "create project" item
        popUpMenu.addItem(
            "Create Project",
            true,
            false,
            [&, this]() { this->launchPopUpWindow("Create Project", new CreateProjectPopUpWindow()); } //if selected, launch create project pop up window
        );

        //add "delete project" item
        popUpMenu.addItem(
            "Delete Project",
            true,
            false,
            [&, this]() { this->launchPopUpWindow("Delete Project", new DeleteProjectPopUpWindow()); } //if selected, launch delete project pop up window
        );

        //add "open project" item
        popUpMenu.addItem(
            "Open Project",
            true,
            false,
            [&, this]() { this->launchPopUpWindow("Open Project", new OpenProjectPopUpWindow()); } //if selected, launch open project pop up window
        );

        //checks if a project id currently opened
        isProjectOpened = this->_openedProjectName.get().toStdString() != "";

        //add "close project" item
        popUpMenu.addItem(
            "Close Project",
            isProjectOpened, //if project is not opened, disable item
            false,
            []() { LogicExecuter::getInstance()->pushMessage(new CloseProjectMessage()); } //if selected, launch close project pop up window
        );

        //add "track" seperator
        popUpMenu.addSectionHeader("Track");
        popUpMenu.addSeparator();

        //add "create track" item
        popUpMenu.addItem(
            "Create Track",
            isProjectOpened, //if project is not opened, disable item
            false,
            [&, this]() { this->launchPopUpWindow("Create Track", new CreateTrackPopUpWindow()); } //if selected, launch create track pop up window
        );

        //add "delete track" item
        popUpMenu.addItem(
            "Delete Track",
            isProjectOpened, //if project is not opened, disable item
            false,
            [&, this]() { this->launchPopUpWindow("Delete Track", new DeleteTrackPopUpWindow()); } //if selected, launch delete track pop up window
        );

        //add "clip" seperator
        popUpMenu.addSectionHeader("Clip");
        popUpMenu.addSeparator();

        //add "create clip" item
        popUpMenu.addItem(
            "Create Clip",
            isProjectOpened, //if project is not opened, disable item
            false,
            [&, this]() { this->launchPopUpWindow("Create Clip", new CreateClipPopUpWindow()); } //if selected, launch create clip pop up window
        );

        //add "delete clip" item
        popUpMenu.addItem(
            "Delete Clip",
            isProjectOpened, //if project is not opened, disable item
            false,
            [&, this]() { this->launchPopUpWindow("Delete Clip", new DeleteClipPopUpWindow()); } //if selected, launch delete clip pop up window
        );

        break;
    
    //if the setting menu was chosen
    case menuBarActions::settingsMenu:

        //add "audio devices" item
        popUpMenu.addItem(
            "Audio Devices",
            true,
            false,
            [&, this]() { this->launchPopUpWindow("Audio Devices", new EditAudioDevicesPopUpWindow()); } //if selected, launch audio devices pop up window
        );

        break;
    }

    return popUpMenu;
}

/*
    usage: unused
*/
void MainMenuBar::menuItemSelected(int menuItemID, int topLevelMenuIndex)
{
}

/*
    usage: aunches a pop up window with a given main component
    mainComponent: the main component
*/
void MainMenuBar::launchPopUpWindow(const std::string& title, juce::Component* mainComponent)
{
    //setting up the pop up window
    juce::DialogWindow::LaunchOptions options;
    options.content.setOwned(mainComponent);
    options.content->setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    options.dialogTitle = title;
    options.escapeKeyTriggersCloseButton = true;
    options.useNativeTitleBar = true;
    options.resizable = false;

    //launching the window
    this->_currentlyPopedWindow = options.launchAsync();

    if (this->_currentlyPopedWindow != nullptr)
    {
        //setting the window's size
        this->_currentlyPopedWindow->centreWithSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }
}
