/*
  ==============================================================================

    DAWApplication.cpp
    Created: 4 Nov 2020 8:37:25pm
    Author:  eyal

  ==============================================================================
*/

#include "DAWApplication.h"


/*
    usage: gets the name of the application
    return: the name of the application
*/
const juce::String DAWApplication::getApplicationName()
{
    return ProjectInfo::projectName;
}

/*
    usage: gets the version of the application
    return: the version of the application
*/
const juce::String DAWApplication::getApplicationVersion()
{
    return ProjectInfo::versionString;
}

/*
    usage: tells if more than one instance of the application is allowed
    return: if more than one instance of the application is allowed
*/
bool DAWApplication::moreThanOneInstanceAllowed()
{
    return false;
}

/*
    usage: initializes the application
    commandLine: unused
*/
void DAWApplication::initialise(const juce::String& commandLine)
{
    //creating the app's main window
    mainWindow.reset(new MainWindow(getApplicationName()));
}

/*
    usage: shuts down the application.
*/
void DAWApplication::shutdown()
{
    mainWindow = nullptr;
}

/*
    usage: requests to shut down the application.
*/
void DAWApplication::systemRequestedQuit()
{
    quit();
}
