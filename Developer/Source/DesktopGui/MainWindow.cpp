/*
  ==============================================================================

    MainWindow.cpp
    Created: 4 Nov 2020 8:24:53pm
    Author:  eyal

  ==============================================================================
*/

#include <JuceHeader.h>

#include "MainWindow.h"
#include "MainComponent.h"
#include "../Logic/LogicExecuter.h"
#include "../Engine//Engine.h"


/*
    usage: constructor
    name: the window's name
*/
MainWindow::MainWindow(const juce::String& name) :
    DocumentWindow(
        name,
        juce::Desktop::getInstance().getDefaultLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId),
        DocumentWindow::closeButton | DocumentWindow::minimiseButton
    ),
    _menuBar(new MainMenuBar())
{
    //setting up the window
    setUsingNativeTitleBar(true);
    setFullScreen(true);
    setMenuBar(this->_menuBar.get());
    setContentOwned(new MainComponent(), true);
    setResizable(false, false);
    
    //setting it visible on screen
    setVisible(true);
}

/*
    usage: reacts to a click of the close button
*/
void MainWindow::closeButtonPressed()
{
    Engine::getInstance()->closeProject();
    this->setMenuBar(nullptr);
    juce::JUCEApplication::getInstance()->systemRequestedQuit();
}
