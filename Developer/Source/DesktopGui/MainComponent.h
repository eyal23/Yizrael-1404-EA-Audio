#pragma once

#include <JuceHeader.h>

#include "Sections/TimeLine/TimeLineSection.h"
#include "Sections/Actions/ActionsSection.h"
#include "Sections/Mixer/MixerSection.h"


/*
    the main window's main component
*/
class MainComponent : public juce::Component,
                      private juce::ValueTree::Listener
{
public:
    MainComponent();
    ~MainComponent() override;

    void paint(juce::Graphics&) override; //paints the component
    void resized() override;              //resizes the component

    void projectChanged(); //the opened project has changes

private:
    //callback to when a ValueTree property has changes
    void valueTreePropertyChanged(juce::ValueTree& treeWhosePropertyHasChanged, const juce::Identifier& property) override;

private:
    juce::ValueTree _engineState;                       //the engine's state (ValueTree)
    juce::CachedValue<juce::String> _openedProjectName; //the opened project's name

    std::unique_ptr<TimeLineSection> _timeLineSection; //the timeline section's component
    MixerSection* _mixerSection;                       //the mixer section's component
    ActionsSection _actionSection;                     //the actions section's component
    juce::Viewport _viewPort;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(MainComponent)
};
