/*
  ==============================================================================

    Gui.h
    Created: 6 Jan 2021 5:38:42pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>


/*
    namespace for all GUI related helpers
*/
namespace Gui
{
    /*
        usage: gets the current screen area
        return: the current screen area
    */
    static juce::Rectangle<int> getScreenArea()
    {
        return juce::Desktop::getInstance().getDisplays().getMainDisplay().userArea;
    }

    /*
        usage: gets the length of a second on screen, in pixels
        return: the length of a second on screen, in pixels
    */
    static float getTimeLineSpaceBetweenSeconds()
    {
        return getScreenArea().getWidth() * 0.05;
    }

    /*
        usage: gets the TrackMixer component width, in pixels
        return: the TrackMixer component width, in pixels
    */
    static float getTrackMixerWidth()
    {
        return getScreenArea().getWidth() * 0.1;
    }

    /*
        usage: gets the TrackHeader component width, in pixels
        return: the TrackHeader component width, in pixels
    */
    static juce::Rectangle<int> getTrackHeaderArea(const juce::ListBox& listBox)
    {
        auto visibileArea = listBox.getViewport()->getViewArea().withHeight(listBox.getRowHeight()).withY(0);

        return visibileArea.reduced(2).withTrimmedRight(visibileArea.getWidth() * 0.92);
    }
}