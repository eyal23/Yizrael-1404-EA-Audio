/*
  ==============================================================================

    Models.h
    Created: 8 Dec 2020 11:21:09am
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>


/*
    a namespace for all data entities models
*/
namespace Models
{
    /*
        a class for storing a project's data
    */
    typedef struct Project
    {
        Project(const std::string& name, const std::string& creationDate) :
        name(name), creationDate(creationDate) {}

        std::string name;
        std::string creationDate;

    } Project;

    /*
        a class for storing a track's data
    */
    typedef struct Track
    {
        Track(const int id, const std::string& name, const std::string& color, const std::string& projectName, const float fader, const float panner, const bool mute) :
        id(id), name(name), color(color), projectName(projectName), fader(fader), panner(panner), mute(mute) {}

        int id;
        std::string name;
        std::string color;
        std::string projectName;
        float fader;
        float panner;
        bool mute;
    } Track;

    /*
        a class for storing a clip's data
    */
    typedef struct Clip
    {
        Clip(const int id, const std::string& name, const std::string& path, const float maxLength, const float length, const float start, const int trackId) :
        id(id), name(name), path(path), maxLength(maxLength), length(length), start(start), trackId(trackId) {}

        int id;
        std::string name;
        std::string path;
        float maxLength;
        float length;
        float start;
        int trackId;
    } Clip;
}