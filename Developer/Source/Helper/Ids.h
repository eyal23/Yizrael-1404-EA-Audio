/*
  ==============================================================================

    Ids.h
    Created: 12 Nov 2020 1:35:46pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>


/*
    namespace for all ValueTree identifiers
*/
namespace IDs
{
    /*
        sub-namespace for all ValueTree Engine identifiers
    */
    namespace Engine
    {
        static const juce::Identifier object("project");
        static const juce::Identifier openedProjectName("opened project name");
    };

    /*
        sub-namespace for all ValueTree Project identifiers
    */
    namespace Project
    {
        static const juce::Identifier object("project");
        static const juce::Identifier name("project name");
        static const juce::Identifier creationDate("project creation date");
        static const juce::Identifier timeLineLength("project time line length");
    };

    /*
        sub-namespace for all ValueTree Track identifiers
    */
    namespace Track
    {
        static const juce::Identifier object("track");
        static const juce::Identifier id("track id");
        static const juce::Identifier name("track name");
        static const juce::Identifier color("track color");
        static const juce::Identifier length("track length");
        static const juce::Identifier fader("track fader");
        static const juce::Identifier panner("track panner");
        static const juce::Identifier mute("track mute");
    };

    /*
        sub-namespace for all ValueTree Clip identifiers
    */
    namespace Clip
    {
        static const juce::Identifier object("clip");
        static const juce::Identifier id("clip id");
        static const juce::Identifier name("clip name");
        static const juce::Identifier path("clip path");
        static const juce::Identifier maxLength("clip max length");
        static const juce::Identifier length("clip length");
        static const juce::Identifier StartingSecond("clip starting second");
    };
}