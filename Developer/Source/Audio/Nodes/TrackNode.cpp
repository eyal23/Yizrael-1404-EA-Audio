/*
  ==============================================================================

    TrackNode.cpp
    Created: 26 Dec 2020 11:50:25am
    Author:  eyal

  ==============================================================================
*/

#include "TrackNode.h"
#include "../TimeLinePlayHead.h"


/*
    usage: constructor
    trackData: the track's data
    clipsNodes: the track's initial clips
*/
TrackNode::TrackNode(const Models::Track& trackData, const juce::Array<ClipNode*>& clipsNodes) :
    _trackData(trackData), _samplesPerBlockExpected(0), _sampleRate(0), _isPrepared(false)
{
    //adding the initial clips
    for (auto* clipNode : clipsNodes)
    {
        this->addClip(clipNode);
    }
}

/*
    usage: prepares the class to play
    samplesPerBlockExpected: the number samples expected per block
    sampleRate: the sample rate
*/
void TrackNode::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
    this->_sampleRate = sampleRate;
    this->_samplesPerBlockExpected = samplesPerBlockExpected;

    //preparing all other essentials
    this->_gain.prepare({ (double)this->_sampleRate, (juce::uint32)this->_samplesPerBlockExpected, 2 });
    this->_gain.setGainDecibels(this->_trackData.fader);
    this->_panner.prepare({ (double)this->_sampleRate, (juce::uint32)this->_samplesPerBlockExpected, 2 });
    this->_panner.setPan(this->_trackData.panner);

    for (auto* clipNode : this->_clipsNodes)
    {
        clipNode->prepareToPlay(samplesPerBlockExpected, sampleRate);
    }

    this->_isPrepared = true;
}

/*
    usage: releases the class play-resources
*/
void TrackNode::releaseResources()
{
    //releasing each clip's resources
    for (auto* clipNode : this->_clipsNodes)
    {
        clipNode->releaseResources();
    }

    this->_isPrepared = false;
}

/*
    usage: actual playback
    bufferToFill: the buffer to fill with audio for playback
*/
void TrackNode::getNextAudioBlock(const juce::AudioSourceChannelInfo& bufferToFill)
{
    //if the track is muted, empty the buffer
    if (this->_trackData.mute)
    {
        bufferToFill.clearActiveBufferRegion();
    }
    else
    {
        //retrieving the current play position
        juce::AudioPlayHead::CurrentPositionInfo positionInfo;
        TimeLinePlayHead::getInstance()->getCurrentPosition(positionInfo);
        juce::dsp::ProcessContextReplacing<float> processContext(juce::dsp::AudioBlock<float>(*bufferToFill.buffer));
        bool clipFound = false;

        //finding the clip for the current play position, and retrieving it's audio
        for (auto* clipNode : this->_clipsNodes)
        {
            if (clipNode->getStart() < positionInfo.timeInSeconds && positionInfo.timeInSeconds < clipNode->getStart() + clipNode->getLength())
            {
                clipNode->getNextAudioBlock(bufferToFill);
                clipFound = true;
                break;
            }
        }

        //if a clip was found for the current play position, apply gain and pann
        if (clipFound)
        {
            this->_gain.process(processContext);
            this->_panner.process(processContext);
        }
        else
        {
            bufferToFill.clearActiveBufferRegion();
        }
    }
}

/*
    usage: adds a clip
    clipNode: the clip to add
*/
void TrackNode::addClip(ClipNode* clipNode)
{
    //if the track is prepared, the clip needs to prepare too
    if (this->_isPrepared)
    {
        clipNode->prepareToPlay(this->_samplesPerBlockExpected, this->_sampleRate);
    }

    this->_clipsNodes.add(clipNode);
}

/*
    usage: removes a clip
    clipNode: the clip to remove
*/
void TrackNode::removeClip(ClipNode* clipNode)
{
    this->_clipsNodes.removeFirstMatchingValue(clipNode);

    //if the track is prepared, the clip needs to release it's resources
    if (this->_isPrepared)
    {
        clipNode->releaseResources();
    }
}

/*
    usage: sets the track's fader
    newFader: the fader value
*/
void TrackNode::setFader(const float newFader)
{
    this->_gain.setGainDecibels(newFader);
    this->_trackData.fader = newFader;
}

/*
    usage: sets the track's mute
    newMute: the mute value
*/
void TrackNode::setMute(const bool newMute)
{
    this->_trackData.mute = newMute;
}

/*
    usage: sets the track's pann
    newPanner: the new pann value
*/
void TrackNode::setPanner(const float newPanner)
{
    this->_panner.setPan(newPanner);
    this->_trackData.panner = newPanner;
}