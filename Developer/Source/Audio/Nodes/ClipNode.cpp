/*
  ==============================================================================

    ClipNode.cpp
    Created: 26 Dec 2020 11:50:35am
    Author:  eyal

  ==============================================================================
*/

#include "ClipNode.h"
#include "../TimeLinePlayHead.h"


/*
    usage: constructor
    clipData: the clip's data
*/
ClipNode::ClipNode(const Models::Clip& clipData) :
    _clipData(clipData)
{
    this->_formatManager.registerBasicFormats();

    //sets up the audio file for readign
    juce::File file(this->_clipData.path);
    auto* reader = this->_formatManager.createReaderFor(file);
    this->_readerSource.reset(new juce::AudioFormatReaderSource(reader, true));
    this->_readerSource.get()->setNextReadPosition(0);
}

/*
    usage: prepares the class to play
    samplesPerBlockExpected: the number samples expected per block
    sampleRate: the sample rate
*/
void ClipNode::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
    this->_sampleRate = sampleRate;
    this->_samplesPerBlockExpected = samplesPerBlockExpected;

    //prepares the audio file reader
    this->_readerSource.get()->prepareToPlay(samplesPerBlockExpected, sampleRate);
}

/*
    usage: releases the class play-resources
*/
void ClipNode::releaseResources()
{
    //releases the audio file reader's sources
    this->_readerSource.get()->releaseResources();
}

/*
    usage: actual playback
    bufferToFill: the buffer to fill with audio for playback
*/
void ClipNode::getNextAudioBlock(const juce::AudioSourceChannelInfo& bufferToFill)
{
    //retrievs the current play position
    juce::AudioPlayHead::CurrentPositionInfo positionInfo;
    TimeLinePlayHead::getInstance()->getCurrentPosition(positionInfo);

    //finds the start sample in the audio file
    int sampleInClip = (positionInfo.timeInSeconds - this->_clipData.start) * this->_sampleRate;

    //retrievs the audio from the audio file
    this->_readerSource.get()->setNextReadPosition(sampleInClip);
    this->_readerSource.get()->getNextAudioBlock(bufferToFill);
}

/*
    usage: gets the startin second
    return: the starting second
*/
float ClipNode::getStart() const
{
    return this->_clipData.start;
}

/*
    usage: gets the length
    return: the length
*/
float ClipNode::getLength() const
{
    return this->_clipData.length;
}

/*
    usage: sets the starting second
    newStartSecond: the new starting second
*/
void ClipNode::setStart(const float newStartsecond)
{
    this->_clipData.start = newStartsecond;
}

/*
    usage: sets the length
    newLength: the new length
*/
void ClipNode::setLenght(const float newLenght)
{
    this->_clipData.length = newLenght;
}
