/*
  ==============================================================================

    TrackNode.h
    Created: 26 Dec 2020 11:50:25am
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "ClipNode.h"
#include "../../Helper/Models.h"
#include "../TimeLinePlayer.h"


class TimeLinePlayer;

/*
    a class for managing and supplying a track's audio
*/
class TrackNode : public juce::AudioSource
{
public:
    TrackNode(const Models::Track& trackData, const juce::Array<ClipNode*>& clipsNodes);

    void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;       //prepares the class to play
    void releaseResources() override;                                                  //releases the class play-resources
    void getNextAudioBlock(const juce::AudioSourceChannelInfo& bufferToFill) override; //actual playback
     
    void addClip(ClipNode* clipNode);
    void removeClip(ClipNode* clipNode);

    void setFader(const float newFader);
    void setPanner(const float newPanner);
    void setMute(const bool newMute);

private:
    Models::Track _trackData;                              //the track's data
    juce::Array<ClipNode*> _clipsNodes;                    //the track's clips
    int _sampleRate;
    int _samplesPerBlockExpected;
    juce::dsp::Panner<float> _panner; //applies pann to the audio 
    juce::dsp::Gain<float> _gain;     //applies gain to the audio (fader)
    bool _isPrepared;
};