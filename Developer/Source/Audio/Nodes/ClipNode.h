/*
  ==============================================================================

    ClipNode.h
    Created: 26 Dec 2020 11:50:35am
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../../Helper/Models.h"


/*
    a class for managing and supplying a clip's audio
*/
class ClipNode : public juce::AudioSource
{
public:
    ClipNode(const Models::Clip& clipData);

    void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;       //prepares the class to play
    void releaseResources() override;                                                  //releases the class play-resources
    void getNextAudioBlock(const juce::AudioSourceChannelInfo& bufferToFill) override; //actual playback

    float getStart() const;
    float getLength() const;

    void setStart(const float newStartsecond);
    void setLenght(const float newLenght);

private:
    juce::AudioFormatManager _formatManager;                      //manages the different audio file formats
    std::unique_ptr<juce::AudioFormatReaderSource> _readerSource; //reads directly from the audio file
    Models::Clip _clipData;                                       //the clip's data
    int _sampleRate;
    int _samplesPerBlockExpected;
};