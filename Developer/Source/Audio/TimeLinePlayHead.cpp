/*
  ==============================================================================

    TimeLinePlayHead.cpp
    Created: 26 Dec 2020 11:59:26am
    Author:  eyal

  ==============================================================================
*/

#include "TimeLinePlayHead.h"

TimeLinePlayHead* TimeLinePlayHead::_instance{ nullptr };


/*
    usage: gets the class instance
    return: the class instance
*/
TimeLinePlayHead* TimeLinePlayHead::getInstance()
{
    if (_instance == nullptr)
    {
        _instance = new TimeLinePlayHead();
    }

    return _instance;
}

/*
    gets the current play position
    result: where to store the current play position
    return: true
*/
bool TimeLinePlayHead::getCurrentPosition(CurrentPositionInfo& result)
{
    result.resetToDefault();

    result.isPlaying = this->_isPlaying;
    result.isRecording = this->_isRecording;
    result.timeInSamples = this->_timeInSamples;
    
    //converts position from samples to seconds
    result.timeInSeconds = _sampleRate == 0 ? 0 : (double)this->_timeInSamples / _sampleRate;

    return true;
}

/*
    usage: unused
*/
bool TimeLinePlayHead::canControlTransport()
{
    return true;
}

/*
    usage: sets is playing
    shouldStartPlaying: is playing
*/
void TimeLinePlayHead::transportPlay(bool shouldStartPlaying)
{
    this->_isPlaying = shouldStartPlaying;
}

/*
    usage: sets is recording
    shouldStartPlaying: is recording
*/
void TimeLinePlayHead::transportRecord(bool shouldStartRecording)
{
    this->_isRecording = shouldStartRecording;
}

/*
    usage: rewinds the position
*/
void TimeLinePlayHead::transportRewind()
{
    this->_timeInSamples = 0;
    this->sendChangeMessage();
}

/*
    usage: sets the current play position in samples
    positionInSamples: the position in samples
*/
void TimeLinePlayHead::setPlayPosition(const int positionInSamples)
{
    this->_timeInSamples = positionInSamples;
    this->sendChangeMessage();
}

/*
    usage: sets the current play position in seconds
    positionInSeconds: the position in seconds
*/
void TimeLinePlayHead::setPlayPosition(const float positionInSeconds)
{
    //converts position from seconds to samples
    this->_timeInSamples = positionInSeconds * this->_sampleRate;
    this->sendChangeMessage();
}

/*
    usage: sets the sample rate
    sampleRate: the sample rate
*/
void TimeLinePlayHead::setSampleRate(const int sampleRate)
{
    this->_sampleRate = sampleRate;
}

/*
    usage: constructor
*/
TimeLinePlayHead::TimeLinePlayHead() : 
    _timeInSamples(0), _isPlaying(false), _isRecording(false), _sampleRate(0)
{
}

/*
    usage: destructor
*/
TimeLinePlayHead::~TimeLinePlayHead()
{
}
