/*
  ==============================================================================

    TimeLinePlayHead.h
    Created: 26 Dec 2020 11:59:26am
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>


/*
    a class that keeps the current play position (singletone)
*/
class TimeLinePlayHead : public juce::AudioPlayHead,
                         public juce::ChangeBroadcaster
{
public:
    //part of the singletone design pattern
    TimeLinePlayHead(TimeLinePlayHead& other) = delete;
    void operator=(const TimeLinePlayHead& other) = delete;

    static TimeLinePlayHead* getInstance();                        //gets the class instance

    bool getCurrentPosition(CurrentPositionInfo& result) override; //gets the current play position
    bool canControlTransport() override;                           //unused
    void transportPlay(bool shouldStartPlaying) override;          //sets is played
    void transportRecord(bool shouldStartRecording) override;      //sets is recording
    void transportRewind() override;                               //rewinds

    void setPlayPosition(const int positionInSamples);   //sets the current play position by samples
    void setPlayPosition(const float positionInSeconds); //sets the current play position by seconds
    void setSampleRate(const int sampleRate);

private:
    TimeLinePlayHead();
    ~TimeLinePlayHead();

private:
    static TimeLinePlayHead* _instance; //the class instance

    int _timeInSamples;
    bool _isPlaying;
    bool _isRecording;
    int _sampleRate;
};