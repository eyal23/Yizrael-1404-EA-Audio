/*
  ==============================================================================

    TimeLinePlayer.cpp
    Created: 22 Dec 2020 5:00:21pm
    Author:  eyal

  ==============================================================================
*/

#include "TimeLinePlayer.h"
#include "TimeLinePlayHead.h"
#include "../Engine/Engine.h"


/*
    usage: constructor
    trackNodes: the initial tracks of the project
*/
TimeLinePlayer::TimeLinePlayer(juce::Array<TrackNode*> tracksNodes) :
    _mixerSource(new juce::MixerAudioSource()),
    _sampleRate(0),
    _samplesPerBlockExpected(0),
    _isPrepared(false)
{

    //adding all inital tracks to project
    for (auto* trackNode : tracksNodes)
    {
        this->addTrack(trackNode);
    }

    //setting up for playback
    Engine::getInstance()->getDeviceManager().addAudioCallback(&this->_audioSourcePlayer);
    this->_audioSourcePlayer.setSource(this);
}

/*
    usage: destructor
*/
TimeLinePlayer::~TimeLinePlayer()
{
    //removing all tracks from project
    for (auto* trackNode : this->_tracksNodes)
    {
        this->removeTrack(trackNode);
    }

    //cleaning playback
    Engine::getInstance()->getDeviceManager().removeAudioCallback(&this->_audioSourcePlayer);
    this->_audioSourcePlayer.setSource(nullptr);
}

/*
    usage: prepares the class to play
    samplesPerBlockExpected: the number samples expected per block
    sampleRate: the sample rate
*/
void TimeLinePlayer::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
    this->_sampleRate = sampleRate;
    this->_samplesPerBlockExpected = samplesPerBlockExpected;

    //preparing all other essentials
    TimeLinePlayHead::getInstance()->setSampleRate(sampleRate);
    this->_mixerSource.get()->prepareToPlay(samplesPerBlockExpected, sampleRate);

    this->_isPrepared = true;
}

/*
    usage: releases the class play-resources
*/
void TimeLinePlayer::releaseResources()
{
    //reseasing all other essentials play-resources
    this->_mixerSource.get()->releaseResources();

    this->_isPrepared = false;
}

/*
    usage: actual playback
    bufferToFill: the buffer to fill with audio for playback
*/
void TimeLinePlayer::getNextAudioBlock(const juce::AudioSourceChannelInfo& bufferToFill)
{
    //getting the current play-position
    juce::AudioPlayHead::CurrentPositionInfo positionInfo;
    TimeLinePlayHead::getInstance()->getCurrentPosition(positionInfo);

    if (positionInfo.isPlaying)
    {
        //retrieving the audio from all tracks, mixed
        this->_mixerSource.get()->getNextAudioBlock(bufferToFill);
        //advancing the play-position
        TimeLinePlayHead::getInstance()->setPlayPosition((int)(positionInfo.timeInSamples + bufferToFill.numSamples));
    }
}

/*
    usage: adds a track to the project
    trackNode: the track to add
*/
void TimeLinePlayer::addTrack(TrackNode* trackNode)
{
    //if the project is prepared, the track needs
    //to get prepared too
    if (this->_isPrepared)
    {
        trackNode->prepareToPlay(this->_samplesPerBlockExpected, this->_sampleRate);
    }

    this->_tracksNodes.add(trackNode);
    this->_mixerSource.get()->addInputSource(trackNode, false);
}

/*
    usage: removes a track from the project
    trackNode: the track to remove
*/
void TimeLinePlayer::removeTrack(TrackNode* trackNode)
{
    this->_tracksNodes.removeFirstMatchingValue(trackNode);
    this->_mixerSource.get()->removeInputSource(trackNode);

    //if the project is prepared, the track needs to
    //release its resources
    if (this->_isPrepared)
    {
        trackNode->releaseResources();
    }
}