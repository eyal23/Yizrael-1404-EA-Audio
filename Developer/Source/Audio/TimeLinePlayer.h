/*
  ==============================================================================

    TimeLinePlayer.h
    Created: 22 Dec 2020 5:00:21pm
    Author:  eyal

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "Nodes/TrackNode.h"


class TrackNode;


/*
    A class for playing a project's audio.
*/
class TimeLinePlayer : public juce::AudioSource
{
public:
    TimeLinePlayer(juce::Array<TrackNode*> tracksNodes);
    ~TimeLinePlayer();

    void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;       //prepares the class to play
    void releaseResources() override;                                                  //releases the class play-resources
    void getNextAudioBlock(const juce::AudioSourceChannelInfo& bufferToFill) override; //actual playback

    void addTrack(TrackNode* trackNode);
    void removeTrack(TrackNode* trackNode);

private:
    juce::AudioSourcePlayer _audioSourcePlayer;           //suplies audio to the audio devices
    std::unique_ptr<juce::MixerAudioSource> _mixerSource; //mixes each track's audio together
    juce::Array<TrackNode*> _tracksNodes;                 //the tracks of the project
    int _sampleRate;                                      
    int _samplesPerBlockExpected;
    bool _isPrepared;
};