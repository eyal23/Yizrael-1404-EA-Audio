# **EA-Audio**
D.A.W developed by Eyal Heinrich and Amit Azoulay.

![Alt text](./resources/project overview.png)
## Features
- Manage and edit multiple musical projects.
- Organize audio recordings via tracks.
- Set tracks audio levels with the fader tool.
- Set tracks spaceial location with the panner tool.
- Mute and unmute specific tracks.
- Configure tracks visuals.
- Import external audio recording as clips.
- Cut clips content from their start/end.
- Edit clips starting second.
- Configure clips visuals.
- Real-time audio playback.
- Configure audio settings (input/output device, etc).
#### Projects
Go to the "Files" tab, there you can find the "create"/"delete"/"open"/"close" project options (marked in green).

![Alt text](./resources/projects1.png)
After an option is selected, a matching form will pop on the screen.
#### Tracks
Go to the "Files" tab, there you can find the "create"/"delete" track options (marked in green).

![Alt text](./resources/tracks1.png)
After an option is selected, a matching form will pop on the screen.

When a track is created, identify the followings:
- **The track timeline component (marked in orange)**
    * The track's content (marked in yellow) - displays the track's clips, layed out on the timeline. 
    * The track's header (marked in pink) - when clicked, the track is selected, and the "track editing pannel" will show. 
- **The Track mixer component (painted in green)**
    * The Fader (marked in red) - sets the track's audio level.
    * The Panner (marked in blue) - sets the track's spacial location.
    * The mute button (marked in purple) - mutes/unmutes the track from playback.
- **The track editing pannel (marked in cyan)** - allows to edit the track's parametrs manually.

![Alt text](./resources/tracks2.png)
#### Clips
Go to the "Files" tab, there you can find the "create"/"delete" clip options (marked in green).

![Alt text](./resources/clips1.png)
After an option is selected, a matching form will pop on screen.

When a clip is created, identify the followings:
- **The clip timeline component (painted in blue)**
    * The clip's header (marked in red) - by dragging it, the clip will move backwords and forward in the timeline.
    * The cut-from-start grabber (marked in green) - by dragging it, the clip's content will get cut from it's start.
    * The cut-from-end grabber (marked in pink) - by dragging it, the clip's content will get cut from it's end.

![Alt text](./resources/clips2.png)
#### Real-time playback
By placing the white vertical line anywhere on the timeline, with the mouse. The current play position will be set.

![Alt text](./resources/playback1.png)
To start/stop the playback, click the green play and the red stop buttons.

![Alt text](./resources/playback2.png)

#### Audio settings
Go to the "Settings" tab, there you can find the "Audio Devices" option.

![Alt text](./resources/audio devices1.png)
## Installation
This is a step by step installation guide for the general user, and the developer.
#### General user
1. Download/clone the most updated version from the "master" branche.
2. Place the download in a location (path) which contains **only standart ASCII characaters**.
3. Extract the files, if downloaded as .zip.
4. Go to "Yizrael-1404-EA-Audio\User\".
5. Execute "DAW.exe".
#### Developer
1. Download/clone the most updated version from the "master" branche.
2. Place the download in a location (path) which contains **only standart ASCII characaters**.
3. Extract the files, if downloaded as .zip.
4. Go to "Yizrael-1404-EA-Audio\Developer\Builds\VisualStudio2019\".
5. Open "DAW.sln" in Visual Studio 19.
6. Build the project on "Debug" mode, and targeted for x64.
## Requirements
- Windows x64 bit OS.
- Visual Studio 19.
- Visual Studio "Desktop development with C++" workload.

**Enjoy!**
